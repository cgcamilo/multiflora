<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/noticias/define.php';
include 'cms/modules/noticias/model/noticia.php';
include 'cms/modules/noticias/model/noticiaDAO.php';

$db = new Database();
$db->connect();

$DAO = new NoticiaDAO($db);
$noticias = $DAO->gets('date', "desc");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />
<link href="css/multiflora.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script src="js/menu.js"></script>
<script src="js/slide.js"></script>
<script src="js/noticias.js"></script>

<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->

</head>

<body>
<?php include("header.php"); ?>
<div class="contendor-noticias">
	<div class="cont-noticias">
    	<div class="noticias-tit"><?php echo $generalLang['noticias'];?></div>
        <?php foreach($noticias as $noticia){ if($noticia->getLang() != $lang) continue; ?>
        <div class="noticia">
            <div class="noticia-img"><img src="cms/modules/noticias/files/<?php echo $noticia->getImage1();?>" alt="" /></div>
            <div class="noticia-info">
                <div class="noticia-fecha"><?php echo $noticia->getDateFormat();?></div>
                <div class="noticia-tit"><?php echo $noticia->getTitle();?></div>
                <div class="noticia-txt"><?php echo $noticia->getDescriptionLimited(320);?></div>
                <div class="ver-mas"><a href="noticias-detalle.php?id=<?php echo $noticia->getId();?>"><?php echo $generalLang['ver'];?></a></div>
            </div>
        </div>
        <?php break; } ?>
        <div class="clear"></div>
    </div>
    <div class="flor"></div>
</div>

<div class="contendor-otras-noticias">
	<div class="cont-otras-noticias">
		<div class="noticias-tit"><?php echo $generalLang['otrasnoticias'];?></div>
        <div class="bot-prev"></div>
        <div class="slide-cont">
            <div class="noticias-slide">
                
                <ul>
                    <?php $i = 0; foreach($noticias as $noticia){ if($noticia->getLang() != $lang) continue; $i++; if($i==1) continue;?>
                    <li class="noticia-mini">
                    	<div class="noticia-mini-tit"><?php echo $noticia->getTitle();?></div>
                        <div class="noticia-m-img"><img src="cms/modules/noticias/files/<?php echo $noticia->getImage1();?>" width="80" /></div>
                        <div class="noticia-mini-info">
                            <div class="noticia-mini-fecha"><?php echo $noticia->getDateFormat();?></div>
                            <div class="noticia-mini-txt"><?php echo $noticia->getDescriptionLimited(75);?></div>
                            <div class="ver-mas"><a href="noticias-detalle.php?id=<?php echo $noticia->getId();?>"><?php echo $generalLang['ver'];?></a></div>
                        </div>
                        <div class="clear"></div>
                    </li>                    
                    <?php } ?>
                </ul>
                
            </div>
        </div>
        <div class="bot-next"></div>
        <div class="clear"></div>
    </div>
</div>
<?php include("footer.php"); ?>
</body>
</html>
