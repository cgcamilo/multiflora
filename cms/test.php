<?php

$in_filename = 'four-square.jpg';

list($width, $height) = getimagesize($in_filename);

$offset_x = 0;
$offset_y = 0+5;

$new_height = $height;
$new_width = $width;

$image = imagecreatefromjpeg($in_filename);
$new_image = imagecreatetruecolor($width, 20);
imagecopy($new_image, $image, 0, 0, $offset_x, $offset_y, $width, 50);

header('Content-Type: image/jpeg');
imagejpeg($new_image);

?>