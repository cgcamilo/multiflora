<?php
////////////////////////////////
//Camilo Cifuentes
//info@ccamilo.com
//Lappeenranta, Finlandia, 2012
////////////////////////////////
class Noticia
{
    private $id;
    private $title;
    private $date;
    private $image1;
    private $image2;
    private $order;
    private $description;
    private $shortDescription;
    private $lang;

    function __construct()
    {}

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function getDateFormat() {
        return date("d-m-Y", $this->date);
    }

    public function getImage1() {
        return $this->image1;
    }

    public function setImage1($image1) {
        $this->image1 = $image1;
    }

    public function getImage2() {
        return $this->image2;
    }

    public function setImage2($image2) {
        $this->image2 = $image2;
    }

    public function getOrder() {
        return $this->order;
    }

    public function setOrder($order) {
        $this->order = $order;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getShortDescription() {
        return $this->shortDescription;
    }

    public function setShortDescription($shortDescription) {
        $this->shortDescription = $shortDescription;
    }

    public function getLang() {
        return $this->lang;
    }

    public function setLang($lang) {
        $this->lang = $lang;
    }

    public function getDescriptionLimited($l = 400) {
        $noHTMLText = strip_tags($this->description);

        if( strlen($noHTMLText) > $l ){            
            return substr($noHTMLText, 0, $l).'...';
        }
        else
            return $noHTMLText;
    }

}
?>