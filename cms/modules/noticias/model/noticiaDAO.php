<?php
@include '../define.php';

class NoticiaDAO{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(Noticia $noticia){

        $querty =   'insert into cms_'.TABLE_NAME.'
                ('.TABLE_NAME.'_title , '.TABLE_NAME.'_date, '.TABLE_NAME.'_img1, '.TABLE_NAME.'_img2,
                 '.TABLE_NAME.'_order, '.TABLE_NAME.'_description, '.TABLE_NAME.'_shortdescription, '.TABLE_NAME.'_lang)
                values(
                "'.$noticia->getTitle().'",
                "'.mysql_real_escape_string($noticia->getDate()).'",
                "'.mysql_real_escape_string($noticia->getImage1()).'",
                "'.mysql_real_escape_string($noticia->getImage2()).'",
                "'.mysql_real_escape_string($noticia->getOrder()).'",
                "'.$noticia->getDescription().'",
                "'.mysql_real_escape_string($noticia->getShortDescription()).'",
                "'.mysql_real_escape_string($noticia->getLang()).'"
                )';


       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }

   
    function gets($order, $orderType){

        $sql = 'SELECT * from cms_'.TABLE_NAME.'  ';
        $sql .= 'order by '.TABLE_NAME.'_'.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $lista=array();

        if(count( $results ) == 0){
            return $lista;
        }

        $i = 0;
        foreach ($results as $result){
            $noticia = new Noticia();
            $noticia->setId($result[''.TABLE_NAME.'_id']);
            $noticia->setTitle($result[''.TABLE_NAME.'_title']);
            $noticia->setDate($result[''.TABLE_NAME.'_date']);
            $noticia->setImage1($result[''.TABLE_NAME.'_img1']);
            $noticia->setImage2($result[''.TABLE_NAME.'_img2']);
            $noticia->setOrder($result[''.TABLE_NAME.'_order']);
            $noticia->setDescription($result[''.TABLE_NAME.'_description']);
            $noticia->setShortDescription($result[''.TABLE_NAME.'_shortdescription']);
            $noticia->setLang($result[''.TABLE_NAME.'_lang']);
            $lista[$i] = $noticia;
            $i++;
        }


        return $lista;
    }

    function getById($id){

        $sql = 'SELECT * from cms_'.TABLE_NAME.' WHERE '.TABLE_NAME.'_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $noticia = new Noticia();
        $noticia->setId($result[''.TABLE_NAME.'_id']);
        $noticia->setTitle($result[''.TABLE_NAME.'_title']);
        $noticia->setDate($result[''.TABLE_NAME.'_date']);
        $noticia->setImage1($result[''.TABLE_NAME.'_img1']);
        $noticia->setImage2($result[''.TABLE_NAME.'_img2']);
        $noticia->setOrder($result[''.TABLE_NAME.'_order']);
        $noticia->setDescription($result[''.TABLE_NAME.'_description']);
        $noticia->setShortDescription($result[''.TABLE_NAME.'_shortdescription']);
        $noticia->setLang($result[''.TABLE_NAME.'_lang']);
            
        return $noticia;
    }

   
   
    function delete($id){

        $sql = 'Delete from cms_'.TABLE_NAME.' WHERE '.TABLE_NAME.'_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }

    //TODO
    function update(Noticia $noticia){

        $sql =   "UPDATE
                     cms_".TABLE_NAME."
                    SET
                    ".TABLE_NAME."_title =
                    \"".$noticia->getTitle()."\",
                    ".TABLE_NAME."_img1 =
                    \"".mysql_real_escape_string($noticia->getImage1())."\",
                    ".TABLE_NAME."_img2 =
                    \"".mysql_real_escape_string($noticia->getImage2())."\",
                    ".TABLE_NAME."_description =
                    \"".$noticia->getDescription()."\",
                    ".TABLE_NAME."_shortdescription =
                    \"".$noticia->getShortDescription()."\",
                    ".TABLE_NAME."_date =
                    \"".$noticia->getDate()."\",
                    ".TABLE_NAME."_lang =
                    \"".$noticia->getLang()."\",
                    ".TABLE_NAME."_order =
                    \"".$noticia->getOrder()."\"
                    WHERE ".TABLE_NAME."_id =
                    ".mysql_real_escape_string($noticia->getId())."
                    ";
        //echo $sql;
        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_'.TABLE_NAME.';';
        if($opt == 1)
                $sql = 'select count(*) from cms_'.TABLE_NAME.' where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];
        
    }


}

?>
