<?php
$queryCreateTable[0] = "
CREATE TABLE cms_".TABLE_NAME."_config (
".TABLE_NAME."_config_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
".TABLE_NAME."_config_funcionality int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
".TABLE_NAME."_config_date datetime DEFAULT NULL COMMENT 'Fecha y hora de instalación módulo',
PRIMARY KEY (".TABLE_NAME."_config_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
/////////////////////////////////////////////////////////////////////////

//option 1: id, title, url, blank, order
$queryCreateTable[1] = "
CREATE TABLE cms_".TABLE_NAME." (
".TABLE_NAME."_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
".TABLE_NAME."_title varchar(80) DEFAULT NULL COMMENT 'NOTICIA title',
".TABLE_NAME."_date int(11) unsigned DEFAULT 0 COMMENT 'NOTICIA fecha timestamp',
".TABLE_NAME."_img1 varchar(100) DEFAULT NULL COMMENT 'IMAGEN 1',
".TABLE_NAME."_img2 varchar(100) DEFAULT NULL COMMENT 'IMAGEN 1',
".TABLE_NAME."_order int(5) unsigned DEFAULT 0 COMMENT 'NOTICIA order',
".TABLE_NAME."_description TEXT DEFAULT NULL COMMENT 'NOTICIA descripcion',
".TABLE_NAME."_shortdescription TEXT DEFAULT NULL COMMENT 'NOTICIA descripcion corta',
".TABLE_NAME."_lang varchar(100) DEFAULT 'es' COMMENT 'Idioma',
PRIMARY KEY (".TABLE_NAME."_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";

?>