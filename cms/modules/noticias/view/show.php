<?php
$notiDAO = new NoticiaDAO($db);
$noticias = $notiDAO->gets('date', "desc");
?>
<a name="linkShow"></a>
<div class="tableName toolbar">
    <table class="display data_table2" >
        <thead>
                <tr>
                    <th><div class="th_wrapp">Fecha</div></th>
                    <th><div class="th_wrapp">Idioma</div></th>
                    <th><div class="th_wrapp">Título</div></th>
                    <th><div class="th_wrapp">Imagen</div></th>
                    <th><div class="th_wrapp">Acciones</div></th>
                </tr>
        </thead>
        <tbody>
        <?php foreach ($noticias as $noticia){ ?>
            <tr class="odd gradeX">
                <td class="center" width="100px"><?php echo $noticia->getDateFormat();?></td>
                <td class="center" width="40px"><?php echo $noticia->getLang();?></td>
                <td class="center" width="100px">
                    <?php echo $noticia->getTitle();?>
                </td>
                <td class="center" width="80px">
                    <?php if( $noticia->getImage1() != "" ){ ?>
                    <img alt="" src="../files/<?php echo $noticia->getImage1();?>" width="60" />
                    <?php } ?>
                </td>
                <td class="center" width="100px">
                    <a class="Delete uibutton special" href="../controller/notiDelete.php?id=<?php echo $noticia->getId()?>">Eliminar</a>
                    <a  href="edit.php?id=<?php echo $noticia->getId()?>" class="uibutton">Editar</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>