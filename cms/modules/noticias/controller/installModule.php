<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../define.php';
//Creamos el nuevo objeto "Database"
$db = new Database();
//Conectamos
$db->connect();


foreach ($_GET as $key => $value) {
    $$key = $value;
}

//one more check
$db->doQuery("SHOW TABLES LIKE 'cms_".TABLE_NAME."'",SHOW_TABLE_QUERY);
//if already exists go back to the index
if($db->show){
    $location = "location: ./../view/index.php?";
    header($location."&message=Módulo ya instalado");
    exit;
}

include '../includes/querys.php';

$dateTime =  date("Y-m-d H:i:s");
$db->createTable($queryCreateTable[0]);
$db->doQuery("INSERT INTO cms_".TABLE_NAME."_config(".TABLE_NAME."_config_id, ".TABLE_NAME."_config_funcionality, ".TABLE_NAME."_config_date)VALUES(1,0,'$dateTime')",INSERT_QUERY);

$db->createTable($queryCreateTable[1]);

if( $example ){
    include '../includes/dataExample.php';
    $db->doQuery($dataExample,INSERT_QUERY);
}

$db->disconnect();

$location = "location: ./../view/index.php?";
header($location."&message=Módulo instalado correctamente");
exit;
?>
