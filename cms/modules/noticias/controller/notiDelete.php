<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include 'includes.php';

$db = new Database();
$db->connect();

$id = $_GET['id'];

$DAO = new NoticiaDAO($db);

$noti = $DAO->getById($id);
if( $noti == null ){
    header($location."&error=Noticia not found");
    exit;
}

if( $noti->getImage1() != "" ){
    $filename = $destino = './../files/'.$noti->getImage1();
    @unlink($filename);
}

if( $noti->getImage2() != "" ){
    $filename = $destino = './../files/'.$noti->getImage2();
    @unlink($filename);
}

$DAO->delete($id);

$location = "location: ./../view/index.php?";
header($location."&message=Noticia eliminada");
exit;


?>
