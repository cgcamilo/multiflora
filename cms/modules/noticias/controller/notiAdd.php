<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include 'includes.php';
include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

$DAO = new NoticiaDAO($db);
$postBack = '';
foreach ($_POST as $key => $value) {
    $$key = $value;
    $postBack .= '&'.$key.'='.$value;
}

$location = "location: ./../view/index.php?".$postBack;

$noticia = new Noticia();
$noticia->setTitle($title);
$noticia->setDescription($description);
$noticia->setShortDescription($shortDescription);
$noticia->setOrder(1);
$noticia->setDate(strtotime($date));
$noticia->setLang($lang);
if( $title == "" || $date == "" ){
    header($location."&message=Noticia no añadida. el Título y fecha son campos obligatorios!");
    exit;
}

if( $_FILES['file1']['name'] != ""){

    $fileName = $_FILES['file1']['name'];

    $key = cRandom(4);
    $destino = './../files/'.$key.'.'.$fileName;
       //cambio el tamaño de la imagen
    $image2 = new SimpleImage();
    $image2->load($_FILES[ 'file1' ][ 'tmp_name' ]);
    $image2->resizeToWidth(621);
    $image2->save($destino);

    $noticia->setImage1 ($key.'.'.$fileName);
}

if( $_FILES['file2']['name'] != ""){

    $fileName = $_FILES['file2']['name'];

    $key = cRandom(4);
    $destino = './../files/'.$key.'.'.$fileName;
       //cambio el tamaño de la imagen
    $image2 = new SimpleImage();
    $image2->load($_FILES[ 'file2' ][ 'tmp_name' ]);
    $image2->resizeToWidth(273);
    $image2->save($destino);

    $noticia->setImage2 ($key.'.'.$fileName);
}

$DAO->save($noticia);

$location = "location: ./../view/index.php?";
header($location."&message=Noticia añadida");
exit;
?>