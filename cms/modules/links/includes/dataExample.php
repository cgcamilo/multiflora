<?php
@include '../define.php';

$dataExample[1]="INSERT INTO `cms_".TABLE_NAME."` (`".TABLE_NAME."_title`, `".TABLE_NAME."_url`, `".TABLE_NAME."_blank`, `".TABLE_NAME."_order`, `".TABLE_NAME."_description`) VALUES
	('Lorem Ipsum', 'http://www.imaginamos.com', 1, 1, 'description' ),
	('Lorem Ipsum', 'http://www.imaginamos.com', 0, 1, 'description' )
";

$dataExample[2]="INSERT INTO `cms_".TABLE_NAME."` (`".TABLE_NAME."_title`, `".TABLE_NAME."_url`, `".TABLE_NAME."_blank`, `".TABLE_NAME."_order`, `".TABLE_NAME."_image`, `".TABLE_NAME."_description`) VALUES
	('Lorem Ipsum', 'http://www.imaginamos.com', 1, 1, 'test.jpg', 'description' ),
	('Lorem Ipsum', 'http://www.imaginamos.com', 0, 1, 'test.jpg', 'description' )
";
?>