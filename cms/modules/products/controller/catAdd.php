<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/category.php';
include '../model/categoryDAO.php';
include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();


$catDAO = new CategoryDAO($db);

foreach ($_POST as $key => $value) {
    $$key = $value;
    $postBack .= '&'.$key.'='.$value;
}

$location = "location: ./../view/index.php?".$postBack;

$cat = new Category();
$cat->setTitle($title);
$cat->setSubTitle($subTitle);
$cat->setLang($lang);
$cat->setDescription($description);

$fileName = $_FILES['file1']['name'];
$fileName2 = $_FILES['file2']['name'];
if( $title == "" || $fileName == ""  || $fileName2 == ""){
    header($location."&message=Categoría no añadida. el Título y las imagenes son campos obligatorios!");
    exit;
}

//imagen pequeña
$key = cRandom(4);
$destino = './../files/cat_'.$key.'.'.$_FILES['file1']['name'];
$image = new SimpleImage();
$image->load($_FILES[ 'file1' ][ 'tmp_name' ]);
$image->resize(134, 160);
$image->save($destino);
$cat->setImage('cat_'.$key.'.'.$_FILES['file1']['name']);

//imagen grande
$key = cRandom(4);
$destino = './../files/cat_'.$key.'.'.$_FILES['file2']['name'];
$image = new SimpleImage();
$image->load($_FILES[ 'file2' ][ 'tmp_name' ]);
$image->resize(2000, 500);
$image->save($destino);
$cat->setImage_big('cat_'.$key.'.'.$_FILES['file2']['name']);

$catDAO->save($cat);

$location = "location: ./../view/index.php?";
header($location."&message=Categoría añadida");
exit;
?>