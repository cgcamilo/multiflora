<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");

include '../model/image.php';
include '../model/imageDAO.php';

include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

$id = $_POST['id'];

$location = "location: ./../view/editProductPics.php?id=".$id;

$fileName = $_FILES['file']['name'];
if( $fileName == "" ){
    header($location."&message=Debes introducir la imagen a añadir");
    exit;
}

$imageP = new Image();

$imageP->setIdProduct($id);


//imagen pequeña
$key = cRandom(4);
$destino = './../files/img_'.$key.'.'.$_FILES['file']['name'];
$image = new SimpleImage();
$image->load($_FILES[ 'file' ][ 'tmp_name' ]);
$image->resize(460, 419);
$image->save($destino);
$imageP->setPath('img_'.$key.'.'.$_FILES['file']['name']);

$imageDAO = new ImageDAO($db);
$imageDAO->save($imageP);



header($location."&message=Imagen añadida exitosamente!#images");
exit;
?>