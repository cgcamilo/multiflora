<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");

include '../model/category.php';
include '../model/categoryDAO.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';

include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

$postBack = '';
foreach ($_POST as $key => $value) {
    $$key = $value;
}

$location = "location: ./../view/editProduct.php?id=".$id;

$productDAO = new ProductDAO($db);

$product = $productDAO->getById($id);

if( $product == null ){
    $location = "location: ./../view/indexProducts.php?";
    header($location."&message=Producto no encontrado");
    exit;
}

if( $title == "" ){
    $location = "location: ./../view/indexProducts.php?";
    header($location."&message=El Título es un campo obligatorio");
    exit;
}

$product->setTitle($title);
$product->setPrice($price);
$product->setRef($ref);
$product->setColor($color);
$product->setType($type);
$product->setVariety($variety);
$product->setStared($stared);
$product->setDecription($decription);
$product->setLang($lang);
$product->setSubCat($subCat);
//$product->setCat($cat);


//main images
for( $i=1; $i<=5; $i++  ){
    if( $_FILES['file'.$i]['name'] == "" )
        continue;
    $key = cRandom(4);
    $destino = './../files/'.$key.'.'.$_FILES['file'.$i]['name'];

    $image = new SimpleImage();
    $image->load($_FILES[ 'file'.$i ][ 'tmp_name' ]);
    $image->resize(316, 201);
    $image->save($destino);

    $filenameDel = $destino = './../files/'.$product->{'getImg'.$i}();
    @unlink($filenameDel);

    if( $i == 1 ){
        $filenameDel = $destino = './../files/th_'.$product->{'getImg'.$i}();
        @unlink($filenameDel);
        
        $destino = './../files/th_'.$key.'.'.$_FILES['file'.$i]['name'];
        $image->resizeToWidth(167);
        $image->save($destino);
    }

    $product->{'setImg'.$i}($key.'.'.$_FILES['file'.$i]['name']);
}


$productDAO->update($product);

header($location."&message=Producto editado exitosamente!");
exit;
?>