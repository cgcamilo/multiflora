<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");

include '../model/image.php';
include '../model/imageDAO.php';

include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

// If you want to ignore the uploaded files,
// set $demo_mode to true;
$upload_dir = '../files/';
$allowed_ext = array('jpg','jpeg','png','gif');


if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	exit_status('Error! Wrong HTTP method!');
}


if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){

        $key = cRandom(5);
        $id = $_REQUEST['id'];

	$pic = $_FILES['pic'];
        $pic['name'] = $key.'.'.$pic['name'];
        

	if(!in_array(get_extension($pic['name']),$allowed_ext)){
		exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
	}

        //upload itself
        
        $imageP = new Image();
        $imageP->setIdProduct($id);
        //imagen pequeña
        $image = new SimpleImage();
        $image->load($pic['tmp_name']);
        $image->resize(460, 419);
        $image->save($upload_dir.$pic['name']);

        $imageP->setPath($pic['name']);
        $imageDAO = new ImageDAO($db);
        $imageDAO->save($imageP);
        exit_status('File was uploaded successfuly!');
        
/*
	if(move_uploaded_file($pic['tmp_name'], $upload_dir.$pic['name'])){
		exit_status('File was uploaded successfuly!');
	}
*/

}

exit_status('Something went wrong with your upload!');


// Helper functions

function exit_status($str){
	echo json_encode(array('status'=>$str));
	exit;
}

function get_extension($file_name){
	$ext = explode('.', $file_name);
	$ext = array_pop($ext);
	return strtolower($ext);
}
?>