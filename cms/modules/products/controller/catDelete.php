<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/category.php';
include '../model/categoryDAO.php';

$db = new Database();
$db->connect();

$id = $_GET['id'];


$catDAO = new CategoryDAO($db);
$catDAO->delete($id);

$location = "location: ./../view/index.php?";
header($location."&message=Categoría eliminada");
exit;


?>
