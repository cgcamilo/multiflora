<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");

include '../model/category.php';
include '../model/categoryDAO.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';

include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

$postBack = '';
foreach ($_POST as $key => $value) {
    $$key = $value;
}

$location = "location: ./../view/editProductPics.php?id=".$id;

$productDAO = new ProductDAO($db);

$product = $productDAO->getById($id);

if( $product == null ){
    $location = "location: ./../view/indexProducts.php?";
    header($location."&message=Producto no encontrado");
    exit;
}

$product->setImgId($imgId);
$productDAO->update($product);


header($location."&message=Union exitosa");
exit;
?>