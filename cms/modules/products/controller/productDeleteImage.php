<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/image.php';
include '../model/imageDAO.php';

$db = new Database();
$db->connect();

$id = $_GET['id'];
$idm = $_GET['idm'];

$location = "location: ./../view/editProductPics.php?id=".$id;


$imageDAO = new ImageDAO($db);
$image = $imageDAO->getById($idm);

if( $image != null ){
    @unlink('../files/'.$image->getPath());
    $imageDAO->delete($idm);
}

header($location."&message=Imagen eliminada#images");
exit;


?>
