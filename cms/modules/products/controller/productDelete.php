<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/product.class.php';
include '../model/productDAO.class.php';

$db = new Database();
$db->connect();

$id = $_GET['id'];


$productDAO = new ProductDAO($db);
$product = $productDAO->getById($id);

if( $product == null ){
    header($location."&error=Product not found");
    exit;
}

for($i = 1; $i <=4 ; $i++){
    if( $product->{'getImg'.$i}() != "" ){
        $filename = $destino = './../files/'.$product->{'getImg'.$i}();
        @unlink($filename);
    }

    if( $i == 1 ){
        $filename = $destino = './../files/th_'.$product->{'getImg'.$i}();
        @unlink($filename);
    }
}


$productDAO->delete($id);

$location = "location: ./../view/indexProducts.php?";
header($location."&message=Producto eliminado");
exit;


?>