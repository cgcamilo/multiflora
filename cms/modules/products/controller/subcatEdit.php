<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/subCategoryDAO.php';
include '../model/category.php';
include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

$subCatDAO = new SubCategoryDAO($db);

foreach ($_POST as $key => $value) {
    $$key = $value;
}

$location = "location: ./../view/editSubCat.php?id=".$id;

$subCat = $subCatDAO->getById($id);

if( $subCat == null ){
    header($location."&error=SubCat not found");
    exit;
}

$subCat->setTitle($title);
//$subCat->setCat($cat);

if( $title == "" ){
    header($location."&message=Subcategoría no actualizada. el Título es un campo obligatorio!");
    exit;
}

if($_FILES['file']['name'] != ""){

    if($subCat->getImage() != ""){
        @unlink('./../files/'.$subCat->getImage());
    }
    //imagen pequeña
    $key = cRandom(4);
    $destino = './../files/sub_cat_'.$key.'.'.$_FILES['file']['name'];
    $image = new SimpleImage();
    $image->load($_FILES[ 'file' ][ 'tmp_name' ]);
    $image->resize(230, 100);
    $image->save($destino);
    $subCat->setImage('sub_cat_'.$key.'.'.$_FILES['file']['name']);
}

$subCatDAO->update($subCat);


header($location."&message=Subcategoría actualizada");
exit;
?>