<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");

include '../model/category.php';
include '../model/categoryDAO.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';

include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

$postBack = '';
foreach ($_POST as $key => $value) {
    $$key = $value;
    $postBack .= '&'.$key.'='.$value;
}

$location = "location: ./../view/addProduct.php?".$postBack;

$product = new Product();
$product->setTitle($title);
$product->setPrice($price);
$product->setColor($color);
$product->setType($type);
$product->setVariety($variety);
$product->setRef($ref);
$product->setStared($stared);
$product->setDecription($decription);
$product->setLang($lang);
$product->setCat($cat);
$product->setSubCat($subCat);
$product->setImgId(0);

$fileName1 = $_FILES['file1']['name'];

if( $title == "" || $fileName1 == ""){
    header($location."&message=Producto no añadido. El título y la foto principal son campos obligatorios");
    exit;
}



//main images
for( $i=1; $i<=5; $i++  ){
    if( $_FILES['file'.$i]['name'] == "" )
        continue;
    $key = cRandom(4);
    $destino = './../files/'.$key.'.'.$_FILES['file'.$i]['name'];

    $image = new SimpleImage();
    $image->load($_FILES[ 'file'.$i ][ 'tmp_name' ]);
    $image->resize(316, 201);
    $image->save($destino);
    $product->{'setImg'.$i}($key.'.'.$_FILES['file'.$i]['name']);

    if( $i == 1 ){
        $destino = './../files/th_'.$key.'.'.$_FILES['file'.$i]['name'];
        $image->resizeToWidth(167);
        $image->save($destino);
    }
}

$productDAO = new ProductDAO($db);
$productDAO->save($product);

$id = $productDAO->getLastId();

$location = "location: ./../view/editProduct.php?id=".$id;
header($location."&message=Producto añadido exitosamente! Ahora puedes añadirle imagenes a este producto.");
exit;
?>