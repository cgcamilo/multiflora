<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/categoryDAO.php';
include '../model/category.php';
include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();

$catDAO = new CategoryDAO($db);

foreach ($_POST as $key => $value) {
    $$key = $value;
}

$location = "location: ./../view/editCat.php?id=".$id;

$cat = $catDAO->getById($id);

if( $cat == null ){
    header($location."&error=Cat not found");
    exit;
}

$cat->setTitle($title);
$cat->setSubTitle($subTitle);
//$cat->setLang($lang);
$cat->setDescription($description);

if( $title == "" ){
    header($location."&message=Categoría no actualizada. el Título es un campo obligatorio!");
    exit;
}

//Imagen pequeña
$fileName = $_FILES['file1']['name'];
if( $fileName != "" ){
    if( $cat->getImage() != "" ){
        @unlink('./../files/'.$cat->getImage());
    }
    $key = cRandom(4);
    $destino = './../files/cat_'.$key.'.'.$_FILES['file1']['name'];
    $image = new SimpleImage();
    $image->load($_FILES[ 'file1' ][ 'tmp_name' ]);
    $image->resize(134, 160);
    $image->save($destino);
    $cat->setImage('cat_'.$key.'.'.$_FILES['file1']['name']);
}

//Imagen grande
$fileName = $_FILES['file2']['name'];
if( $fileName != "" ){
    if( $cat->getImage_big() != "" ){
        @unlink('./../files/'.$cat->getImage_big());
    }
    $key = cRandom(4);
    $destino = './../files/cat_'.$key.'.'.$_FILES['file2']['name'];
    $image = new SimpleImage();
    $image->load($_FILES[ 'file2' ][ 'tmp_name' ]);
    $image->resize(2000, 500);
    $image->save($destino);
    $cat->setImage_big('cat_'.$key.'.'.$_FILES['file2']['name']);
}

$catDAO->update($cat);


header($location."&message=Categoría actualizada");
exit;
?>