<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/category.php';
include '../model/subCategoryDAO.php';
include '../includes/simpleImages.php';
include '../includes/random.php';

$db = new Database();
$db->connect();


$catDAO = new SubCategoryDAO($db);

$postBack = '';
foreach ($_POST as $key => $value) {
    $$key = $value;
    $postBack .= '&'.$key.'='.$value;
}

$location = "location: ./../view/addSubCat.php?".$postBack;

$subcat = new Category();
$subcat->setTitle($title);
$subcat->setCat($cat);
$subcat->setLang($lang);

$fileName = $_FILES['file']['name'];

if( $title == "" || $fileName == "" ){
    header($location."&message=Subcategoría no añadida. el Título y la imagen son campos obligatorios!");
    exit;
}

//imagen pequeña
$key = cRandom(4);
$destino = './../files/sub_cat_'.$key.'.'.$_FILES['file']['name'];
$image = new SimpleImage();
$image->load($_FILES[ 'file' ][ 'tmp_name' ]);
$image->resize(230, 100);
$image->save($destino);
$subcat->setImage('sub_cat_'.$key.'.'.$_FILES['file']['name']);

$catDAO->save($subcat);

$location = "location: ./../view/indexSubCat.php?";
header($location."&message=Subcategoría añadida");
exit;
?>