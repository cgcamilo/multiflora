<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/category.php';
include '../model/subCategoryDAO.php';

$db = new Database();
$db->connect();

$id = $_GET['id'];


$catDAO = new SubCategoryDAO($db);
$catDAO->delete($id);

$location = "location: ./../view/indexSubCat.php?";
header($location."&message=Subcategoría eliminada");
exit;


?>
