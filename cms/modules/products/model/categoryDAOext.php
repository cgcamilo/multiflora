<?php

class CategoryDAOext{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(CategoryExt $category){


            $querty =   'insert into cms_products_cat
                    (products_cat_title, products_cat_sub_title, products_cat_lang, products_cat_image, products_cat_image_big, products_cat_description)
                    values(
                    "'.$category->getTitle().'",
                    "'.$category->getSubTitle().'",
                    "'.mysql_real_escape_string($category->getLang()).'",
                    "'.mysql_real_escape_string($category->getImage()).'",
                    "'.mysql_real_escape_string($category->getImage_big()).'",
                    "'.$category->getDescription().'"
                    )';

        //echo $querty;
       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }


    function gets($order, $orderType){

        $sql = 'SELECT * from cms_products_cat  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $categories=array();

        if(count( $results ) == 0){
            return $categories;
        }

        $i = 0;
        foreach ($results as $result){
            $category = new CategoryExt();
            $category->setId($result['products_cat_id']);
            $category->setTitle($result['products_cat_title']);
            $category->setSubTitle($result['products_cat_sub_title']);
            $category->setLang($result['products_cat_lang']);
            $category->setImage($result['products_cat_image']);
            $category->setImage_big($result['products_cat_image_big']);
            $category->setDescription($result['products_cat_description']);

            $categories[$i] = $category;
            $i++;
        }
        return $categories;
    }

    function getsByLang($lang, $order, $orderType){

        $sql = 'SELECT * from cms_products_cat  ';
        $sql .= 'WHERE products_cat_lang = "'.$lang.'"';
        $sql .= 'order by products_cat_'.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $categories=array();

        if(count( $results ) == 0){
            return $categories;
        }

        $i = 0;
        foreach ($results as $result){
            $category = new CategoryExt();
            $category->setId($result['products_cat_id']);
            $category->setTitle($result['products_cat_title']);
            $category->setSubTitle($result['products_cat_sub_title']);
            $category->setLang($result['products_cat_lang']);
            $category->setImage($result['products_cat_image']);
            $category->setImage_big($result['products_cat_image_big']);
            $category->setDescription($result['products_cat_description']);

            $categories[$i] = $category;
            $i++;
        }
        return $categories;
    }

    function getById($id){

        $sql = 'SELECT * from cms_products_cat WHERE products_cat_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $category = new CategoryExt();
        $category->setId($result['products_cat_id']);
        $category->setTitle($result['products_cat_title']);
        $category->setSubTitle($result['products_cat_sub_title']);
        $category->setLang($result['products_cat_lang']);
        $category->setImage($result['products_cat_image']);
        $category->setImage_big($result['products_cat_image_big']);
        $category->setDescription($result['products_cat_description']);

        return $category;
    }

    function delete($id){

        $sql = 'Delete from cms_products_cat WHERE products_cat_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }


    function update(CategoryExt $category){

        $sql =   "UPDATE
                 cms_products_cat
                SET
                products_cat_title =
                \"".$category->getTitle()."\",
                products_cat_sub_title =
                \"".$category->getSubTitle()."\",
                products_cat_image =
                \"".$category->getImage()."\",
                products_cat_description =
                \"".$category->getDescription()."\",
                products_cat_lang =
                \"".$category->getLang()."\"
                WHERE products_cat_id =
                ".mysql_real_escape_string($category->getId())."
                ";

        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_products_cat;';
        if($opt == 1)
                $sql = 'select count(*) from cms_products_cat where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];

    }


}

?>