<?php

class ProductDAO{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(Product $product){

        
            $querty =   'insert into cms_products
                    (products_id_cat, products_id_subcat,
                    products_title , products_price , products_ref,
                    products_type, products_color, products_variety,
                    products_stared, products_description,
                    products_image1, products_image2, products_image3, products_image4, products_image5, products_lang)
                    values(
                    "'.mysql_real_escape_string($product->getCat()).'",
                    "'.mysql_real_escape_string($product->getSubCat()).'",
                    "'.$product->getTitle().'",
                    "'.mysql_real_escape_string($product->getPrice()).'",
                    "'.mysql_real_escape_string($product->getRef()).'",
                    "'.mysql_real_escape_string($product->getType()).'",
                    "'.mysql_real_escape_string($product->getColor()).'",
                    "'.mysql_real_escape_string($product->getVariety()).'",
                    "'.mysql_real_escape_string($product->getStared()).'",
                    "'.mysql_real_escape_string($product->getDecription()).'",
                    "'.mysql_real_escape_string($product->getImg1()).'",
                    "'.mysql_real_escape_string($product->getImg2()).'",
                    "'.mysql_real_escape_string($product->getImg3()).'",
                    "'.mysql_real_escape_string($product->getImg4()).'",
                    "'.mysql_real_escape_string($product->getImg5()).'",
                    "'.mysql_real_escape_string($product->getLang()).'"
                    )';

       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }

   
    function gets($order, $orderType){

        $sql = 'SELECT * from cms_products  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $products=array();

        if(count( $results ) == 0){
            return $products;
        }

        $i = 0;
        foreach ($results as $result){
            $product = new Product();
            $product->setId($result['products_id']);
            $product->setCat($result['products_id_cat']);
            $product->setSubCat($result['products_id_subcat']);
            $product->setTitle($result['products_title']);
            $product->setPrice($result['products_price']);
            $product->setRef($result['products_ref']);
            $product->setType($result['products_type']);
            $product->setColor($result['products_color']);
            $product->setVariety($result['products_variety']);
            $product->setStared($result['products_stared']);
            $product->setDecription($result['products_description']);
            $product->setImg1($result['products_image1']);
            $product->setImg2($result['products_image2']);
            $product->setImg3($result['products_image3']);
            $product->setImg4($result['products_image4']);
            $product->setImg5($result['products_image5']);
            $product->setLang($result['products_lang']);
            $product->setImgId($result['products_img_id']);

            $products[$i] = $product;
            $i++;
        }


        return $products;
    }

    function getsStared($order, $orderType){

        $sql = 'SELECT * from cms_products WHERE products_stared = 1 ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $products=array();

        if(count( $results ) == 0){
            return $products;
        }

        $i = 0;
        foreach ($results as $result){
            $product = new Product();
            $product->setId($result['products_id']);
            $product->setCat($result['products_id_cat']);
            $product->setSubCat($result['products_id_subcat']);
            $product->setTitle($result['products_title']);
            $product->setPrice($result['products_price']);
            $product->setRef($result['products_ref']);
            $product->setType($result['products_type']);
            $product->setColor($result['products_color']);
            $product->setVariety($result['products_variety']);
            $product->setStared($result['products_stared']);
            $product->setDecription($result['products_description']);
            $product->setImg1($result['products_image1']);
            $product->setImg2($result['products_image2']);
            $product->setImg3($result['products_image3']);
            $product->setImg4($result['products_image4']);
            $product->setImg5($result['products_image5']);
            $product->setLang($result['products_lang']);
            $product->setImgId($result['products_img_id']);

            $products[$i] = $product;
            $i++;
        }


        return $products;
    }

    function getById($id){

        $sql = 'SELECT * from cms_products WHERE products_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $product = new Product();
        $product->setId($result['products_id']);
        $product->setCat($result['products_id_cat']);
        $product->setSubCat($result['products_id_subcat']);
        $product->setTitle($result['products_title']);
        $product->setPrice($result['products_price']);
        $product->setRef($result['products_ref']);
        $product->setType($result['products_type']);
        $product->setColor($result['products_color']);
        $product->setVariety($result['products_variety']);
        $product->setStared($result['products_stared']);
        $product->setDecription($result['products_description']);
        $product->setImg1($result['products_image1']);
        $product->setImg2($result['products_image2']);
        $product->setImg3($result['products_image3']);
        $product->setImg4($result['products_image4']);
        $product->setImg5($result['products_image5']);
        $product->setLang($result['products_lang']);
        $product->setImgId($result['products_img_id']);
            
        return $product;
    }

    function getsByIdSubCat($id){

        $sql = 'SELECT * from cms_products WHERE products_id_subcat = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $products=array();

        if(count( $results ) == 0){
            return $products;
        }

        $i = 0;
        foreach ($results as $result){
            $product = new Product();
            $product->setId($result['products_id']);
            $product->setCat($result['products_id_cat']);
            $product->setSubCat($result['products_id_subcat']);
            $product->setTitle($result['products_title']);
            $product->setPrice($result['products_price']);
            $product->setRef($result['products_ref']);
            $product->setType($result['products_type']);
            $product->setColor($result['products_color']);
            $product->setVariety($result['products_variety']);
            $product->setStared($result['products_stared']);
            $product->setDecription($result['products_description']);
            $product->setImg1($result['products_image1']);
            $product->setImg2($result['products_image2']);
            $product->setImg3($result['products_image3']);
            $product->setImg4($result['products_image4']);
            $product->setImg5($result['products_image5']);
            $product->setLang($result['products_lang']);
            $product->setImgId($result['products_img_id']);

            $products[$i] = $product;
            $i++;
        }


        return $products;
    }

    function getByIdCat($id){

        $sql = 'SELECT * from cms_products WHERE products_id_cat = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $products=array();

        if(count( $results ) == 0){
            return $products;
        }

        $i = 0;
        foreach ($results as $result){
            $product = new Product();
            $product->setId($result['products_id']);
            $product->setCat($result['products_id_cat']);
            $product->setSubCat($result['products_id_subcat']);
            $product->setTitle($result['products_title']);
            $product->setPrice($result['products_price']);
            $product->setRef($result['products_ref']);
            $product->setType($result['products_type']);
            $product->setColor($result['products_color']);
            $product->setVariety($result['products_variety']);
            $product->setStared($result['products_stared']);
            $product->setDecription($result['products_description']);
            $product->setImg1($result['products_image1']);
            $product->setImg2($result['products_image2']);
            $product->setImg3($result['products_image3']);
            $product->setImg4($result['products_image4']);
            $product->setImg5($result['products_image5']);
            $product->setLang($result['products_lang']);
            $product->setImgId($result['products_img_id']);

            $products[$i] = $product;
            $i++;
        }


        return $products;
    }

    function getsSearch($s, $lang){

        $sql = 'SELECT * from cms_products WHERE (products_title LIKE "%'.$s.'%" OR products_description LIKE "%'.$s.'%" )AND products_lang = "'.$lang.'" ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $products=array();

        if(count( $results ) == 0){
            return $products;
        }

        $i = 0;
        foreach ($results as $result){
            $product = new Product();
            $product->setId($result['products_id']);
            $product->setCat($result['products_id_cat']);
            $product->setSubCat($result['products_id_subcat']);
            $product->setTitle($result['products_title']);
            $product->setPrice($result['products_price']);
            $product->setRef($result['products_ref']);
            $product->setType($result['products_type']);
            $product->setColor($result['products_color']);
            $product->setVariety($result['products_variety']);
            $product->setStared($result['products_stared']);
            $product->setDecription($result['products_description']);
            $product->setImg1($result['products_image1']);
            $product->setImg2($result['products_image2']);
            $product->setImg3($result['products_image3']);
            $product->setImg4($result['products_image4']);
            $product->setImg5($result['products_image5']);
            $product->setLang($result['products_lang']);
            $product->setImgId($result['products_img_id']);

            $products[$i] = $product;
            $i++;
        }


        return $products;
    }

    function getsByIdColor($color){

        $sql = 'SELECT * from cms_products WHERE products_color LIKE "%'.$color.'%"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $products=array();

        if(count( $results ) == 0){
            return $products;
        }

        $i = 0;
        foreach ($results as $result){
            $product = new Product();
            $product->setId($result['products_id']);
            $product->setCat($result['products_id_cat']);
            $product->setSubCat($result['products_id_subcat']);
            $product->setTitle($result['products_title']);
            $product->setPrice($result['products_price']);
            $product->setRef($result['products_ref']);
            $product->setType($result['products_type']);
            $product->setColor($result['products_color']);
            $product->setVariety($result['products_variety']);
            $product->setStared($result['products_stared']);
            $product->setDecription($result['products_description']);
            $product->setImg1($result['products_image1']);
            $product->setImg2($result['products_image2']);
            $product->setImg3($result['products_image3']);
            $product->setImg4($result['products_image4']);
            $product->setImg5($result['products_image5']);
            $product->setLang($result['products_lang']);
            $product->setImgId($result['products_img_id']);

            $products[$i] = $product;
            $i++;
        }


        return $products;
    }

    function getsByCatAndColor($cat, $color){

        $sql = 'SELECT * from cms_products WHERE products_id_cat = "'.$cat.'" && products_color LIKE "%'.$color.'%"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $products=array();

        if(count( $results ) == 0){
            return $products;
        }

        $i = 0;
        foreach ($results as $result){
            $product = new Product();
            $product->setId($result['products_id']);
            $product->setCat($result['products_id_cat']);
            $product->setSubCat($result['products_id_subcat']);
            $product->setTitle($result['products_title']);
            $product->setPrice($result['products_price']);
            $product->setRef($result['products_ref']);
            $product->setType($result['products_type']);
            $product->setColor($result['products_color']);
            $product->setVariety($result['products_variety']);
            $product->setStared($result['products_stared']);
            $product->setDecription($result['products_description']);
            $product->setImg1($result['products_image1']);
            $product->setImg2($result['products_image2']);
            $product->setImg3($result['products_image3']);
            $product->setImg4($result['products_image4']);
            $product->setImg5($result['products_image5']);
            $product->setLang($result['products_lang']);
            $product->setImgId($result['products_img_id']);

            $products[$i] = $product;
            $i++;
        }


        return $products;
    }
   
    function delete($id){

        $sql = 'Delete from cms_products WHERE products_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }

    
    function update(product $product){

        $sql =   "UPDATE
                 cms_products
                SET
                products_id_cat =
                \"".$product->getCat()."\",
                products_id_subcat =
                \"".$product->getSubCat()."\",
                products_title =
                \"".$product->getTitle()."\",
                products_price =
                \"".$product->getPrice()."\",
                products_ref =
                \"".$product->getRef()."\",
                products_variety =
                \"".$product->getVariety()."\",
                products_color =
                \"".$product->getColor()."\",
                products_type =
                \"".$product->getType()."\",
                products_stared =
                \"".$product->getStared()."\",
                products_lang =
                \"".$product->getLang()."\",
                products_description =
                \"".$product->getDecription()."\",
                products_img_id =
                \"".$product->getImgId()."\",
                products_image1 =
                \"".mysql_real_escape_string($product->getImg1())."\",
                products_image2 =
                \"".mysql_real_escape_string($product->getImg2())."\",
                products_image3 =
                \"".mysql_real_escape_string($product->getImg3())."\",
                products_image4 =
                \"".mysql_real_escape_string($product->getImg4())."\",
                products_image5 =
                \"".mysql_real_escape_string($product->getImg5())."\"
                WHERE products_id =
                ".mysql_real_escape_string($product->getId())."
                ";
        //echo $sql; die;
        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_products;';
        if($opt == 1)
                $sql = 'select count(*) from cms_products where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];
        
    }


}

?>