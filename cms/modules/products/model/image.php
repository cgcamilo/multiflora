<?php
////////////////////////////////
//Camilo Cifuentes
//info@ccamilo.com
//Lappeenranta, Finlandia, 2012
////////////////////////////////
class Image
{
    private $id;
    private $idProduct;
    private $path;
    private $order;

    function __construct()
    {}

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIdProduct() {
        return $this->idProduct;
    }

    public function setIdProduct($idProduct) {
        $this->idProduct = $idProduct;
    }

    public function getPath() {
        return $this->path;
    }

    public function setPath($path) {
        $this->path = $path;
    }

    public function getOrder() {
        return $this->order;
    }

    public function setOrder($order) {
        $this->order = $order;
    }


}
?>