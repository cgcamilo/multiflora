<?php
////////////////////////////////
//Camilo Cifuentes
//info@ccamilo.com
//Lappeenranta, Finlandia, 2012
////////////////////////////////
class Category
{
    private $id;
    private $cat;

    private $title;
    private $subTitle;
    private $lang;
    private $image;
    private $image_big;
    private $description;
    

    function __construct()
    {}

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCat() {
        return $this->cat;
    }

    public function setCat($cat) {
        $this->cat = $cat;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getLang() {
        return $this->lang;
    }

    public function setLang($lang) {
        $this->lang = $lang;
    }

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getImage_big() {
        return $this->image_big;
    }

    public function setImage_big($image_big) {
        $this->image_big = $image_big;
    }

    public function getSubTitle() {
        return $this->subTitle;
    }

    public function setSubTitle($subTitle) {
        $this->subTitle = $subTitle;
    }


}
?>