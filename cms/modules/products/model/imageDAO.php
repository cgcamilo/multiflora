<?php

class ImageDAO{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(Image $image){


            $querty =   'insert into cms_products_img
                    (products_img_id_product, products_img_order , products_img_path)
                    values(
                    "'.mysql_real_escape_string($image->getIdProduct()).'",
                    "'.$image->getOrder().'",
                    "'.$image->getPath().'"
                    )';

        //echo $querty; die;
       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }


    function gets($order, $orderType){

        $sql = 'SELECT * from cms_products_img  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $images=array();

        if(count( $results ) == 0){
            return $images;
        }

        $i = 0;
        foreach ($results as $result){
            $image = new Image();
            $image->setId($result['products_img_id']);
            $image->setIdProduct($result['products_img_id_product']);
            $image->setOrder($result['products_img_order']);
            $image->setPath($result['products_img_path']);

            $images[$i] = $image;
            $i++;
        }


        return $images;
    }

    function getsByIdProduct($idProduct, $order, $orderType){

        $sql = 'SELECT * from cms_products_img WHERE products_img_id_product = '.$idProduct.' ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $images=array();

        if(count( $results ) == 0){
            return $images;
        }

        $i = 0;
        foreach ($results as $result){
            $image = new Image();
            $image->setId($result['products_img_id']);
            $image->setIdProduct($result['products_img_id_product']);
            $image->setOrder($result['products_img_order']);
            $image->setPath($result['products_img_path']);

            $images[$i] = $image;
            $i++;
        }


        return $images;
    }

    function getById($id){

        $sql = 'SELECT * from cms_products_img WHERE products_img_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $image = new Image();
        $image->setId($result['products_img_id']);
        $image->setIdProduct($result['products_img_id_product']);
        $image->setOrder($result['products_img_order']);
        $image->setPath($result['products_img_path']);

        return $image;
    }

    function delete($id){

        $sql = 'Delete from cms_products_img WHERE products_img_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_products_img;';
        if($opt == 1)
                $sql = 'select count(*) from cms_products_img where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];

    }


}

?>