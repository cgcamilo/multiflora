<?php

class SubCategoryDAO{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(Category $category){


            $querty =   'insert into cms_products_subcat
                    (products_subcat_id, products_subcat_id_cat, products_subcat_lang, products_subcat_image, products_subcat_title)
                    values(
                    "'.mysql_real_escape_string($category->getId()).'",
                    "'.mysql_real_escape_string($category->getCat()).'",
                    "'.mysql_real_escape_string($category->getLang()).'",
                    "'.mysql_real_escape_string($category->getImage()).'",
                    "'.$category->getTitle().'"
                    )';

        //echo $querty;
       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }


    function gets($order, $orderType){

        $sql = 'SELECT * from cms_products_subcat  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $categories=array();

        if(count( $results ) == 0){
            return $categories;
        }

        $i = 0;
        foreach ($results as $result){
            $category = new Category();
            $category->setId($result['products_subcat_id']);
            $category->setCat($result['products_subcat_id_cat']);
            $category->setTitle($result['products_subcat_title']);
            $category->setImage($result['products_subcat_image']);
            $category->setLang($result['products_subcat_lang']);

            $categories[$i] = $category;
            $i++;
        }
        return $categories;
    }

    function getsByCat($id, $order, $orderType){

        $sql = 'SELECT * from cms_products_subcat  WHERE products_subcat_id_cat = '.$id.' ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $categories=array();

        if(count( $results ) == 0){
            return $categories;
        }

        $i = 0;
        foreach ($results as $result){
            $category = new Category();
            $category->setId($result['products_subcat_id']);
            $category->setCat($result['products_subcat_id_cat']);
            $category->setTitle($result['products_subcat_title']);
            $category->setImage($result['products_subcat_image']);
            $category->setLang($result['products_subcat_lang']);
            $categories[$i] = $category;
            $i++;
        }
        return $categories;
    }

    function getById($id){

        $sql = 'SELECT * from cms_products_subcat WHERE products_subcat_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;

        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $category = new Category();
        $category->setId($result['products_subcat_id']);
        $category->setCat($result['products_subcat_id_cat']);
        $category->setTitle($result['products_subcat_title']);
        $category->setImage($result['products_subcat_image']);
        $category->setLang($result['products_subcat_lang']);
        return $category;
    }

    function delete($id){

        $sql = 'Delete from cms_products_subcat WHERE products_subcat_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }


    function update(Category $category){

        $sql =   "UPDATE
                 cms_products_subcat
                SET
                products_subcat_title =
                \"".$category->getTitle()."\",
                products_subcat_image =
                \"".$category->getImage()."\",
                products_subcat_id_cat =
                \"".$category->getCat()."\"
                WHERE products_subcat_id =
                ".mysql_real_escape_string($category->getId())."
                ";

        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_products_subcat;';
        if($opt == 1)
                $sql = 'select count(*) from cms_products_subcat where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];

    }


}

?>
