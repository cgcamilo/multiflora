<?php
////////////////////////////////
//Camilo Cifuentes
//info@ccamilo.com
//Lappeenranta, Finlandia, 2012
////////////////////////////////
class Product
{
    private $id;
    private $cat;
    private $subCat;

    private $title;
    private $price;
    
    private $type;
    private $color;
    private $variety;

    private $decription;
    private $ref;
    private $stared;

    private $img1;
    private $img2;
    private $img3;
    private $img4;
    private $img5;

    private $lang;

    private $imgId;

    function __construct()
    {}

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCat() {
        return $this->cat;
    }

    public function setCat($cat) {
        $this->cat = $cat;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }
    
    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getColor() {
        return $this->color;
    }

    public function setColor($color) {
        $this->color = $color;
    }

    public function getVariety() {
        return $this->variety;
    }

    public function setVariety($variety) {
        $this->variety = $variety;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getDecription() {
        return $this->decription;
    }

    public function setDecription($decription) {
        $this->decription = $decription;
    }

    public function getRef() {
        return $this->ref;
    }

    public function setRef($ref) {
        $this->ref = $ref;
    }

    public function getStared() {
        return $this->stared;
    }

    public function setStared($stared) {
        $this->stared = $stared;
    }

    public function getImg1() {
        return $this->img1;
    }

    public function setImg1($img1) {
        $this->img1 = $img1;
    }

    public function getImg2() {
        return $this->img2;
    }

    public function setImg2($img2) {
        $this->img2 = $img2;
    }

    public function getImg3() {
        return $this->img3;
    }

    public function setImg3($img3) {
        $this->img3 = $img3;
    }

    public function getImg4() {
        return $this->img4;
    }

    public function setImg4($img4) {
        $this->img4 = $img4;
    }

    public function getImg5() {
        return $this->img5;
    }

    public function setImg5($img5) {
        $this->img5 = $img5;
    }

    function getDescruptionShortNoTags($l = 400){
        $noHTMLText = strip_tags($this->decription);
        if( strlen($noHTMLText) > $l ){
            return substr($noHTMLText, 0, $l).'...';
        }
        else
            return $noHTMLText;
    }

    public function getLang() {
        return $this->lang;
    }

    public function setLang($lang) {
        $this->lang = $lang;
    }

    public function getSubCat() {
        return $this->subCat;
    }

    public function setSubCat($subCat) {
        $this->subCat = $subCat;
    }

    public function getImgId() {
        return $this->imgId;
    }

    public function setImgId($imgId) {
        $this->imgId = $imgId;
    }



}
?>