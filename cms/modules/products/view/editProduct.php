<?php
session_start();
//Evita presentar contenidos sin el login debido
@include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
@include("../../../core/class/db.class.php");
//Creamos el nuevo objeto "Database"


include '../model/category.php';
include '../model/categoryDAO.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';
include '../model/image.php';
include '../model/imageDAO.php';
include '../model/subCategoryDAO.php';

//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

$db->doQuery("SHOW TABLES LIKE 'cms_products'",SHOW_TABLE_QUERY);
//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
if(!$db->show){
    $location = "location: ./view/index.php?";
    header($location."&error=Products not installed [edit]");
    exit;
}

$catDAO = new CategoryDAO($db);
$productoDAO = new ProductDAO($db);

$id = $_GET['id'];

$product = $productoDAO->getById($id);

if($product == null){
    $location = "location: ./index.php?";
    header($location."&error=Product not found [edit]");
    exit;
}

$cat = $catDAO->getById($product->getCat());
if( $cat == null ){
    $cat = new Category();
    $cat->setTitle('NO ENCONTRADA');
    $cat->setId(0);
}

$imageDAO = new ImageDAO($db);
$images = $imageDAO->getsByIdProduct($id, "products_img_id", "asc");

$subCatDAO = new SubCategoryDAO($db);
$subs = $subCatDAO->getsByCat($product->getCat(), "products_subcat_title", "asc");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>

		<!--External Files-->
        <link href="http://cms.imaginamos.com/css/generalCMS.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://cms.imaginamos.com/components/flot/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://cms.imaginamos.com/js/generalCMS.js"></script>
        <!--End External Files-->


        </head>

        <body class="dashborad">
        <div id="alertMessage" class="error"></div>
		<!-- Header -->
        <div id="header">
                <div id="account_info">
                    <?php include("../../../menu/administrator.php"); ?>
                </div>
            </div><!-- End Header -->
			<div id="shadowhead"></div>

              <div id="left_menu">
                    <ul id="main_menu" class="main_menu">
						<?php include("../../../menu/index.php"); ?>
                    </ul>
              </div>

              <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <ul id="shortcut">
								<?php include("../../../menu/icons.php"); ?>
                            </ul>
					</div>
                    <div class="clear"></div>

					<!-- full width -->
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>PRODUCTOS </span>

                        </div><!-- End header -->
                        <div class="content">
                            <?php if( isset ($_GET['message']) ){ ?>
                            <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message'];?></div>
                            <?php } ?>
                          <div class="formEl_b">
                                <div>
                                    <form id="formnews" method="post" enctype="multipart/form-data" action="../controller/productEdit.php">
                                    <br /><a class="uibutton icon special answer" href="indexProducts.php">Todos los productos</a>
                                    <a class="uibutton" href="editProductPics.php?id=<?php echo $id;?>">Galeria de Imagenes</a>
                                    <a class="uibutton" href="editProductPics2.php?id=<?php echo $id;?>">Subir Imagenes [+]</a>
                                    <br />
                                        <p>&nbsp;</p>
                                        <fieldset>
                                            <legend><h1>Editar producto de categoría <span style="color: red"><?php echo $cat->getTitle(); ?></span></h1></legend>
                                              <div>
                                                  <div><label>Idioma</label>
                                                      <span style="color: red">
                                                          [se corresponde con la categoría]
                                                      </span>
                                                  <br />
                                                  <?php echo $product->getLang();?>
                                                  <input type="hidden" name="lang" value="<?php echo $product->getLang();?>" />
                                                  <!--
                                                  <select name="lang">
                                                      <option value="es" <?php if( $product->getLang() == 'es' ) echo 'selected'; ?>>Español</option>
                                                      <option value="en" <?php if( $product->getLang() == 'en' ) echo 'selected'; ?>>English</option>
                                                      <option value="ru" <?php if( $product->getLang() == 'ru' ) echo 'selected'; ?>>Pусский</option>
                                                  </select>
                                                  -->
                                                    </div><br />
                                                  <div>
                                                      <label>Subcategoría</label><br />
                                                      <?php if( count($subs) != 0 ){ ?>
                                                      <select name="subCat" >
                                                          <?php foreach($subs as $sub){ ?>
                                                          <option value="<?php echo $sub->getId();?>" <?php if($product->getSubCat() == $sub->getId()) echo 'selected'; ?> >
                                                            <?php echo $sub->getTitle();?>
                                                          </option>
                                                          <?php } ?>
                                                      </select>
                                                      <?php }else echo 'La categoría de este producto no tiene subcategorías'; ?>
                                                  </div>
                                                  <br />&nbsp;
                                                  <div>
                                                      <label>Título</label><br /><input type="text" name="title" id="title" value="<?php echo $product->getTitle(); ?>"  class="large"/>
                                                      <span style="color: red">*</span>
                                                  </div>
                                                  <br />&nbsp;
                                                  <div>
                                                      <label>Referencia</label><br /><input type="text" name="ref" id="title" value="<?php echo $product->getRef(); ?>"  class="large"/>
                                                  </div>
                                                  <br />&nbsp;
                                                  <div>
                                                      <label>
                                                          Color
                                                      </label>
                                                          <span style="color: red">
                                                              [Si es más de un color colocarlos separados por espacios " "]
                                                          </span>
                                                      <br />
                                                      <input type="text" name="color" id="title" value="<?php echo $product->getColor(); ?>"  class="large"/>
                                                  </div>
                                                  <br />&nbsp;
                                                  <div>
                                                      <label>Descripción</label><br /><textarea name="decription" cols="50" rows="3"  id="article" class="article"><?php echo $product->getDecription(); ?></textarea>
                                                  </div>
                                                  <br />&nbsp;
                                                  <div>
                                                      <label>
                                                          Imagenes:
                                                          <span style="color: red">
                                                              [Tamaño: 316x 201px]
                                                          </span>
                                                      </label>
                                                      <br />
                                                      <?php if($product->getImg1() != ""){ ?>
                                                      <img src="../files/<?php echo $product->getImg1();?>" alt="" width="150" /><br />
                                                      <?php } ?>
                                                      <input type="file" name="file1" /> Principal<span style="color: red">*</span><br /><br />
                                                      <!--
                                                      <?php if($product->getImg2() != ""){ ?>
                                                      <img src="../files/<?php echo $product->getImg2();?>" alt="" width="150" /><br />
                                                      <?php } ?>
                                                      <input type="file" name="file2" /> <br /><br />

                                                      <?php if($product->getImg3() != ""){ ?>
                                                      <img src="../files/<?php echo $product->getImg3();?>" alt="" width="150" /><br />
                                                      <?php } ?>
                                                      <input type="file" name="file3" /> <br /><br />

                                                      <?php if($product->getImg4() != ""){ ?>
                                                      <img src="../files/<?php echo $product->getImg4();?>" alt="" width="150" /><br />
                                                      <?php } ?>
                                                      <input type="file" name="file4" /> <br /><br />

                                                      <?php if($product->getImg5() != ""){ ?>
                                                      <img src="../files/<?php echo $product->getImg5();?>" alt="" width="150" /><br />
                                                      <?php } ?>
                                                      <input type="file" name="file5" /> <br /><br />
                                                      -->
                                                  </div>
                                                  <br />&nbsp;
                                                  <br />&nbsp;
                                                      <input type="submit" class="uibutton icon add" value="Guardar" />

                                                      <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                              </div>
                                               <div>
                                              </div>
                                        </fieldset>
                                    </form>
                                    </div>
                            </div>


                            <!-- clear fix -->
                            <div class="clear"></div>

                        </div><!-- End content -->
                    </div><!-- End full width -->



					<!-- clear fix -->
					<div class="clear"></div>

                    <div id="footer"> &copy; Copyright 2012 <span class="tip"><a  href="#" title="Todos los derechos reservados" >imaginamos.com</a> </span> </div>

                </div> <!--// End inner -->
              </div> <!--// End content -->

              <script language="javascript">$("#article").cleditor();</script>

</body>
</html>
