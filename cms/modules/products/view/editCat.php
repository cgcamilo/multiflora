<?php
session_start();
//Evita presentar contenidos sin el login debido
@include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
@include("../../../core/class/db.class.php");



include '../model/category.php';
include '../model/categoryDAO.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';

//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

$db->doQuery("SHOW TABLES LIKE 'cms_products'",SHOW_TABLE_QUERY);
//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
if(!$db->show){
    $location = "location: ./view/index.php?";
    header($location."&error=Products not installed [edit]");
    exit;
}


$id = $_GET['id'];
$catDAO = new CategoryDAO($db);
$cat = $catDAO->getById($id);

if($cat == null){
    $location = "location: ./index.php?";
    header($location."&error=Cat not found [edit]");
    exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>

		<!--External Files-->
        <link href="http://cms.imaginamos.com/css/generalCMS.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://cms.imaginamos.com/components/flot/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://cms.imaginamos.com/js/generalCMS.js"></script>
        <!--End External Files-->


        </head>

        <body class="dashborad">
        <div id="alertMessage" class="error"></div>
		<!-- Header -->
        <div id="header">
                <div id="account_info">
                    <?php include("../../../menu/administrator.php"); ?>
                </div>
            </div><!-- End Header -->
			<div id="shadowhead"></div>

              <div id="left_menu">
                    <ul id="main_menu" class="main_menu">
						<?php include("../../../menu/index.php"); ?>
                    </ul>
              </div>

              <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <ul id="shortcut">
								<?php include("../../../menu/icons.php"); ?>
                            </ul>
					</div>
                    <div class="clear"></div>

					<!-- full width -->
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>PRODUCTOS </span>

                        </div><!-- End header -->
                        <div class="content">
                            <?php if( isset ($_GET['message']) ){ ?>
                            <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message'];?></div>
                            <?php } ?>
                          <div class="formEl_b">
                                <div>
                                    <form id="formnews" method="post" enctype="multipart/form-data" action="../controller/catEdit.php">
                                    <br /><a class="uibutton icon special answer" href="index.php">Volver</a>
                                        <p>&nbsp;</p>
                                        <fieldset>
                                            <legend><h1>Edición de Categoría</h1></legend>
                                              <div>
                                              <table width="100%">
                                                      <tr>
                                                          <td width="250" valign="top">
                                                              <label>Título</label>
                                                          </td>
                                                          <td valign="top">
                                                              <input type="text" name="title" id="title" value="<?php echo $cat->getTitle();?>"  class="large"/>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td width="250" valign="top">
                                                              <label>Subtítulo</label> [Recomendado máximo 5 palabras]
                                                          </td>
                                                          <td valign="top">
                                                              <input type="text" name="subTitle" id="title" value="<?php echo $cat->getSubTitle();?>"  class="large"/>
                                                          </td>
                                                      </tr>
                                                  <!--
                                                      <tr>
                                                          <td valign="top">
                                                              <label>Idioma</label>
                                                          </td>
                                                          <td valign="top">
                                                              <select name="lang">
                                                                  <option value="es" <?php if( $cat->getLang() == 'es' ) echo 'selected'; ?>>Español</option>
                                                                  <option value="en" <?php if( $cat->getLang() == 'en' ) echo 'selected'; ?>>English</option>
                                                                  <option value="ru" <?php if( $cat->getLang() == 'ru' ) echo 'selected'; ?>>Pусский</option>
                                                              </select>
                                                          </td>
                                                      </tr>
                                                  -->
                                                      <tr>
                                                          <td valign="top">
                                                              <label>Imagen pequeña</label><br /><span style="color: red;font-size: x-small">Tamaño 134x160 px </span>
                                                          </td>
                                                          <td valign="top">
                                                              <?php if($cat->getImage() != ""){  ?>
                                                              <img alt="" src="../files/<?php echo $cat->getImage();?>" width="150" />
                                                              <br />
                                                              <?php } ?>
                                                              <input type="file" name="file1" />
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td valign="top">
                                                              <label>Imagen grande </label><br /><span style="color: red;font-size: x-small">Tamaño 2000x500 px </span>
                                                          </td>
                                                          <td valign="top">
                                                              <?php if($cat->getImage_big() != ""){  ?>
                                                              <img alt="" src="../files/<?php echo $cat->getImage_big();?>" width="200" />
                                                              <br />
                                                              <?php } ?>
                                                              <input type="file" name="file2" />
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td valign="top">
                                                              <label>Descripción </label> Máximo 200 carácteres
                                                          </td>
                                                          <td>
                                                              <textarea name="description" id="noticia" cols="5" rows="3" class="large"><?php echo $cat->getDescription();?></textarea>
                                                          </td>
                                                      </tr>
                                                  </table>
                                                  <br />
                                                  <input type="hidden" name="id" value="<?php echo $id;?>" />
                                                  <input type="submit" class="uibutton icon add" value="Guardar" />
                                              </div>
                                              <p>&nbsp;</p>
                                               <div>

                                              </div>
                                        </fieldset>
                                        <p>&nbsp;</p>
                                    </form>
                                    </div>
                            </div>
                            <!-- clear fix -->
                            <div class="clear"></div>

                        </div><!-- End content -->
                    </div><!-- End full width -->



					<!-- clear fix -->
					<div class="clear"></div>

                    <div id="footer"> &copy; Copyright 2012 <span class="tip"><a  href="#" title="Todos los derechos reservados" >imaginamos.com</a> </span> </div>

                </div> <!--// End inner -->
              </div> <!--// End content -->

              <script language="javascript">$("#article").cleditor();</script>

</body>
</html>
