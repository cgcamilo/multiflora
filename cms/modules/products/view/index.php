<?php
session_start();
//Evita presentar contenidos sin el login debido
@include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
@include("../../../core/class/db.class.php");
//Creamos el nuevo objeto "Database"


include '../model/category.php';
include '../model/categoryDAO.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';

//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	        
        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>

		<!--External Files-->
        <link href="http://cms.imaginamos.com/css/generalCMS.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://cms.imaginamos.com/components/flot/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://cms.imaginamos.com/js/generalCMS.js"></script>
        <!--End External Files-->

      
        </head>
        
        <body class="dashborad">
        <div id="alertMessage" class="error"></div>
		<!-- Header -->
        <div id="header">
                <div id="account_info">
                    <?php include("../../../menu/administrator.php"); ?>
                </div>
            </div><!-- End Header -->
			<div id="shadowhead"></div>

              <div id="left_menu">
                    <ul id="main_menu" class="main_menu">
						<?php include("../../../menu/index.php"); ?>
                    </ul>
              </div>
            
              <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <ul id="shortcut">
								<?php include("../../../menu/icons.php"); ?>
                            </ul>
					</div>
                    <div class="clear"></div>
                    
					<!-- full width -->
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>PRODUCTOS </span>
                            
                        </div><!-- End header -->	
                        <div class="content">
                            <h3>
                                Categorías
                                |
                                <a href="indexSubCat.php">Subcategorías</a>
                                |
                                <a href="indexProducts.php">Productos</a>
                            </h3>
                            <?php if( isset ($_GET['message']) ){ ?>
                            <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message'];?></div>
                            <?php } ?>
                          <div class="formEl_b">
        
                    <fieldset>
                    
     				<?php
					//Enviamos el query que realiza la búsqueda de la tabla que necesia el módulo
					$db->doQuery("SHOW TABLES LIKE 'cms_products_config'",SHOW_TABLE_QUERY);
					//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
					if($db->show){                                           
                                            //include 'add.php';
                                            $catDAO = new CategoryDAO($db);
                                            $cats = $catDAO->gets("products_cat_id", "asc");
                                            ?>

                                            <h4>Categorías</h4>
                                            <?php //include 'addCat.php';?>
                                            <div class="tableName toolbar">
                                                <table class="display data_table2" >
                                                    <thead>
                                                            <tr>
                                                                <th><div class="th_wrapp">Idioma</div></th>
                                                                <th><div class="th_wrapp">Título</div></th>
                                                                <th><div class="th_wrapp">Imagen pequeña</div></th>
                                                                <th><div class="th_wrapp">Acciones</div></th>
                                                            </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($cats as $cat){ ?>
                                                        <tr class="odd gradeX">
                                                            <td class="center" width="40" >
                                                                <?php echo $cat->getLang();?>
                                                            </td>
                                                            <td class="center" width="200px">
                                                                <?php echo $cat->getTitle();?>
                                                            </td>
                                                            <td class="center" width="200px">
                                                                <img src="../files/<?php echo $cat->getImage();?>" alt="" width="50" />
                                                            </td>
                                                            <td class="center" width="120px">
                                                                <!--<a class="Delete uibutton special" href="../controller/catDelete.php?id=<?php echo $cat->getId()?>">Eliminar</a>-->
                                                                <a  href="editCat.php?id=<?php echo $cat->getId()?>" class="uibutton">Editar</a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <?php
                                        }
					else{
                                            //instalación
                                            include 'install.php';
                                        }
						
					?>
                    </fieldset>
                    </div>
                            <!-- clear fix -->
                            <div class="clear"></div>
                            
                        </div><!-- End content -->
                    </div><!-- End full width -->
                        
					
                                           
					<!-- clear fix -->
					<div class="clear"></div>

                    <div id="footer"> &copy; Copyright 2012 <span class="tip"><a  href="#" title="Todos los derechos reservados" >imaginamos.com</a> </span> </div>

                </div> <!--// End inner -->
              </div> <!--// End content -->
              
              <script language="javascript">$("#article").cleditor();</script>              
              
</body>
</html>