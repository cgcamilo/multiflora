<?php
session_start();
//Evita presentar contenidos sin el login debido
@include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
@include("../../../core/class/db.class.php");



include '../model/category.php';
include '../model/categoryDAO.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';
include '../model/subCategoryDAO.php';

//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>

		<!--External Files-->
        <link href="http://cms.imaginamos.com/css/generalCMS.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://cms.imaginamos.com/components/flot/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://cms.imaginamos.com/js/generalCMS.js"></script>
        <!--End External Files-->


        </head>

        <body class="dashborad">
        <div id="alertMessage" class="error"></div>
		<!-- Header -->
        <div id="header">
                <div id="account_info">
                    <?php include("../../../menu/administrator.php"); ?>
                </div>
            </div><!-- End Header -->
			<div id="shadowhead"></div>

              <div id="left_menu">
                    <ul id="main_menu" class="main_menu">
						<?php include("../../../menu/index.php"); ?>
                    </ul>
              </div>

              <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <ul id="shortcut">
								<?php include("../../../menu/icons.php"); ?>
                            </ul>
					</div>
                    <div class="clear"></div>

					<!-- full width -->
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>PRODUCTOS </span>

                        </div><!-- End header -->
                        <div class="content">
                            <h3>
                                <a href="index.php">Categorías</a>
                                |
                                <a href="indexSubCat.php">Subcategorías</a>
                                |
                                Productos
                            </h3>
                            <?php if( isset ($_GET['message']) ){ ?>
                            <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message'];?></div>
                            <?php } ?>
                          <div class="formEl_b">

                    <fieldset>

     				<?php
					//Enviamos el query que realiza la búsqueda de la tabla que necesia el módulo
					$db->doQuery("SHOW TABLES LIKE 'cms_products_config'",SHOW_TABLE_QUERY);
					//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
					if($db->show){
                                            $catDAO = new CategoryDAO($db);
                                            $cats = $catDAO->gets("products_cat_lang", "asc");

                                            $productDAO = new ProductDAO($db);
                                            $products = $productDAO->gets("products_title", "asc");

                                            $subCatDAO = new SubCategoryDAO($db);
                                            ?>

                                            <h4>Productos</h4>
                                            <form action="./addProduct.php" method="get">
                                            <table>
                                                <tr>
                                                    <td valign="top">
                                                        <select name="cat">
                                                        <?php foreach ($cats as $cat){ ?>
                                                        <option value="<?php echo $cat->getId();?>">
                                                            (<?php echo $cat->getLang();?>) <?php echo $cat->getTitle();?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </td>
                                                    <td valign="top">
                                                        <input type="submit" class="uibutton icon add" value="Añadir" />
                                                    </td>
                                                </tr>
                                            </table>
                                            </form>
                                            <div class="tableName toolbar">
                                                <table class="display data_table2" >
                                                    <thead>
                                                            <tr>
                                                                <th><div class="th_wrapp">Idioma</div></th>
                                                                <th><div class="th_wrapp">Categoría / Subcategoría</div></th>
                                                                <th><div class="th_wrapp">Título (ref)</div></th>
                                                                <th><div class="th_wrapp">Image</div></th>
                                                                <th><div class="th_wrapp">Acciones</div></th>
                                                            </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($products as $product){ ?>
                                                        <tr class="odd gradeX">
                                                            <td class="center" width="40" >
                                                                <?php echo $product->getLang();?>
                                                            </td>
                                                            <td class="center" width="100px">
                                                                <?php
                                                                    $cat = $catDAO->getById($product->getCat());
                                                                    if( $cat != null )
                                                                        echo $cat->getTitle().'<br />';
                                                                    $subCat = $subCatDAO->getById($product->getSubCat());
                                                                    if( $subCat != null )
                                                                        echo $subCat->getTitle();
                                                                ?>
                                                            </td>
                                                            <td class="center" width="120px">
                                                                <?php echo $product->getTitle();?>
                                                                <br />
                                                                (<?php echo $product->getRef();?>)
                                                                <?php if( $product->getStared() ){ ?>
                                                                <br /><img src="../images/hearth.png" alt="" title="Producto destacado" />
                                                                <?php } ?>
                                                            </td>
                                                            <td class="center" width="80px">
                                                                <img src="../files/th_<?php echo $product->getImg1(); ?>" width="60" alt="img" />
                                                            </td>
                                                            <td class="center" width="120px">
                                                                <a  href="editProduct.php?id=<?php echo $product->getId()?>" class="uibutton">Editar</a>
                                                                <a  href="editProductPics.php?id=<?php echo $product->getId()?>#images" class="uibutton">Galería</a>
                                                                <br />
                                                                <a  href="editProductPics2.php?id=<?php echo $product->getId()?>#images" class="uibutton">Subir fotos</a>
                                                                <a class="Delete uibutton special" href="../controller/productDelete.php?id=<?php echo $product->getId()?>">Eliminar</a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <?php
                                        }
					else{
                                            //instalación
                                            include 'install.php';
                                        }

					?>
                    </fieldset>
                    </div>
                            <!-- clear fix -->
                            <div class="clear"></div>

                        </div><!-- End content -->
                    </div><!-- End full width -->



					<!-- clear fix -->
					<div class="clear"></div>

                    <div id="footer"> &copy; Copyright 2012 <span class="tip"><a  href="#" title="Todos los derechos reservados" >imaginamos.com</a> </span> </div>

                </div> <!--// End inner -->
              </div> <!--// End content -->

              <script language="javascript">$("#article").cleditor();</script>

</body>
</html>
