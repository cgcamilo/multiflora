<div>
    <form id="formnews" method="post" enctype="multipart/form-data" action="../controller/catAdd.php">
    <p>&nbsp;</p>
    <fieldset>
        <legend><h1>Carga de categoría</h1></legend>
              <table width="100%">
                  <tr>
                      <td width="250" valign="top">
                          <label>Título</label>
                      </td>
                      <td valign="top">
                          <input type="text" name="title" id="title" value="<?php echo $_GET['title'];?>"  class="large"/>
                      </td>
                  </tr>
                  <tr>
                      <td width="250" valign="top">
                          <label>Subtítulo</label>
                      </td>
                      <td valign="top">
                          <input type="text" name="subTitle" id="title" value="<?php echo $_GET['subtitle'];?>"  class="large"/>
                      </td>
                  </tr>
                  <tr>
                      <td valign="top">
                          <label>Idioma</label>
                      </td>
                      <td valign="top">
                          <select name="lang">
                              <option value="es">Español</option>
                              <option value="en">English</option>
                              <option value="ru">Pусский</option>
                          </select>
                      </td>
                  </tr>
                  <tr>
                      <td valign="top">
                          <label>Imagen pequeña</label><br /><span style="color: red;font-size: x-small">Tamaño 134x160 px </span>
                      </td>
                      <td valign="top"><input type="file" name="file1" /></td>
                  </tr>
                  <tr>
                      <td valign="top">
                          <label>Imagen grande</label><br /><span style="color: red;font-size: x-small">Tamaño 2000x500 px </span>
                      </td>
                      <td valign="top"><input type="file" name="file2" /></td>
                  </tr>
                  <tr>
                      <td valign="top">
                          <label>Descripción </label>
                      </td>
                      <td>
                          <textarea name="description" id="noticia" cols="5" rows="3" class="large"><?php echo $_GET['description'];?></textarea>
                      </td>
                  </tr>
              </table>
              <br />
              <input type="submit" class="uibutton icon add" value="Guardar" />

          <p>&nbsp;</p>
    </fieldset>
    <p>&nbsp;</p>
</form>
</div>