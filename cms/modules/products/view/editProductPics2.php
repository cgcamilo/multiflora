<?php
session_start();
//Evita presentar contenidos sin el login debido
@include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
@include("../../../core/class/db.class.php");
//Creamos el nuevo objeto "Database"

include '../define.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';
include '../model/image.php';
include '../model/imageDAO.php';

//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

$db->doQuery("SHOW TABLES LIKE 'cms_products'",SHOW_TABLE_QUERY);
//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
if(!$db->show){
    $location = "location: ./view/index.php?";
    header($location."&error=Products not installed [edit]");
    exit;
}


$productoDAO = new ProductDAO($db);

$id = $_GET['id'];

$product = $productoDAO->getById($id);

if($product == null){
    $location = "location: ./index.php?";
    header($location."&error=Product not found [edit]");
    exit;
}


$imageDAO = new ImageDAO($db);
$images = $imageDAO->getsByIdProduct($id, "products_img_id", "asc");

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />

        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <link rel="stylesheet" href="assets/css/styles.css" />
        <script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
        <script src="assets/js/jquery.filedrop.js"></script>
        <script>
            $(function(){

            var dropbox = $('#dropbox'),
                    message = $('.message', dropbox);

            dropbox.filedrop({
                    // The name of the $_FILES entry:
                    paramname:'pic',

                    maxfiles: 8,
                    maxfilesize: 17,
                    url: '../controller/productEditImages2.php?id=<?php echo $id;?>',

                    uploadFinished:function(i,file,response){
                            $.data(file).addClass('done');
                            // response is the JSON object that post_file.php returns
                    },

            error: function(err, file) {
                            switch(err) {
                                    case 'BrowserNotSupported':
                                            showMessage('Tu navegador no suporta HTML5. Por favor usa la versión que está en "Galería"');
                                            break;
                                    case 'TooManyFiles':
                                            alert('Muchos archivos, por favor sube archivos de 8 en 8 máximo');
                                            break;
                                    case 'FileTooLarge':
                                            alert(file.name+' muy grande! Máximo 6mb por archivo.');
                                            break;
                                    default:
                                            break;
                            }
                    },

                    // Called before each upload is started
                    beforeEach: function(file){
                            if(!file.type.match(/^image\//)){
                                    alert('Solo se permiten imagenes!');
                                    // Returning false will cause the
                                    // file to be rejected
                                    return false;
                            }
                    },

                    uploadStarted:function(i, file, len){
                            createImage(file);
                    },

                    progressUpdated: function(i, file, progress) {
                            $.data(file).find('.progress').width(progress);
                    }

            });

            var template = '<div class="preview">'+
                                                    '<span class="imageHolder">'+
                                                            '<img />'+
                                                            '<span class="uploaded"></span>'+
                                                    '</span>'+
                                                    '<div class="progressHolder">'+
                                                            '<div class="progress"></div>'+
                                                    '</div>'+
                                            '</div>';


            function createImage(file){

                    var preview = $(template),
                            image = $('img', preview);

                    var reader = new FileReader();

                    image.width = 100;
                    image.height = 100;

                    reader.onload = function(e){

                            // e.target.result holds the DataURL which
                            // can be used as a source of the image:

                            image.attr('src',e.target.result);
                    };

                    // Reading the file as a DataURL. When finished,
                    // this will trigger the onload function above:
                    reader.readAsDataURL(file);

                    message.hide();
                    preview.appendTo(dropbox);

                    // Associating a preview container
                    // with the file, using jQuery's $.data():

                    $.data(file,preview);
            }

            function showMessage(msg){
                    message.html(msg);
            }

    });
        </script>


        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>
        </head>

    <body class="dashborad">
              <div id="content">
                <div class="inner">
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>Productos </span>

                        </div><!-- End header -->
                        <div class="content">
                            <div class="formEl_b">
                                <a class="uibutton icon special answer" href="indexProducts.php">Todos los productos</a>
                                <a class="uibutton" href="editProduct.php?id=<?php echo $id;?>">Editar datos del producto</a>
                                <a class="uibutton" href="editProductPics.php?id=<?php echo $id;?>">Galeria de Imagenes</a>
                                        <?php if( isset ($_GET['message2']) ){ ?>
                                        <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message2'];?></div>
                                        <?php } ?>
                                        <fieldset>
                                            <legend>
                                                <h1>Subir fotos a <span style="color: red"><?php echo $product->getTitle(); ?></span></h1>
                                            </legend>
                                                  <label>
                                                      tamaño: 460 X 419  px ó proporcional a esta medida.
                                                  </label>
                                                  <div id="dropbox">
                                                        <span class="message">Arrastre y suelte las imagenes aqui!
                                                            <br />
                                                            <i>
                                                                La imagen final será redimensionada a 460 X 419  px!!
                                                            </i>
                                                        </span>
                                                  </div>
                                        </fieldset>
                            </div>


                            <!-- clear fix -->
                            <div class="clear"></div>

                        </div><!-- End content -->
                    </div><!-- End full width -->
                </div> <!--// End inner -->
              </div> <!--// End content -->
</body>
</html>
