<?php
session_start();
//Evita presentar contenidos sin el login debido
@include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
@include("../../../core/class/db.class.php");
//Creamos el nuevo objeto "Database"

include '../define.php';
include '../model/product.class.php';
include '../model/productDAO.class.php';
include '../model/image.php';
include '../model/imageDAO.php';

//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

$db->doQuery("SHOW TABLES LIKE 'cms_products'",SHOW_TABLE_QUERY);
//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
if(!$db->show){
    $location = "location: ./view/index.php?";
    header($location."&error=Products not installed [edit]");
    exit;
}


$productoDAO = new ProductDAO($db);

$id = $_GET['id'];

$product = $productoDAO->getById($id);
$productos = $productoDAO->gets("products_title", "asc");
if($product == null){
    $location = "location: ./index.php?";
    header($location."&error=Product not found [edit]");
    exit;
}


$imageDAO = new ImageDAO($db);
$images = $imageDAO->getsByIdProduct($id, "products_img_id", "asc");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>

		<!--External Files-->
        <link href="http://cms.imaginamos.com/css/generalCMS.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://cms.imaginamos.com/components/flot/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://cms.imaginamos.com/js/generalCMS.js"></script>
        <!--End External Files-->


        </head>

    <body class="dashborad">
        <div id="alertMessage" class="error"></div>
		<!-- Header -->
        <div id="header">
                <div id="account_info">
                    <?php include("../../../menu/administrator.php"); ?>
                </div>
            </div><!-- End Header -->
			<div id="shadowhead"></div>

              <div id="left_menu">
                    <ul id="main_menu" class="main_menu">
						<?php include("../../../menu/index.php"); ?>
                    </ul>
              </div>

              <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <ul id="shortcut">
								<?php include("../../../menu/icons.php"); ?>
                            </ul>
					</div>
                    <div class="clear"></div>

					<!-- full width -->
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>Productos</span>

                        </div><!-- End header -->
                        <div class="content">
                            <?php if( isset ($_GET['message']) ){ ?>
                            <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message'];?></div>
                            <?php } ?>
                            <a name="images"></a>
                            <div class="formEl_b">
                                <div>
                                    <form id="formnews" method="post" enctype="multipart/form-data" action="../controller/productEditImages.php">
                                        <br /><a class="uibutton icon special answer" href="indexProducts.php">Todos los productos</a>
                                        <a class="uibutton" href="editProduct.php?id=<?php echo $id;?>">Editar datos del producto</a>
                                        <a class="uibutton" href="editProductPics2.php?id=<?php echo $id;?>">Subir Imagenes [+]</a>
                                        <p>&nbsp;</p>
                                        <fieldset>
                                            <legend><h1>Editar Imagenes de <span style="color: red"><?php echo $product->getTitle(); ?></span></h1></legend>
                                            <h3>Añadir imagenes</h3>
                                              <div>
                                                  <div>
                                                      <label>Subir una imagen. Tamaño: 460 x 419 px</label><br />
                                                      <input type="file" name="file" />
                                                      <input type="submit" value="Guardar" />
                                                      <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                                  </div>
                                              </div>
                                            <div>
                                                <br />&nbsp;
                                                <h3>Unir imagenes</h3>
                                                <span style="font-size: x-small">*Solo una unión</span>
                                                <form action="../controller/productEditImagesId.php" method="post">
                                                <table>
                                                    <tr>
                                                        <td width="120">Productos Español</td>
                                                        <td>
                                                            <select name="imgId" >
                                                                <option value="0">Ninguna</option>
                                                            <?php foreach($productos as $p){ if( $p->getLang() != "es" ) continue; ?>
                                                                <option value="<?php echo $p->getId(); ?>" <?php if( $product->getImgId() == $p->getId() ) echo 'selected'; ?> >
                                                                    <?php echo $p->getTitle(); ?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>
                                                            <input type="hidden" name="id" value="<?php echo $product->getId() ?>" />
                                                        </td>
                                                        <td>
                                                            <input type="submit" value="Unir" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                </form>

                                                <form action="../controller/productEditImagesId.php" method="post">
                                                <table>
                                                    <tr>
                                                        <td width="120">Productos Ruso</td>
                                                        <td>
                                                            <select name="imgId" >
                                                                <option value="0">Ninguna</option>
                                                            <?php foreach($productos as $p){ if( $p->getLang() != "ru" ) continue; ?>
                                                                <option value="<?php echo $p->getId(); ?>" <?php if( $product->getImgId() == $p->getId() ) echo 'selected'; ?> >
                                                                    <?php echo $p->getTitle(); ?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>
                                                            <input type="hidden" name="id" value="<?php echo $product->getId() ?>" />
                                                        </td>
                                                        <td>
                                                            <input type="submit" value="Unir" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                </form>
                                                <form action="../controller/productEditImagesId.php" method="post">
                                                <table>
                                                    <tr>
                                                        <td width="120">Productos Inglés</td>
                                                        <td>
                                                            <select name="imgId" >
                                                                <option value="0">Ninguna</option>
                                                            <?php foreach($productos as $p){ if( $p->getLang() != "en" ) continue; ?>
                                                                <option value="<?php echo $p->getId(); ?>" <?php if( $product->getImgId() == $p->getId() ) echo 'selected'; ?> >
                                                                    <?php echo $p->getTitle(); ?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>
                                                            <input type="hidden" name="id" value="<?php echo $product->getId() ?>" />
                                                        </td>
                                                        <td>
                                                            <input type="submit" value="Unir" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                </form>
                                                
                                            </div>
                                              <p>&nbsp;</p>
                                               <div>
                                                   <h3>Galeria</h3>
                                                   <table width="100%">
                                                       <tr>
                                                       <?php $i=0; foreach($images as $image){ ?>
                                                           <td width="160">
                                                               <img alt="" src="../files/<?php echo $image->getPath();?>" width="150" />
                                                               <br />
                                                               <a href="../controller/productDeleteImage.php?id=<?php echo $id?>&idm=<?php echo $image->getId()?>">
                                                                   Eliminar
                                                               </a>
                                                           </td>
                                                       <?php $i++; if($i%5 == 0) echo '</tr><tr>'; } ?>
                                                       </tr>
                                                   </table>
                                               </div>
                                        </fieldset>
                                        <p>&nbsp;</p>
                                    </form>
                                    </div>
                            </div>


                            <!-- clear fix -->
                            <div class="clear"></div>

                        </div><!-- End content -->
                    </div><!-- End full width -->



					<!-- clear fix -->
					<div class="clear"></div>

                    <div id="footer"> &copy; Copyright 2012 <span class="tip"><a  href="#" title="Todos los derechos reservados" >imaginamos.com</a> </span> </div>

                </div> <!--// End inner -->
              </div> <!--// End content -->

              <script language="javascript">$("#article").cleditor();</script>

</body>
</html>
