<?php
$queryCreateTable[0] = "
CREATE TABLE cms_products_config (
products_config_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
products_config_funcionality int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
products_config_date datetime DEFAULT NULL COMMENT 'Fecha y hora de instalación módulo',
PRIMARY KEY (products_config_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
/////////////////////////////////////////////////////////////////////////

//product
$queryCreateTable[1] = "
CREATE TABLE cms_products (
products_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',

products_id_cat int(11) unsigned NOT NULL COMMENT 'id cat',
products_id_subcat int(11) unsigned NOT NULL COMMENT 'id sub-cat',

products_title varchar(100) DEFAULT NULL COMMENT 'Product title',
products_price varchar(80) DEFAULT NULL COMMENT 'Product price',
products_ref varchar(80) DEFAULT NULL COMMENT 'Product ref',
products_type TEXT COMMENT 'Product types tag',
products_color TEXT COMMENT 'Product colors tag',
products_variety TEXT COMMENT 'Product varietyes tag',
products_stared BOOL DEFAULT 0 COMMENT 'Product stared',
products_description varchar(200) DEFAULT 0 COMMENT 'Product stared',

products_image1 varchar(80) DEFAULT NULL COMMENT 'Product main image',
products_image2 varchar(80) DEFAULT NULL COMMENT 'Product image',
products_image3 varchar(80) DEFAULT NULL COMMENT 'Product image',
products_image4 varchar(80) DEFAULT NULL COMMENT 'Product image',
products_image5 varchar(80) DEFAULT NULL COMMENT 'Product image',

products_lang varchar(20) DEFAULT NULL COMMENT 'Product Lang',

PRIMARY KEY (products_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";


//category
$queryCreateTable[2] = "
CREATE TABLE cms_products_cat (
products_cat_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
products_cat_title varchar(120) DEFAULT NULL COMMENT 'cat title',
products_cat_sub_title varchar(120) DEFAULT NULL COMMENT 'cat subtitle',
products_cat_lang varchar(20) DEFAULT 'es' COMMENT 'cat lang',
products_cat_image varchar(120) DEFAULT NULL COMMENT 'cat image',
products_cat_image_big varchar(120) DEFAULT NULL COMMENT 'cat image big',
products_cat_description TEXT DEFAULT NULL COMMENT 'cat title',
PRIMARY KEY (products_cat_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";

//sub category
$queryCreateTable[3] = "
CREATE TABLE cms_products_subcat (
products_subcat_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
products_subcat_id_cat int(11) unsigned NOT NULL COMMENT 'id cat',
products_subcat_title varchar(120) DEFAULT NULL COMMENT 'subcat title',
products_subcat_lang varchar(20) DEFAULT 'es' COMMENT 'cat lang',
products_subcat_image varchar(120) DEFAULT NULL COMMENT 'cat image',
PRIMARY KEY (products_subcat_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";

//product images
$queryCreateTable[4] = "
CREATE TABLE cms_products_img (
products_img_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
products_img_id_product int(11) unsigned NOT NULL COMMENT 'id producto al que se liga',
products_img_order int(11) unsigned NOT NULL COMMENT 'image order',
products_img_path varchar(120) DEFAULT NULL COMMENT 'image path',
PRIMARY KEY (products_img_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
?>