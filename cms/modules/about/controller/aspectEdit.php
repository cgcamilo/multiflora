<?php
session_start();

//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/aspect.class.php';
include '../model/aspectDAO.class.php';
include '../includes/simpleImages.php';
include '../includes/random.php';


$db = new Database();
$db->connect();

$aspectDAO = new AspectDAO($db);

foreach ($_POST as $key => $valuep) {
    $$key = $valuep;
}


$location = "location: ./../view/index.php?";

$aspect = $aspectDAO->getById($id);

if( $aspect == null ){
    header($location."&error=Link not found");
    exit;
}

if( $aspect->getType() != 'IMAGE' )
    $aspect->setValue($value);
else
    if( $_FILES['file']['name'] != ""){

        $fileName = $_FILES['file']['name'];
        //borro la imagne anterior (si tenia)
        if( $aspect->getValue() != "" ){
            $filename = $destino = './../files/'.$aspect->getValue();
            unlink($filename);
        }

        $key = cRandom(4);
        $destino = './../files/'.$key.'.'.$fileName;
           //cambio el tamaño de la imagen en Historia
        if( $id ==  13 || $id ==  14 || $id ==  15){
            $image = new SimpleImage();
            $image->load($_FILES[ 'file' ][ 'tmp_name' ]);
            $image->resize(622, 364);
            $image->save($destino);
            $aspect->setValue($key.'.'.$fileName);

            //crop 1
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/h1_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 0, 0, 622, 121);

            //crop 2
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/h2_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 0, 121, 622, 121);

            //crop 3
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/h3_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 0, 242, 622, 121);
            

        }else if( $id ==  16 || $id ==  17 || $id ==  18 || $id ==  23 || $id ==  24 || $id ==  25){
            $image = new SimpleImage();
            $image->load($_FILES[ 'file' ][ 'tmp_name' ]);
            $image->resize(973, 240);
            $image->save($destino);

            $aspect->setValue($key.'.'.$fileName);
        }else{
            $image = new SimpleImage();
            $image->load($_FILES[ 'file' ][ 'tmp_name' ]);
            $image->resize(220, 82);
            $image->save($destino);

            $aspect->setValue($key.'.'.$fileName);
        }
    }

$aspectDAO->update($aspect);


header($location."&notification-".$id."=Datos actualizados#noti-".$id);
exit;
?>