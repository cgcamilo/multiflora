<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/aspect.class.php';
include '../model/aspectDAO.class.php';
include '../includes/simpleImages.php';
include '../includes/random.php';
@include '../define.php';

$db = new Database();
$db->connect();

$location = "location: ./../view/index.php?";

$db->doQuery("SELECT ".TABLE_NAME."_config_funcionality FROM cms_".TABLE_NAME."_config WHERE ".TABLE_NAME."_config_id = 1",SELECT_QUERY);
$open = $db->results[0][TABLE_NAME.'_config_funcionality'];

if( !$open ){
    $aspectDAO->delete($id);
    header($location."&error=Configuration closed");
    exit;
}

foreach($_GET as $key => $value){
    $$key = $value;
}

$aspect = new Aspect();
$aspect->setTitle($title);
$aspect->setDescription($decription);
$aspect->setType($type);

$aspectDAO = new AspectDAO($db);
$aspectDAO->save($aspect);

header($location."&message=Aspecto dinamico añadido");
exit;
?>