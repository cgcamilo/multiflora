<?php
session_start();

//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/aspect.class.php';
include '../model/aspectDAO.class.php';
include '../includes/simpleImages.php';
include '../includes/random.php';


$db = new Database();
$db->connect();

$aspectDAO = new AspectDAO($db);

foreach ($_POST as $key => $valuep) {
    $$key = $valuep;
}


$location = "location: ./../view/index.php?";

$aspect = $aspectDAO->getById($id);

if( $aspect == null ){
    header($location."&error=Link not found");
    exit;
}

if( $aspect->getType() != 'IMAGE' )
    $aspect->setValue($value);
else
    if( $_FILES['file']['name'] != ""){

        $fileName = $_FILES['file']['name'];
        //borro la imagne anterior (si tenia)
        if( $aspect->getValue() != "" ){
            $filename = $destino = './../files/'.$aspect->getValue();
            unlink($filename);
        }

        $key = cRandom(4);
        $destino = './../files/'.$key.'.'.$fileName;
           //cambio el tamaño de la imagen en Historia
        
            $image = new SimpleImage();
            $image->load($_FILES[ 'file' ][ 'tmp_name' ]);
            $image->resize(649,387);
            $image->save($destino);
            $aspect->setValue($key.'.'.$fileName);

            //crop 1
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/v1_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 0, 0, 417, 116);

            //crop 2
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/v2_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 0, 116, 417, 120);

            //crop 4
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/v4_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 0, 232, 417, 126);

            //crop 3
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/v3_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 417, 0, 204, 237);

            //crop 5
            $image = new SimpleImage();
            $image->load($destino);
            $destinoCrop = './../files/v5_'.$key.'.'.$fileName;
            $image->simpleCrop($destinoCrop, 417, 238, 204, 127);
            
    }

$aspectDAO->update($aspect);


header($location."&notification-".$id."=Datos actualizados#noti-".$id);
exit;
?>