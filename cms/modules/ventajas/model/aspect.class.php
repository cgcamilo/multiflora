<?php
////////////////////////////////
//Camilo Cifuentes
//info@ccamilo.com
//Lappeenranta, Finlandia, 2012
////////////////////////////////
class Aspect
{
    private $id;
    private $title;
    private $description;
    private $value;
    private $type;

    function __construct()
    {}
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }
}
?>





















