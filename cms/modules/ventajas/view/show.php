<?php
$aspectDAO = new AspectDAO($db);
$aspects = $aspectDAO->gets(TABLE_NAME."_id", "asc");


    foreach($aspects as $aspect){
?>
<a name="noti-<?php echo $aspect->getId()?>"></a>
<br />&nbsp;
<form id="formnews" method="post" enctype="multipart/form-data" action="../controller/aspectEdit.php">
            <fieldset>
                <legend><?php echo $aspect->getTitle()?></legend>
                <?php echo $aspect->getDescription();?>
                <div>
                    <div>
                        <?php if($aspect->getType() == 'INPUT'){ ?>
                            <input type="text" name="value"  value="<?php echo $aspect->getValue();?>"  class="large"/>
                        <?php } ?>
                        <?php if($aspect->getType() == 'IMAGE'){ ?>
                            <?php if($aspect->getValue() != ""){ ?>
                            <img src="../files/<?php echo $aspect->getValue();?>" width="300" alt="" />
                            <br />
                            <?php } ?>
                            <input type="file" name="file"  />
                        <?php } ?>
                        <?php if($aspect->getType() == 'TEXT'){ $textareas[]='text'.$aspect->getId(); ?>
                            <textarea name="value" id="<?php echo 'text'.$aspect->getId();?>" cols="5" rows="3" class="large"><?php echo $aspect->getValue();?></textarea>
                        <?php } ?>
                        <?php if($aspect->getType() == 'TEXTAREA'){  ?>
                            <textarea name="value" cols="5" rows="3" class="large"><?php echo $aspect->getValue();?></textarea>
                        <?php } ?>
                    </div>
                    <br />
                    <input type="hidden" name="id" value="<?php echo $aspect->getId()?>" />
                    <input type="hidden" name="notification" value="<?php echo $aspect->getId()?>" />
                    <input type="submit" class="uibutton icon add" value="Guardar" />

                    <?php if($open){ ?>
                    <a class="uibutton special" href="../controller/aspectDel.php?id=<?php echo $aspect->getId()?>">Delete aspect</a>
                    <?php } ?>
                    <?php if( isset ($_GET['notification-'.$aspect->getId()]) ){ ?>
                    <div id="notification-<?php echo $aspect->getId()?>" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['notification-'.$aspect->getId()];?></div>
                    <?php } ?>
                </div>
            </fieldset>
        </form>
<?php } ?>