<?php
@include '../define.php';
class LinkCertificacionesDAO{

    private $db;
    private $funcionality;

    function __construct(Database $db, $funcionality){
            $this->db = $db;
            $this->funcionality = $funcionality;
    }

    function save(Link $link){

        if( $this->funcionality == 1 )
            $querty =   'insert into cms_'.TABLE_NAME_certificaciones.'
                    ('.TABLE_NAME_certificaciones.'_title , '.TABLE_NAME_certificaciones.'_url , '.TABLE_NAME_certificaciones.'_blank, '.TABLE_NAME_certificaciones.'_order, '.TABLE_NAME_certificaciones.'_description, '.TABLE_NAME_certificaciones.'_lang)
                    values(
                    "'.$link->getTitle().'",
                    "'.$link->getUrl().'",
                    "'.$link->getBlank().'",
                    "'.$link->getOrder().'",
                    "'.$link->getDescription().'",
                    "'.$link->getLang().'"
                    )';
        else
            $querty =   'insert into cms_'.TABLE_NAME_certificaciones.'
                    ('.TABLE_NAME_certificaciones.'_title , '.TABLE_NAME_certificaciones.'_url , '.TABLE_NAME_certificaciones.'_blank, '.TABLE_NAME_certificaciones.'_order, '.TABLE_NAME_certificaciones.'_image, '.TABLE_NAME_certificaciones.'_description, '.TABLE_NAME_certificaciones.'_lang)
                    values(
                    "'.$link->getTitle().'",
                    "'.$link->getUrl().'",
                    "'.$link->getBlank().'",
                    "'.$link->getOrder().'",
                    "'.$link->getImage().'",
                    "'.$link->getDescription().'",
                    "'.$link->getLang().'"
                    )';


       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }


    function gets($order, $orderType){

        $sql = 'SELECT * from cms_'.TABLE_NAME_certificaciones.'  ';
        $sql .= 'order by '.TABLE_NAME_certificaciones.'_'.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $links=array();

        if(count( $results ) == 0){
            return $links;
        }

        $i = 0;
        foreach ($results as $result){
            $link = new Link();
            $link->setId($result[''.TABLE_NAME_certificaciones.'_id']);
            $link->setTitle($result[''.TABLE_NAME_certificaciones.'_title']);
            $link->setUrl($result[''.TABLE_NAME_certificaciones.'_url']);
            $link->setBlank($result[''.TABLE_NAME_certificaciones.'_blank']);
            $link->setOrder($result[''.TABLE_NAME_certificaciones.'_order']);
            $link->setLang($result[''.TABLE_NAME_certificaciones.'_lang']);
            $link->setDescription($result[''.TABLE_NAME_certificaciones.'_description']);
            if( $this->funcionality == 2)
                $link->setImage ($result[''.TABLE_NAME_certificaciones.'_image']);

            $links[$i] = $link;
            $i++;
        }


        return $links;
    }

    function getById($id){

        $sql = 'SELECT * from cms_'.TABLE_NAME_certificaciones.' WHERE '.TABLE_NAME_certificaciones.'_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $link = new Link();
        $link->setId($result[''.TABLE_NAME_certificaciones.'_id']);
        $link->setTitle($result[''.TABLE_NAME_certificaciones.'_title']);
        $link->setUrl($result[''.TABLE_NAME_certificaciones.'_url']);
        $link->setBlank($result[''.TABLE_NAME_certificaciones.'_blank']);
        $link->setLang($result[''.TABLE_NAME_certificaciones.'_lang']);
        $link->setOrder($result[''.TABLE_NAME_certificaciones.'_order']);
        $link->setDescription($result[''.TABLE_NAME_certificaciones.'_description']);
        if( $this->funcionality == 2)
            $link->setImage ($result[''.TABLE_NAME_certificaciones.'_image']);

        return $link;
    }


    function delete($id){

        $sql = 'Delete from cms_'.TABLE_NAME_certificaciones.' WHERE '.TABLE_NAME_certificaciones.'_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }

    //TODO
    function update(link $link){

        if( $this->funcionality == 1 )
        $sql =   "UPDATE
                     cms_".TABLE_NAME_certificaciones."
                    SET
                    ".TABLE_NAME_certificaciones."_title =
                    \"".$link->getTitle()."\",
                    ".TABLE_NAME_certificaciones."_url =
                    \"".mysql_real_escape_string($link->getUrl())."\",
                    ".TABLE_NAME_certificaciones."_blank =
                    \"".$link->getBlank()."\",
                    ".TABLE_NAME_certificaciones."_lang =
                    \"".$link->getLang()."\",
                    ".TABLE_NAME_certificaciones."_description =
                    \"".$link->getDescription ()."\",
                    ".TABLE_NAME_certificaciones."_order =
                    \"".$link->getOrder()."\"
                    WHERE ".TABLE_NAME_certificaciones."_id =
                    ".mysql_real_escape_string($link->getId())."
                    ";
        else
            $sql =   "UPDATE
                     cms_".TABLE_NAME_certificaciones."
                    SET
                    ".TABLE_NAME_certificaciones."_title =
                    \"".$link->getTitle()."\",
                    ".TABLE_NAME_certificaciones."_url =
                    \"".$link->getUrl()."\",
                    ".TABLE_NAME_certificaciones."_blank =
                    \"".$link->getBlank()."\",
                    ".TABLE_NAME_certificaciones."_lang =
                    \"".$link->getLang()."\",
                    ".TABLE_NAME_certificaciones."_description =
                    \"".$link->getDescription ()."\",
                    ".TABLE_NAME_certificaciones."_order =
                    \"".$link->getOrder()."\",
                    ".TABLE_NAME_certificaciones."_image =
                    \"".$link->getImage()."\"
                    WHERE ".TABLE_NAME_certificaciones."_id =
                    ".mysql_real_escape_string($link->getId())."
                    ";
        //echo $sql;
        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_'.TABLE_NAME_certificaciones.';';
        if($opt == 1)
                $sql = 'select count(*) from cms_'.TABLE_NAME_certificaciones.' where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];

    }


}

?>
