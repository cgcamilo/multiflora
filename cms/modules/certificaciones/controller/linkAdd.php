<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/link.class.php';
include '../model/linkDAO.class.php';
include '../includes/simpleImages.php';
include '../includes/random.php';
@include '../define.php';

$db = new Database();
$db->connect();

$db->doQuery("SELECT ".TABLE_NAME."_config_funcionality FROM cms_".TABLE_NAME."_config WHERE ".TABLE_NAME."_config_id = 1",SELECT_QUERY);
$config = $db->results[0][''.TABLE_NAME.'_config_funcionality'];

$location = "location: ./../view/index.php?";

$linkDAO = new LinkDAO($db, $config);

foreach ($_POST as $key => $value) {
    $$key = $value;
}

$link = new Link();
$link->setTitle($title);
$link->setLang($lang);
$link->setOrder($order);
$link->setUrl($url);
$link->setDescription($description);
$link->setBlank($blank);
$link->setOrder(1);

if( $title == "" ){
    header($location."&message=Item no añadido. el Título y URL son campos obligatorios!");
    exit;
}

if( $config == 2 && $_FILES['file']['name'] != ""){

    $fileName = $_FILES['file']['name'];

    $key = cRandom(4);
    $destino = './../files/'.$key.'.'.$fileName;
       //cambio el tamaño de la imagen
    $image2 = new SimpleImage();
    $image2->load($_FILES[ 'file' ][ 'tmp_name' ]);
    $image2->resize(75, 75);
    $image2->save($destino);

    $link->setImage ($key.'.'.$fileName);
}

$linkDAO->save($link);


header($location."&message=Item añadido");
exit;
?>