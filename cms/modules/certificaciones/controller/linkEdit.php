<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/link.class.php';
include '../model/linkDAO.class.php';
include '../includes/simpleImages.php';
include '../includes/random.php';
@include '../define.php';

$db = new Database();
$db->connect();

$db->doQuery("SELECT ".TABLE_NAME."_config_funcionality FROM cms_".TABLE_NAME."_config WHERE ".TABLE_NAME."_config_id = 1",SELECT_QUERY);
$config = $db->results[0]["".TABLE_NAME."_config_funcionality"];

$linkDAO = new LinkDAO($db, $config);

foreach ($_POST as $key => $value) {
    $$key = $value;
}

$location = "location: ./../view/edit.php?id=".$id;

$link = $linkDAO->getById($id);

if( $link == null ){
    header($location."&error=Item not found");
    exit;
}

$link->setTitle($title);
$link->setLang($lang);
$link->setDescription($description);
//$link->setOrder($order);
$link->setUrl($url);
$link->setBlank($blank);

if( $title == "" ){
    header($location."&message=Item no actualizado. el Título y URL son campos obligatorios!");
    exit;
}

if( $config == 2 && $_FILES['file']['name'] != ""){

    $fileName = $_FILES['file']['name'];
    //borro la imagne anterior (si tenia)
    if( $link->getImage() != "" ){
        $filename = $destino = './../files/'.$link->getImage();
        unlink($filename);
    }

    $key = cRandom(4);
    $destino = './../files/'.$key.'.'.$fileName;
       //cambio el tamaño de la imagen
    $image2 = new SimpleImage();
    $image2->load($_FILES[ 'file' ][ 'tmp_name' ]);
    $image2->resize(75, 75);
    $image2->save($destino);

    $link->setImage ($key.'.'.$fileName);
}

$linkDAO->update($link);


header($location."&message=Item actualizado");
exit;
?>