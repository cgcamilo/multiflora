<?php
session_start();

include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include 'includes.php';

$db = new Database();
$db->connect();

$id = $_GET['id'];

$userDAO = new UserDAO($db);

$user = $userDAO->getById($id);
if( $user == null ){
    header($location."&error=User not found");
    exit;
}

$userDAO->delete($id);

$location = "location: ./../view/index.php?";
header($location."&message=Usuario eliminado");
exit;

?>