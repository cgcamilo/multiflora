<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include 'includes.php';

$db = new Database();
$db->connect();

$userDAO = new UserDAO($db);

foreach ($_POST as $key => $value) {
    $$key = $value;
}

$location = "location: ./../view/edit.php?id=".$id;

$user = $userDAO->getById($id);

if( $user == null ){
    header($location."&error=User not found");
    exit;
}

$user->setCountry($country);
$user->setCity($city);
$user->setZip($zip);
$user->setCorta($corta);
$user->setDir2($dir2);
$user->setCompany($company);
$user->setDir($dir);
$user->setEmail($email);
$user->setIdNumber($idNumber);
$user->setIdType($idType);
$user->setLastName($lastName);
$user->setName($name);
$user->setRole($role);
$user->setTel($tel);
$user->setValidated($validated);

$userDAO->update($user);


header($location."&message=Usuario actualizado");
exit;
?>