<?php
////////////////////////////////
//Camilo Cifuentes
//info@ccamilo.com
//Lappeenranta, Finlandia, 2012
////////////////////////////////
class User
{
    private $id;

    private $userName;
    private $pass;
    private $validated;

    private $name;
    private $lastName;
    private $email;
    private $company;
    private $role;

    private $idType;
    private $idNumber;

    private $city;
    private $tel;
    private $dir;

    private $country;
    private $state;
    private $zip;
    private $dir2;
    private $corta;

    private $lang;


    private $date;

    function __construct()
    {}

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getCompany() {
        return $this->company;
    }

    public function setCompany($company) {
        $this->company = $company;
    }

    public function getRole() {
        return $this->role;
    }

    public function setRole($role) {
        $this->role = $role;
    }

    public function getIdType() {
        return $this->idType;
    }

    public function setIdType($idType) {
        $this->idType = $idType;
    }

    public function getIdNumber() {
        return $this->idNumber;
    }

    public function setIdNumber($idNumber) {
        $this->idNumber = $idNumber;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getTel() {
        return $this->tel;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function getDir() {
        return $this->dir;
    }

    public function setDir($dir) {
        $this->dir = $dir;
    }

    public function getUserName() {
        return $this->userName;
    }

    public function setUserName($userName) {
        $this->userName = $userName;
    }

    public function getPass() {
        return $this->pass;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function getValidated() {
        return $this->validated;
    }

    public function setValidated($validated) {
        $this->validated = $validated;
    }

    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
    }


    function getDateFormat(){
        $fechaYhora = explode(" ", $this->getDate());
        $fecha = explode("-", $fechaYhora[0]);
        $hora = explode(":", $fechaYhora[1]);
        switch ($fecha[1]){
            case '01':
                $mes = "Jan";
                break;
            case '02':
                $mes = "Feb";
                break;
            case '03':
                $mes = "Mar";
                break;
            case '04':
                $mes = "Abr";
                break;
            case '05':
                $mes = "May";
                break;
            case '06':
                $mes = "Jun";
                break;
            case '07':
                $mes = "Jul";
                break;
            case '08':
                $mes = "Ago";
                break;
            case '09':
                $mes = "Sep";
                break;
            case '10':
                $mes = "Oct";
                break;
            case '11':
                $mes = "Nov";
                break;
            case '12':
                $mes = "Dic";
                break;
            default:
                $mes = $fecha[1];
                break;
        }
        return $fecha[2].' '.$mes.' '.$fecha[0];
    }

    public function getCountry() {
        return $this->country;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function getState() {
        return $this->state;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function getZip() {
        return $this->zip;
    }

    public function setZip($zip) {
        $this->zip = $zip;
    }

    public function getDir2() {
        return $this->dir2;
    }

    public function setDir2($dir2) {
        $this->dir2 = $dir2;
    }

    public function getCorta() {
        return $this->corta;
    }

    public function setCorta($corta) {
        $this->corta = $corta;
    }

    public function getLang() {
        return $this->lang;
    }

    public function setLang($lang) {
        $this->lang = $lang;
    }



}
?>