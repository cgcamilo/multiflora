<?php
////////////////////////////////
//Camilo Cifuentes
//info@ccamilo.com
//Lappeenranta, Finlandia, 2012
////////////////////////////////
class UserDAO{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(User $User){

        $querty =   'insert into cms_users
                (users_username, users_pass, users_validated,
                users_name, users_lastname , users_email,
                users_company, users_role, users_idtype, users_id,
                users_city, users_tel, users_dir, users_country, users_state, users_zip, users_dir2,
                users_lang, users_corta)
                values(
                "'.$User->getUserName ().'",
                "'.$User->getPass().'",
                "'.$User->getValidated().'",
                "'.$User->getName().'",
                "'.$User->getLastName().'",
                "'.$User->getEmail().'",
                "'.$User->getCompany().'",
                "'.$User->getRole().'",
                "'.$User->getIdType().'",
                "'.$User->getIdNumber().'",
                "'.$User->getCity ().'",
                "'.$User->getTel().'",
                "'.$User->getDir().'",
                "'.$User->getCountry().'",
                "'.$User->getState().'",
                "'.$User->getZip().'",
                "'.$User->getDir2().'",
                "'.$User->getLang().'",
                "'.$User->getCorta().'"
                )';
       //echo $querty; die;
        $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }

   
    function gets($order, $orderType){

        $sql = 'SELECT * from cms_users  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $users=array();

        if(count( $results ) == 0){
            return $users;
        }

        $i = 0;
        foreach ($results as $result){
            $user = new User();
            $user->setId($result['users_id']);
            $user->setUserName($result['users_username']);
            $user->setPass($result['users_pass']);
            $user->setValidated($result['users_validated']);
            $user->setName($result['users_name']);
            $user->setLastName($result['users_lastname']);
            $user->setEmail($result['users_email']);
            $user->setCompany($result['users_company']);
            $user->setRole($result['users_role']);
            $user->setIdType($result['users_idtype']);
            $user->setIdNumber($result['users_idnumber']);
            $user->setCity($result['users_city']);
            $user->setTel($result['users_tel']);
            $user->setDir($result['users_dir']);
            $user->setDate($result['users_date']);
            $user->setCountry($result['users_country']);
            $user->setState($result['users_state']);
            $user->setZip($result['users_zip']);
            $user->setDir2($result['users_dir2']);
            $user->setCorta($result['users_corta']);
            $user->setLang($result['users_lang']);
            
            $users[$i] = $user;
            $i++;
        }


        return $users;
    }

    function getsByLang($lang, $order, $orderType){

        $sql = 'SELECT * from cms_users  ';
        $sql .= 'WHERE users_lang = "'.$lang.'" ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $users=array();

        if(count( $results ) == 0){
            return $users;
        }

        $i = 0;
        foreach ($results as $result){
            $user = new User();
            $user->setId($result['users_id']);
            $user->setUserName($result['users_username']);
            $user->setPass($result['users_pass']);
            $user->setValidated($result['users_validated']);
            $user->setName($result['users_name']);
            $user->setLastName($result['users_lastname']);
            $user->setEmail($result['users_email']);
            $user->setCompany($result['users_company']);
            $user->setRole($result['users_role']);
            $user->setIdType($result['users_idtype']);
            $user->setIdNumber($result['users_idnumber']);
            $user->setCity($result['users_city']);
            $user->setTel($result['users_tel']);
            $user->setDir($result['users_dir']);
            $user->setDate($result['users_date']);
            $user->setCountry($result['users_country']);
            $user->setState($result['users_state']);
            $user->setZip($result['users_zip']);
            $user->setDir2($result['users_dir2']);
            $user->setCorta($result['users_corta']);
            $user->setLang($result['users_lang']);

            $users[$i] = $user;
            $i++;
        }


        return $users;
    }

    function getById($id){

        $sql = 'SELECT * from cms_users WHERE users_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $user = new User();
        $user->setId($result['users_id']);
        $user->setUserName($result['users_username']);
        $user->setPass($result['users_pass']);
        $user->setValidated($result['users_validated']);
        $user->setName($result['users_name']);
        $user->setLastName($result['users_lastname']);
        $user->setEmail($result['users_email']);
        $user->setCompany($result['users_company']);
        $user->setRole($result['users_role']);
        $user->setIdType($result['users_idtype']);
        $user->setIdNumber($result['users_idnumber']);
        $user->setCity($result['users_city']);
        $user->setTel($result['users_tel']);
        $user->setDir($result['users_dir']);
        $user->setDate($result['users_date']);
        $user->setCountry($result['users_country']);
        $user->setState($result['users_state']);
        $user->setZip($result['users_zip']);
        $user->setDir2($result['users_dir2']);
        $user->setCorta($result['users_corta']);
        $user->setLang($result['users_lang']);
            
        return $user;
    }

   
    function delete($id){

        $sql = 'Delete from cms_users WHERE users_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }

    //TODO
    function update(User $user){


        $sql =   "UPDATE
                 cms_users
                SET
                users_name =
                \"".$user->getName()."\",
                users_lastname =
                \"".$user->getLastName()."\",
                users_validated =
                \"".$user->getValidated()."\",
                users_email =
                \"".$user->getEmail()."\",
                users_company =
                \"".$user->getCompany()."\",
                users_role =
                \"".$user->getRole()."\",
                users_idtype =
                \"".$user->getIdType()."\",
                users_idnumber =
                \"".$user->getIdNumber()."\",
                users_city =
                \"".$user->getCity()."\",
                users_country =
                \"".$user->getCountry()."\",
                users_zip =
                \"".$user->getZip()."\",
                users_dir2 =
                \"".$user->getDir2()."\",
                users_corta =
                \"".$user->getCorta()."\",
                users_dir =
                \"".$user->getDir()."\",
                users_tel =
                \"".$user->getTel()."\"
                WHERE users_id =
                ".$user->getId()."
                ";
        //echo $sql; die;
        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }

    function updatePass(User $User){


        $sql =   "UPDATE
                 cms_users
                SET
                users_pass =
                \"".$User->getPass()."\"
                WHERE users_id =
                ".$User->getId()."
                ";

        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }

    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_users;';
        if($opt == 1)
                $sql = 'select count(*) from cms_users where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];
        
    }


}

?>
