<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
include("../../../core/class/db.class.php");
//Creamos el nuevo objeto "Database"
include("includes.php");
//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

$db->doQuery("SHOW TABLES LIKE 'cms_users'",SHOW_TABLE_QUERY);
//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
if(!$db->show){
    $location = "location: ./view/index.php?";
    header($location."&error=User not installed [edit]");
    exit;
}


$id = $_GET['id'];
$userDAO = new UserDAO($db);
$user = $userDAO->getById($id);

if($user == null){
    $location = "location: ./index.php?";
    header($location."&error=User not found [edit]");
    exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>

		<!--External Files-->
        <link href="http://cms.imaginamos.com/css/generalCMS.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://cms.imaginamos.com/components/flot/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://cms.imaginamos.com/js/generalCMS.js"></script>
        <!--End External Files-->


        </head>

        <body class="dashborad">
        <div id="alertMessage" class="error"></div>
		<!-- Header -->
        <div id="header">
                <div id="account_info">
                    <?php include("../../../menu/administrator.php"); ?>
                </div>
            </div><!-- End Header -->
			<div id="shadowhead"></div>

              <div id="left_menu">
                    <ul id="main_menu" class="main_menu">
						<?php include("../../../menu/index.php"); ?>
                    </ul>
              </div>

              <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <ul id="shortcut">
								<?php include("../../../menu/icons.php"); ?>
                            </ul>
					</div>
                    <div class="clear"></div>

					<!-- full width -->
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>LINKS </span>

                        </div><!-- End header -->
                        <div class="content">
                            <?php if( isset ($_GET['message']) ){ ?>
                            <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message'];?></div>
                            <?php } ?>
                          <div class="formEl_b">
                            <fieldset>
                                <div>
                                    <form id="formnews" method="post" enctype="multipart/form-data" action="../controller/userEdit.php">
                                    <legend><h1>Edición de Usuarios</h1></legend>
                                    <br /><a class="uibutton icon special answer" href="index.php">Volver</a>
                                        <p>&nbsp;</p>
                                        <fieldset>
                                              <div>
                                                  <div><label>Nombre</label><br /><input type="text" name="name" id="title" value="<?php echo $user->getName();?>"  class="large"/></div><br />
                                                  <div><label>Apellidos</label><br /><input type="text" name="lastName" id="title" value="<?php echo $user->getLastName();?>"  class="large"/></div><br />
                                                  <div><label>E-mail</label><br /><input type="text" name="email" id="title" value="<?php echo $user->getEmail();?>"  class="large"/></div><br />
                                                  <div><label>Compañía</label><br /><input type="text" name="company" id="title" value="<?php echo $user->getCompany();?>"  class="large"/></div><br />
                                                  <div><label>Puesto</label><br /><input type="text" name="role" id="title" value="<?php echo $user->getRole();?>"  class="large"/></div><br />

                                                  <div><label>Teléfono</label><br /><input type="text" name="tel" id="title" value="<?php echo $user->getTel();?>"  class="large"/></div><br />
                                                  <div><label>Dirección</label><br /><input type="text" name="dir" id="title" value="<?php echo $user->getDir();?>"  class="large"/></div><br />
                                                  <div><label>Pais</label><br /><input type="text" name="country" id="title" value="<?php echo $user->getCountry();?>"  class="large"/></div><br />
                                                  <div><label>Ciudad</label><br /><input type="text" name="city" id="title" value="<?php echo $user->getCity();?>"  class="large"/></div><br />
                                                  <div><label>Código Postal</label><br /><input type="text" name="zip" id="title" value="<?php echo $user->getZip();?>"  class="large"/></div><br />
                                                  <div><label>Dirección 2</label><br /><input type="text" name="dir2" id="title" value="<?php echo $user->getDir2();?>"  class="large"/></div><br />
                                                  <div><label>Comentario</label><br /><textarea  rows="4" cols="40" name="corta" class="large"><?php echo $user->getCorta();?></textarea></div><br />
                                                  <input type="hidden" name="id" value="<?php echo $id;?>" />
                                                  <input type="submit" class="uibutton icon add" value="Guardar" />
                                              </div>
                                              <p>&nbsp;</p>
                                               <div>

                                              </div>
                                        </fieldset>
                                        <p>&nbsp;</p>
                                    </form>
                                    </div>
                            </fieldset>
                            </div>
                            <!-- clear fix -->
                            <div class="clear"></div>

                        </div><!-- End content -->
                    </div><!-- End full width -->



					<!-- clear fix -->
					<div class="clear"></div>

                    <div id="footer"> &copy; Copyright 2012 <span class="tip"><a  href="#" title="Todos los derechos reservados" >imaginamos.com</a> </span> </div>

                </div> <!--// End inner -->
              </div> <!--// End content -->

              <script language="javascript">$("#article").cleditor();</script>

</body>
</html>
