<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
include("../../../core/class/db.class.php");

include("includes.php");

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=usuarios.xls");
header("Pragma: no-cache");
header("Expires: 0");

//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

$userDAO = new UserDAO($db);
if( !isset($_GET['lang']) )
    $users = $userDAO->gets("users_username", "asc");
else{
    $users = $userDAO->getsByLang($_GET['lang'], "users_lang", "asc");
}
?>
    <table>
          <tr>
            <th>Idioma</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>E-mail</th>
            <th>Empresa</th>
            <th>Cargo</th>
            <th>Direcci&oacute;n</th>
            <th>Ciudad</th>
            <th>Tel&eacute;fono</th>
            <th>Pa&iacute;s</th>
            <th>C&oacute;digo posta</th>
            <th>Direcci&oacute;n</th>
            <th>Comentario</th>
          </tr>
        <?php
        foreach ($users as $item) {
        ?>
          <tr>
            <td ><?php echo $item->getLang();?></td>
            <td ><?php echo $item->getName();?></td>
            <td ><?php echo $item->getLastName();?></td>
            <td ><?php echo $item->getEmail();?></td>
            <td >
                <?php echo $item->getCompany();?>
            </td>
            <td >
                <?php echo $item->getRole();?>
            </td>
            <td ><?php echo $item->getDir();?></td>
            <td ><?php echo $item->getCity();?></td>
            <td ><?php echo $item->getTel();?></td>
            <td ><?php echo $item->getCountry();?></td>
            <td ><?php echo $item->getZip();?></td>
            <td ><?php echo $item->getDir2();?></td>
            <td ><?php echo $item->getCorta();?></td>
          </tr>
      <?php } ?>

      </table>
