<?php
$userDAO = new UserDAO($db);
$users = $userDAO->gets("users_lang", "asc");
?>
<a href="showExport.php">Exportar usuarios: TODOS</a><br />
<a href="showExport.php?lang=es">Exportar usuarios: ES</a><br />
<a href="showExport.php?lang=en">Exportar usuarios: EN</a><br />
<a href="showExport.php?lang=ru">Exportar usuarios: RU</a><br />
<a name="linkShow"></a>
<div class="tableName toolbar">
    <table class="display data_table2" >
        <thead>
                <tr>
                        <th><div class="th_wrapp">Idioma</div></th>
                        <th><div class="th_wrapp">Nombre completo</div></th>
                        <th><div class="th_wrapp">Email / Teléfono / Pais</div></th>
                        <th><div class="th_wrapp">Fecha registro</div></th>
                        <th><div class="th_wrapp">Acciones</div></th>
                </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user){ ?>
            <tr class="odd gradeX">
                <td class="center" width="40px"><?php echo $user->getLang();?></td>
                <td class="center" width="120px">
                   <?php echo $user->getName().' '.$user->getLastName();?>
                </td>
                <td class="center" width="100px">
                    <?php echo $user->getEmail();?>
                    <br />
                    <?php echo $user->getTel();?>
                    <br />
                    <?php echo $user->getCountry();?>
                </td>
                <td class="center" width="100px"><?php echo $user->getDateFormat();?></td>
                <td class="center" width="100px">
                    <a class="Delete uibutton special" href="../controller/userDelete.php?id=<?php echo $user->getId()?>">Eliminar</a>
                    <a  href="edit.php?id=<?php echo $user->getId()?>" class="uibutton">Editar</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>