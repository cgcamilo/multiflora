<?php
$queryCreateTable[0] = "
CREATE TABLE cms_users_config (
users_config_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
users_config_funcionality int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
users_config_date datetime DEFAULT NULL COMMENT 'Fecha y hora de instalación módulo',
PRIMARY KEY (users_config_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
/////////////////////////////////////////////////////////////////////////


$queryCreateTable[1] = "
CREATE TABLE cms_users (
users_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
users_username varchar(100) DEFAULT NULL COMMENT 'User username',
users_pass varchar(100) DEFAULT NULL COMMENT 'User pass',
users_validated BOOLEAN DEFAULT 0 COMMENT 'User validated',
users_name varchar(100) DEFAULT NULL COMMENT 'User name',
users_lastname varchar(100) DEFAULT NULL COMMENT 'User lastname',
users_email varchar(100) DEFAULT NULL COMMENT 'User email',
users_company varchar(100) DEFAULT NULL COMMENT 'User company',
users_role varchar(100) DEFAULT NULL COMMENT 'User company role',
users_idtype varchar(100) DEFAULT NULL COMMENT 'User identification type',
users_idnumber varchar(100) DEFAULT NULL COMMENT 'User identification number',
users_city varchar(100) DEFAULT NULL COMMENT 'User city',
users_tel varchar(100) DEFAULT NULL COMMENT 'User phone',
users_dir varchar(100) DEFAULT NULL COMMENT 'User address',
users_country varchar(100) DEFAULT NULL COMMENT 'User ',
users_state varchar(100) DEFAULT NULL COMMENT 'User ',
users_zip varchar(100) DEFAULT NULL COMMENT 'User ',
users_dir2 varchar(100) DEFAULT NULL COMMENT 'User ',
users_corta varchar(100) DEFAULT NULL COMMENT 'User ',
users_lang varchar(100) DEFAULT NULL COMMENT 'User ',
users_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'User registration date',
PRIMARY KEY (users_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";

?>