<?php
@include '../define.php';
class AspectHomeDAO{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(Aspect $aspect){

        $querty = 'insert into cms_'.TABLE_NAME_HOME.'
                ('.TABLE_NAME_HOME.'_title , '.TABLE_NAME_HOME.'_description , '.TABLE_NAME_HOME.'_value, '.TABLE_NAME_HOME.'_type)
                values(
                "'.mysql_real_escape_string($aspect->getTitle()).'",
                "'.mysql_real_escape_string($aspect->getDescription()).'",
                "'.mysql_real_escape_string($aspect->getValue()).'",
                "'.mysql_real_escape_string($aspect->getType()).'"
                )';

       //echo $querty;
       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }


    function gets($order, $orderType){

        $sql = 'SELECT * from cms_'.TABLE_NAME_HOME.'  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $aspects=array();

        if(count( $results ) == 0){
            return $aspects;
        }

        $i = 0;
        foreach ($results as $result){
            $aspect = new Aspect();
            $aspect->setId($result[''.TABLE_NAME_HOME.'_id']);
            $aspect->setTitle($result[''.TABLE_NAME_HOME.'_title']);
            $aspect->setDescription($result[''.TABLE_NAME_HOME.'_description']);
            $aspect->setValue($result[''.TABLE_NAME_HOME.'_value']);
            $aspect->setType($result[''.TABLE_NAME_HOME.'_type']);

            $aspects[$i] = $aspect;
            $i++;
        }


        return $aspects;
    }

    function getById($id){

        $sql = 'SELECT * from cms_'.TABLE_NAME_HOME.' WHERE '.TABLE_NAME_HOME.'_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $aspect = new Aspect();
        $aspect->setId($result[''.TABLE_NAME_HOME.'_id']);
        $aspect->setTitle($result[''.TABLE_NAME_HOME.'_title']);
        $aspect->setDescription($result[''.TABLE_NAME_HOME.'_description']);
        $aspect->setValue($result[''.TABLE_NAME_HOME.'_value']);
        $aspect->setType($result[''.TABLE_NAME_HOME.'_type']);

        return $aspect;
    }

    function getByName($name){

        $sql = 'SELECT * from cms_'.TABLE_NAME_HOME.' WHERE '.TABLE_NAME_HOME.'_title = "'.$name.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $aspect = new Aspect();
        $aspect->setId($result[''.TABLE_NAME_HOME.'_id']);
        $aspect->setTitle($result[''.TABLE_NAME_HOME.'_title']);
        $aspect->setDescription($result[''.TABLE_NAME_HOME.'_description']);
        $aspect->setValue($result[''.TABLE_NAME_HOME.'_value']);
        $aspect->setType($result[''.TABLE_NAME_HOME.'_type']);

        return $aspect;
    }


    function delete($id){

        $sql = 'Delete from cms_'.TABLE_NAME_HOME.' WHERE '.TABLE_NAME_HOME.'_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }

    //TODO
    function update(Aspect $aspect){


        $sql =   "UPDATE
                     cms_".TABLE_NAME_HOME."
                    SET
                    ".TABLE_NAME_HOME."_value =
                    \"".mysql_real_escape_string($aspect->getValue())."\"
                    WHERE ".TABLE_NAME_HOME."_id =
                    ".mysql_real_escape_string($aspect->getId())."
                    ";

        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_'.TABLE_NAME_HOME.';';
        if($opt == 1)
                $sql = 'select count(*) from cms_'.TABLE_NAME_HOME.' where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];

    }


}

?>