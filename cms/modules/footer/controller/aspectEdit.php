<?php
session_start();

//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/aspect.class.php';
include '../model/aspectDAO.class.php';
include '../includes/simpleImages.php';
include '../includes/random.php';


$db = new Database();
$db->connect();

$aspectDAO = new AspectDAO($db);

foreach ($_POST as $key => $valuep) {
    $$key = $valuep;
}


$location = "location: ./../view/index.php?";

$aspect = $aspectDAO->getById($id);

if( $aspect == null ){
    header($location."&error=Link not found");
    exit;
}

if( $aspect->getType() != 'IMAGE' &&  $aspect->getType() != 'FILE')
    $aspect->setValue($value);
else if($aspect->getType() == 'FILE' && $_FILES['file']['name'] != ""){
        $fileName = $_FILES['file']['name'];
        //borro la imagne anterior (si tenia)
        if( $aspect->getValue() != "" ){
            $filename = $destino = './../files/'.$aspect->getValue();
            @unlink($filename);
        }

        $key = cRandom(4);
        $destino = './../files/'.$key.'.'.$fileName;
        if(move_uploaded_file($_FILES[ 'file' ][ 'tmp_name' ], $destino))
            $aspect->setValue($key.'.'.$fileName);
    }else if( $_FILES['file']['name'] != ""){

        $fileName = $_FILES['file']['name'];
        //borro la imagne anterior (si tenia)
        if( $aspect->getValue() != "" ){
            $filename = $destino = './../files/'.$aspect->getValue();
            unlink($filename);
        }

        $key = cRandom(4);
        $destino = './../files/'.$key.'.'.$fileName;
        //cambio el tamaño de la imagen
        $image2 = new SimpleImage();
        $image2->load($_FILES[ 'file' ][ 'tmp_name' ]);
        $image2->resize(977, 270);
        $image2->save($destino);

        $aspect->setValue($key.'.'.$fileName);
    }

$aspectDAO->update($aspect);


header($location."&notification-".$id."=Datos actualizados#noti-".$id);
exit;
?>