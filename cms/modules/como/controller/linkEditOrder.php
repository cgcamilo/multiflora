<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include '../model/link.class.php';
include '../model/linkDAO.class.php';
@include '../define.php';

$db = new Database();
$db->connect();

$db->doQuery("SELECT ".TABLE_NAME."_config_funcionality FROM cms_".TABLE_NAME."_config WHERE ".TABLE_NAME."_config_id = 1",SELECT_QUERY);
$config = $db->results[0]["".TABLE_NAME."_config_funcionality"];

$id = $_GET['id'];
$type = $_GET['type'];


$linkDAO = new LinkDAO($db, $config);

$link = $linkDAO->getById($id);
if( $link == null ){
    header($location."&error=Item not found");
    exit;
}

if( $type == 'up' )
    $link->setOrder ($link->getOrder () - 1);
else
    $link->setOrder ($link->getOrder () + 1);

$linkDAO->update($link);

$location = "location: ./../view/index.php?";
header($location."&message=Orden actualizado#linkShow");
exit;


?>
