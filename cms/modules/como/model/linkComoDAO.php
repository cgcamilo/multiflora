<?php
@include '../define.php';
class LinkComoDAO{

    private $db;
    private $funcionality;

    function __construct(Database $db, $funcionality){
            $this->db = $db;
            $this->funcionality = $funcionality;
    }

    function save(Link $link){

        if( $this->funcionality == 1 )
            $querty =   'insert into cms_'.TABLE_NAME_como.'
                    ('.TABLE_NAME_como.'_title , '.TABLE_NAME_como.'_url , '.TABLE_NAME_como.'_blank, '.TABLE_NAME_como.'_order, '.TABLE_NAME_como.'_lang, '.TABLE_NAME_como.'_description)
                    values(
                    "'.$link->getTitle().'",
                    "'.$link->getUrl().'",
                    "'.$link->getBlank().'",
                    "'.$link->getOrder().'",
                    "'.$link->getLang().'",
                    "'.$link->getDescription().'"
                    )';
        else
            $querty =   'insert into cms_'.TABLE_NAME_como.'
                    ('.TABLE_NAME_como.'_title , '.TABLE_NAME_como.'_url , '.TABLE_NAME_como.'_blank, '.TABLE_NAME_como.'_order, '.TABLE_NAME_como.'_image, '.TABLE_NAME_como.'_lang, '.TABLE_NAME_como.'_description)
                    values(
                    "'.$link->getTitle().'",
                    "'.$link->getUrl().'",
                    "'.$link->getBlank().'",
                    "'.$link->getOrder().'",
                    "'.$link->getImage().'",
                    "'.$link->getLang().'",
                    "'.$link->getDescription().'"
                    )';


       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }


    function gets($order, $orderType){

        $sql = 'SELECT * from cms_'.TABLE_NAME_como.'  ';
        $sql .= 'order by '.TABLE_NAME_como.'_'.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $links=array();

        if(count( $results ) == 0){
            return $links;
        }

        $i = 0;
        foreach ($results as $result){
            $link = new Link();
            $link->setId($result[''.TABLE_NAME_como.'_id']);
            $link->setTitle($result[''.TABLE_NAME_como.'_title']);
            $link->setUrl($result[''.TABLE_NAME_como.'_url']);
            $link->setBlank($result[''.TABLE_NAME_como.'_blank']);
            $link->setOrder($result[''.TABLE_NAME_como.'_order']);
            $link->setLang($result[''.TABLE_NAME_como.'_lang']);
            $link->setDescription($result[''.TABLE_NAME_como.'_description']);
            if( $this->funcionality == 2)
                $link->setImage ($result[''.TABLE_NAME_como.'_image']);

            $links[$i] = $link;
            $i++;
        }


        return $links;
    }

    function getById($id){

        $sql = 'SELECT * from cms_'.TABLE_NAME_como.' WHERE '.TABLE_NAME_como.'_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $link = new Link();
        $link->setId($result[''.TABLE_NAME_como.'_id']);
        $link->setTitle($result[''.TABLE_NAME_como.'_title']);
        $link->setUrl($result[''.TABLE_NAME_como.'_url']);
        $link->setBlank($result[''.TABLE_NAME_como.'_blank']);
        $link->setOrder($result[''.TABLE_NAME_como.'_order']);
        $link->setLang($result[''.TABLE_NAME_como.'_lang']);
        $link->setDescription($result[''.TABLE_NAME_como.'_description']);
        if( $this->funcionality == 2)
            $link->setImage ($result[''.TABLE_NAME_como.'_image']);

        return $link;
    }


    function delete($id){

        $sql = 'Delete from cms_'.TABLE_NAME_como.' WHERE '.TABLE_NAME_como.'_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }

    //TODO
    function update(link $link){

        if( $this->funcionality == 1 )
        $sql =   "UPDATE
                     cms_".TABLE_NAME_como."
                    SET
                    ".TABLE_NAME_como."_title =
                    \"".$link->getTitle()."\",
                    ".TABLE_NAME_como."_url =
                    \"".mysql_real_escape_string($link->getUrl())."\",
                    ".TABLE_NAME_como."_blank =
                    \"".$link->getBlank()."\",
                    ".TABLE_NAME_como."_lang =
                    \"".$link->getLang()."\",
                    ".TABLE_NAME_como."_description =
                    \"".$link->getDescription ()."\",
                    ".TABLE_NAME_como."_order =
                    \"".$link->getOrder()."\"
                    WHERE ".TABLE_NAME_como."_id =
                    ".mysql_real_escape_string($link->getId())."
                    ";
        else
            $sql =   "UPDATE
                     cms_".TABLE_NAME_como."
                    SET
                    ".TABLE_NAME_como."_title =
                    \"".$link->getTitle()."\",
                    ".TABLE_NAME_como."_url =
                    \"".$link->getUrl()."\",
                    ".TABLE_NAME_como."_blank =
                    \"".$link->getBlank()."\",
                    ".TABLE_NAME_como."_lang =
                    \"".$link->getLang()."\",
                    ".TABLE_NAME_como."_description =
                    \"".$link->getDescription ()."\",
                    ".TABLE_NAME_como."_order =
                    \"".$link->getOrder()."\",
                    ".TABLE_NAME_como."_image =
                    \"".$link->getImage()."\"
                    WHERE ".TABLE_NAME_como."_id =
                    ".mysql_real_escape_string($link->getId())."
                    ";
        //echo $sql;
        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_'.TABLE_NAME_como.';';
        if($opt == 1)
                $sql = 'select count(*) from cms_'.TABLE_NAME_como.' where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];

    }


}

?>
