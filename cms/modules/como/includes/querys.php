<?php
@include '../define.php';
$queryCreateTable[0] = "
CREATE TABLE cms_".TABLE_NAME."_config (
".TABLE_NAME."_config_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
".TABLE_NAME."_config_funcionality int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
".TABLE_NAME."_config_date datetime DEFAULT NULL COMMENT 'Fecha y hora de instalación módulo',
PRIMARY KEY (".TABLE_NAME."_config_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
/////////////////////////////////////////////////////////////////////////

//option 1: id, title, url, blank, order
$queryCreateTable[1] = "
CREATE TABLE cms_".TABLE_NAME." (
".TABLE_NAME."_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
".TABLE_NAME."_title varchar(80) DEFAULT NULL COMMENT 'Link title',
".TABLE_NAME."_url varchar(80) DEFAULT NULL COMMENT 'Link URL',
".TABLE_NAME."_blank BOOLEAN DEFAULT 0 COMMENT 'Open in new blank page',
".TABLE_NAME."_order int(5) unsigned DEFAULT 0 COMMENT 'Link order',
".TABLE_NAME."_lang varchar(10) DEFAULT NULL COMMENT 'Link lang',
".TABLE_NAME."_description TEXT COMMENT 'Link description',
PRIMARY KEY (".TABLE_NAME."_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";


//option 2: id, title, url, blank, order, image
$queryCreateTable[2] = "
CREATE TABLE cms_".TABLE_NAME." (
".TABLE_NAME."_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
".TABLE_NAME."_title varchar(120) DEFAULT NULL COMMENT 'Link title',
".TABLE_NAME."_url varchar(120) DEFAULT NULL COMMENT 'Link URL',
".TABLE_NAME."_blank BOOLEAN DEFAULT 0 COMMENT 'Open in new blank page',
".TABLE_NAME."_order int(5) unsigned DEFAULT 0 COMMENT 'Link order',
".TABLE_NAME."_image varchar(120) DEFAULT NULL COMMENT 'Link image',
".TABLE_NAME."_lang varchar(10) DEFAULT NULL COMMENT 'Link lang',
".TABLE_NAME."_description TEXT COMMENT 'Link description',
PRIMARY KEY (".TABLE_NAME."_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
?>