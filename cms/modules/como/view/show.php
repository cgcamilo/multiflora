<?php
$linkDAO = new LinkDAO($db, $config);
$links = $linkDAO->gets("order", "asc");
?>
<a name="linkShow"></a>
<div class="tableName toolbar">
    <table class="display data_table2" >
        <thead>
                <tr>
                        <th><div class="th_wrapp">Idioma</div></th>
                        <th><div class="th_wrapp">Título</div></th>
                        <th><div class="th_wrapp">URL</div></th>
                        <?php if( $config == 2 ){ ?><th><div class="th_wrapp">Imagen</div></th><?php } ?>
                        <th><div class="th_wrapp">Acciones</div></th>
                </tr>
        </thead>
        <tbody>
        <?php foreach ($links as $link){ ?>
            <tr class="odd gradeX">
                <td class="center" width="80px">
                    <?php echo $link->getLang();?>
                </td>
                <td class="center" width="100px"><?php echo $link->getTitle();?></td>
                <td class="center" width="100px"><?php echo $link->getUrl();?></td>
                <?php if( $config == 2 ){ ?>
                <td class="center" width="80px">
                    <?php if( $link->getImage() != "" ){ ?>
                    <img alt="" src="../files/<?php echo $link->getImage();?>" width="60" />
                    <?php } ?>
                </td>
                <?php } ?>
                <td class="center" width="100px">
                    <a class="Delete uibutton special" href="../controller/linkDelete.php?id=<?php echo $link->getId()?>">Eliminar</a>
                    <a  href="edit.php?id=<?php echo $link->getId()?>" class="uibutton">Editar</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>