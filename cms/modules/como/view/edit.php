<?php
session_start();
//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
//Carga las funciones generales en XAJAX para la actualización de contenidos
include("../../../core/class/db.class.php");
//Creamos el nuevo objeto "Database"

include("../model/link.class.php");
include("../model/linkDAO.class.php");
//Carga conexión e interacción con la base de datos
$db = new Database();
//Conectamos
$db->connect();

$db->doQuery("SHOW TABLES LIKE 'cms_".TABLE_NAME."'",SHOW_TABLE_QUERY);
//Si recibimos TRUE como respuesta quiere decir que si existe la tabla
if(!$db->show){
    $location = "location: ./view/index.php?";
    header($location."&error=Links not installed [edit]");
    exit;
}

$db->doQuery("SELECT ".TABLE_NAME."_config_funcionality FROM cms_".TABLE_NAME."_config WHERE ".TABLE_NAME."_config_id = 1",SELECT_QUERY);
$config = $db->results[0]["".TABLE_NAME."_config_funcionality"];

$id = $_GET['id'];
$linkDAO = new LinkDAO($db, $config);
$link = $linkDAO->getById($id);

if($link == null){
    $location = "location: ./index.php?";
    header($location."&error=Item not found [edit]");
    exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>CMS imaginamos.com - Todos los derechos reservados</title>

        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/favicon2.ico"/>

		<!--External Files-->
        <link href="http://cms.imaginamos.com/css/generalCMS.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://cms.imaginamos.com/components/flot/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="http://cms.imaginamos.com/js/generalCMS.js"></script>
        <!--End External Files-->


        </head>

        <body class="dashborad">
        <div id="alertMessage" class="error"></div>
		<!-- Header -->
        <div id="header">
                <div id="account_info">
                    <?php include("../../../menu/administrator.php"); ?>
                </div>
            </div><!-- End Header -->
			<div id="shadowhead"></div>

              <div id="left_menu">
                    <ul id="main_menu" class="main_menu">
						<?php include("../../../menu/index.php"); ?>
                    </ul>
              </div>

              <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <ul id="shortcut">
								<?php include("../../../menu/icons.php"); ?>
                            </ul>
					</div>
                    <div class="clear"></div>

					<!-- full width -->
                    <div class="widget" >
                        <div class="header"><span ><span class="ico gray pictures_folder"></span>DESTACADOS </span>

                        </div><!-- End header -->
                        <div class="content">
                            <?php if( isset ($_GET['message']) ){ ?>
                            <div id="notification" style="background-color: #dbf262; color: black; width: 800px; padding: 20px; border: 1px solid #FFD700"><?php echo $_GET['message'];?></div>
                            <?php } ?>
                          <div class="formEl_b">
                            <fieldset>
                                <div>
                                    <form id="formnews" method="post" enctype="multipart/form-data" action="../controller/linkEdit.php">
                                    <legend><h1>Edición de imagenes</h1></legend>
                                    <br /><a class="uibutton icon special answer" href="index.php">Volver</a>
                                        <p>&nbsp;</p>
                                        <fieldset>
                                              <div>
                                                  <label>Idioma</label><br />
                                                  <select name="lang">
                                                      <option value="es" <?php if( $link->getLang() == 'es' ) echo 'selected'; ?>>Español</option>
                                                      <option value="en" <?php if( $link->getLang() == 'en' ) echo 'selected'; ?>>English</option>
                                                      <option value="ru" <?php if( $link->getLang() == 'ru' ) echo 'selected'; ?>>Pусский</option>
                                                  </select>
                                                  <br />
                                                  <div><label>Título</label><br /><input type="text" name="title" id="title" value="<?php echo $link->getTitle();?>"  class="large"/></div><br />
                                                      <br />
                                                      <?php if($config == 2){ ?>
                                                      <div><label>Image</label> [973x 240 px]<br />
                                                          <input type="file" name="file" />
                                                          <br />
                                                          <?php if( $link->getImage() != "" ){ ?>
                                                          <img alt="" src="../files/<?php echo $link->getImage();?>" width="200" />
                                                          <?php } ?>
                                                      </div>
                                                      <br />
                                                      <?php } ?>
                                                      <input type="hidden" name="id" value="<?php echo $id;?>" />
                                                      <input type="submit" class="uibutton icon add" value="Guardar" />
                                              </div>
                                              <p>&nbsp;</p>
                                               <div>

                                              </div>
                                        </fieldset>
                                        <p>&nbsp;</p>
                                    </form>
                                    </div>
                            </fieldset>
                            </div>
                            <!-- clear fix -->
                            <div class="clear"></div>

                        </div><!-- End content -->
                    </div><!-- End full width -->



					<!-- clear fix -->
					<div class="clear"></div>

                    <div id="footer"> &copy; Copyright 2012 <span class="tip"><a  href="#" title="Todos los derechos reservados" >imaginamos.com</a> </span> </div>

                </div> <!--// End inner -->
              </div> <!--// End content -->

              <script language="javascript">$("#article").cleditor();</script>

</body>
</html>
