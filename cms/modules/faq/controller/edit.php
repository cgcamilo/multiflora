<?php
session_start();

//Evita presentar contenidos sin el login debido
include("../../../security/secure.php");
include("../../../core/class/db.class.php");
include 'includes.php';
include '../includes/simpleImages.php';
include '../includes/random.php';


$db = new Database();
$db->connect();

$DAO = new EstructuraDAO($db);

foreach ($_POST as $key => $valuep) {
    $$key = $valuep;
}


$location = "location: ./../view/index.php?id=".$id;

$item = $DAO->getById($id);
$item->setTitle($title);
$item->setDescription($description);
$item->setType($type);
$item->setValue($value);

if( $item == null ){
    header($location."&error=Item not found");
    exit;
}


$location = "location: ./../view/edit.php?id=".$id;

$DAO->update($item);


header($location."&message=Datos actualizados");
exit;
?>