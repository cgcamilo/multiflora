<div>
    <form id="forminstall" method="get" action="../controller/installModule.php">
        
        <legend><h1>Configuración del módulo</h1></legend>
        <p>&nbsp;</p>

        <fieldset>
         <div>
                <p><label>Qué tipo de funcionalidad desea instalar?</label></p>
         </div>
         <div>
                  <p>
                  Este módulo se configura añadiendo textos, imagenes y otros datos de forma dinámica,
                  cuando estos datos estén ya definidos usted puede cerrar la opción para no poder añadir más datos.
                  </p>
          </div>
          </fieldset>

          <p>&nbsp;</p>
        <fieldset>
          <div>
                <p><label>Instalar datos de ejemplo?</label></p>
         </div>
         <div>
                  <p>
                        <label>
                            <input type="radio" name="example" value="1" id="dataExample_0" checked />Si</label>
                        <label>
                          <input type="radio" name="example" value="0" id="dataExample_1" />No</label>
                  </p>
          </div>
                </fieldset>
        <p>&nbsp;</p>
        <input type="submit" class="uibutton special" value="Instalar Módulo" />
    </form>
</div>