<?php
$DAO = new EstructuraDAO($db);
$items = $DAO->gets("title", "asc");
?>
<a name="linkShow"></a>
<div class="tableName toolbar">
    <table class="display data_table2" >
        <thead>
                <tr>
                    <th><div class="th_wrapp">Idioma</div></th>
                    <th><div class="th_wrapp">Título</div></th>
                    <th><div class="th_wrapp">Respuesta</div></th>
                    <th><div class="th_wrapp">Acciones</div></th>
                </tr>
        </thead>
        <tbody>
        <?php foreach ($items as $item){ ?>
            <tr class="odd gradeX">
                <td class="center" width="100px"><?php echo $item->getType();?></td>
                <td class="center" width="200px">
                   <?php echo $item->getTitle();?>
                </td>
                <td class="center" width="100px">
                   <?php echo $item->getDescruptionShortNoTags(100);?>
                </td>
                <td class="center" width="100px">
                    <a class="Delete uibutton special" href="../controller/del.php?id=<?php echo $item->getId()?>">Eliminar</a>
                    <a  href="edit.php?id=<?php echo $item->getId()?>" class="uibutton">Editar</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>