<?php
@include_once '../define.php';
$queryCreateTable[0] = "
CREATE TABLE cms_".TABLE_NAME."_config (
".TABLE_NAME."_config_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
".TABLE_NAME."_config_funcionality int(1) NOT NULL COMMENT 'Permitir añadir mas campos dinamicos',
".TABLE_NAME."_config_date datetime DEFAULT NULL COMMENT 'Fecha y hora de instalación módulo',
PRIMARY KEY (".TABLE_NAME."_config_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
/////////////////////////////////////////////////////////////////////////

//option 1: id, title, url, blank, order
$queryCreateTable[1] = "
CREATE TABLE cms_".TABLE_NAME." (
".TABLE_NAME."_id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id unico',
".TABLE_NAME."_title varchar(80) DEFAULT NULL COMMENT '".TABLE_NAME." title',
".TABLE_NAME."_description TEXT COMMENT '".TABLE_NAME." description',
".TABLE_NAME."_value TEXT COMMENT 'field value',
".TABLE_NAME."_type varchar(20) COMMENT '".TABLE_NAME." type',
".TABLE_NAME."_image varchar(80) COMMENT '".TABLE_NAME." type',
PRIMARY KEY (".TABLE_NAME."_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
?>