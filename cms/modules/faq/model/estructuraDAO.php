<?php
@include '../define.php';
class EstructuraDAO{

    private $db;

    function __construct(Database $db){
            $this->db = $db;
    }

    function save(Estructura $estructura){

        $querty = 'insert into cms_'.TABLE_NAME.'
                ('.TABLE_NAME.'_title , '.TABLE_NAME.'_description , '.TABLE_NAME.'_value, '.TABLE_NAME.'_type, '.TABLE_NAME.'_image)
                values(
                "'.mysql_real_escape_string($estructura->getTitle()).'",
                "'.mysql_real_escape_string($estructura->getDescription()).'",
                "'.mysql_real_escape_string($estructura->getValue()).'",
                "'.mysql_real_escape_string($estructura->getType()).'",
                "'.mysql_real_escape_string($estructura->getImage()).'"
                )';

       $this->db->doQuery($querty,INSERT_QUERY);

    }

    function getLastId(){
        return $this->db->getLastId();
    }

   
    function gets($order, $orderType){

        $sql = 'SELECT * from cms_'.TABLE_NAME.'  ';
        $sql .= 'order by '.TABLE_NAME.'_'.$order.' '.$orderType.' ';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        $estructuras=array();

        if(count( $results ) == 0){
            return $estructuras;
        }

        $i = 0;
        foreach ($results as $result){
            $estructura = new Estructura();
            $estructura->setId($result[''.TABLE_NAME.'_id']);
            $estructura->setTitle($result[''.TABLE_NAME.'_title']);
            $estructura->setDescription($result[''.TABLE_NAME.'_description']);
            $estructura->setValue($result[''.TABLE_NAME.'_value']);
            $estructura->setType($result[''.TABLE_NAME.'_type']);
            $estructura->setImage($result[''.TABLE_NAME.'_image']);
            
            $estructuras[$i] = $estructura;
            $i++;
        }


        return $estructuras;
    }

    function getById($id){

        $sql = 'SELECT * from cms_'.TABLE_NAME.' WHERE '.TABLE_NAME.'_id = "'.$id.'"';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;


        if(count( $results ) == 0){
            return null;
        }

        $result = $results[0];
        $estructura = new Estructura();
        $estructura->setId($result[''.TABLE_NAME.'_id']);
        $estructura->setTitle($result[''.TABLE_NAME.'_title']);
        $estructura->setDescription($result[''.TABLE_NAME.'_description']);
        $estructura->setValue($result[''.TABLE_NAME.'_value']);
        $estructura->setType($result[''.TABLE_NAME.'_type']);
        $estructura->setImage($result[''.TABLE_NAME.'_image']);
        
        return $estructura;
    }

   
    function delete($id){

        $sql = 'Delete from cms_'.TABLE_NAME.' WHERE '.TABLE_NAME.'_id = '.$id.' ';
        $this->db->doQuery($sql,DELETE_QUERY);
    }

    //TODO
    function update(Estructura $estructura){

         
        $sql =   "UPDATE
                     cms_".TABLE_NAME."
                    SET
                    ".TABLE_NAME."_value =
                    \"".mysql_real_escape_string($estructura->getValue())."\",
                    ".TABLE_NAME."_type =
                    \"".mysql_real_escape_string($estructura->getType())."\",
                    ".TABLE_NAME."_title =
                    \"".mysql_real_escape_string($estructura->getTitle())."\",
                    ".TABLE_NAME."_description =
                    \"".mysql_real_escape_string($estructura->getDescription())."\",
                    ".TABLE_NAME."_image =
                    \"".mysql_real_escape_string($estructura->getImage())."\"
                    WHERE ".TABLE_NAME."_id =
                    ".mysql_real_escape_string($estructura->getId())."
                    ";

        $this->db->doQuery($sql,UPDATE_QUERY);

        return true;
    }


    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
                $sql = 'select count(*) from cms_'.TABLE_NAME.';';
        if($opt == 1)
                $sql = 'select count(*) from cms_'.TABLE_NAME.' where '.$campo.' LIKE "%'.$valor.'%";';

        $this->db->doQuery($sql,SELECT_QUERY);
        $results = $this->db->results;
        return $results[0]['count(*)'];
        
    }


}

?>