<?php
@include 'cms/modules/products/model/categoryDAOext.php';
@include 'cms/modules/products/model/categoryExt.php';
@include 'utiles.php';

$db = new Database();
$db->connect();

$catExtDAO = new CategoryDAOext($db);
$catsExt = $catExtDAO->getsByLang($lang, "order", "asc");
?>

<div class="header">
  <div class="header-cont">
    <div class="header-nav">
    
                
      <div class="header-nav-i"> <a href="contacto.php">
        <div class="ayuda"><?php echo $generalLang['ayuda'];?></div>
        </a> <a href="#">
        <div class="idioma">
          <div class="clear"></div>
          <div class="idioma-act"><?php echo $generalLang['idioma'];?>: <span class="lan-sel"><?php echo $generalLang['lang'];?></span></div>
          <div class="op-lang" id="esp"><a href="index.php?lang=es"><?php echo $generalLang['español'];?></a></div>
          <div class="op-lang" id="eng"><a href="index.php?lang=en"><?php echo $generalLang['ingles'];?></a></div>
          <div class="op-lang" id="rus"><a href="index.php?lang=ru"><?php echo $generalLang['ruso'];?></a></div>
        </div>
        </a> 
      	<div class="ingresar">
        	<a href="#"><div class="login"><?php echo $generalLang['pedido'];?></div></a>
        	<div class="campo-ing">
                    <form action="http://multiflora.vida18.com/formslogin.asp" method="post">
                	<input class="usuario" id="usuario" type="text" name="alias" value="Usuario" onblur="if(this.value=='') this.value='Usuario';" onfocus="if(this.value=='Usuario') this.value='';" >
                        <input class="pasw" id="pasw" type="password" name="Pwd" value="Contraseña" onblur="if(this.value=='') this.value='Contraseña';" onfocus="if(this.value=='Contraseña') this.value='';" >
                        <input TYPE="hidden" NAME="strURL" VALUE="/provider/Orders.asp" />
                        <input TYPE="hidden" NAME="dologproc" VALUE="y" />
                        <input class="bot-ing" id="bot-ing" type="submit" value="<?php echo $generalLang['ingresar'];?>">
                    <a href="registro.php"><div class="reg-cont"><?php echo $generalLang['registrate'];?></div></a>
                    <div class="rec-cont">¿Olvidó su contraseña?</div>
                </form>
            </div>
           
        </div>
        <a href="registro.php"><div class="registro"><?php echo $generalLang['registrate'];?></div></a>
        <a href="#"><div class="carrito">Carrito</div></a>
      </div>
      <div class="header-nav-d">
        
        <!--<div class="cont-header-form">
          <form>
            <input class="usuario" type="text" name="usuario" value"Usuario">
            <input class="password" type="text" name="password" value"Contraseña">
            <input class="bot-entrar" id="bot-entrar" type="submit" value="Entrar">
          </form>
        </div>
        <div class="clear"></div>
        <div class="rec-pas">Olvidó su contraseña?</div>
        <div class="registro">Regístrate</div> -->
      </div>
    </div>
    <div class="clear"></div>
    <a href="index.php" class="logo">
    <div >Multiflora Farm Direct Experts</div>
    </a>
    <div class="menu-princ">
      <ul>
        <li id="b1"><a href="index.php"><?php echo $generalLang['inicio'];?></a></li>
        <li id="b2"><a href="about.php"><?php echo $generalLang['nosotros'];?></a></li>
        <li id="b3"><a href="#"><?php echo $generalLang['productos'];?></a></li>
        <li id="b4" style="display:none;"><a href="subastas.php">Subastas</a></li>
        <!--<li id="b5"><a href="#">Envíos</a></li> -->
        <li id="b6"><a href="noticias.php"><?php echo $generalLang['noticias'];?></a></li>
        <li id="b7"><a href="contacto.php"><?php echo $generalLang['contactenos'];?></a></li>
        
      </ul>
      
      <div class="buscar" id="buscar"> <a href="#">
        <div class="bot-buscar" id="bot-buscar"></div>
        </a>
        <div class="campo-buscar" id="campo-buscar">
        <form action="resultado.php" method="get">
          <input class="busqueda" id="busqueda" type="text" name="buscar" value="Buscar flor" onblur="if(this.value=='') this.value='Buscar flor';" onfocus="if(this.value=='Buscar flor') this.value='';" >
          <a href="#">
          <input class="ini-buscar" id="ini-buscar" type="submit" value="">
          </a> 
      	</form>    
      	</div>
      </div>
      
    </div>
    
    <div class="clear"></div>
    <div class="pastilla" id="past"></div>
    
  </div>
  <div class="sub-menu">
 	<div class="sub-slide-selector">
    <ul>
      <?php foreach($catsExt as $catExt){ ?>
      <li>
        <a href="productos.php?idCat=<?php echo $catExt->getId();?>">
            <div class="otra">
                <img src="cms/modules/products/files/<?php echo $catExt->getImage();?>" alt="" />
            </div>
        </a>
        <div class="mini-txt-sub" name="nombre"><?php echo $catExt->getTitle();?></div>
        <div class="mini-txt2-sub" name="especies">
            <?php
            if(!class_exists(ProductDAO)){
                @include 'cms/modules/products/model/productDAO.class.php';
                @include 'cms/modules/products/model/product.class.php';
            }
            $productDAO = new ProductDAO($db);
            $ps = $productDAO->getByIdCat($catExt->getId());
            ?>
              <?php echo count($ps);?> <?php echo $generalLang['variedades'];?>
        </div>
      </li>
      <?php } ?>
    </ul>
  </div>
  </div>

</div>
