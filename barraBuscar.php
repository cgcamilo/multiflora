<?php
//el headr ya me da las categorias
$colores = array();
$productos = array();
foreach($catsExt as $catExt){
    $productosSelect = $productDAO->getByIdCat($catExt->getId());
    foreach($productosSelect as $productSelect){
        $productos[] = $productSelect;
        $cs = explode(" ", $productSelect->getColor());
        foreach($cs as $c){
            if(!in_array(strtolower($c), $colores) )
                    $colores[] = strtolower($c);
        }
    }
}
?>
<div class="tituloWrapper">

	<div class="tituloMain"><?php echo $generalLang['producto'];?>

    <div class="filterBox">
    	<div class="busc-tit"><?php echo $generalLang['buscarproducto'];?></div>
        <div class="selectBox">
            <select  style="width:240px;" id="cat" class="comboBox1" name="cat" onchange="xajax_updateProductsSelect(cat.options[cat.options.selectedIndex].value, color.options[color.options.selectedIndex].value);return false;">
                      <option selected="selected" value="0"><?php echo $generalLang['tipo'];?></option>
                      <?php foreach($catsExt as $catExt){?>
                      <option value="<?php echo $catExt->getId();?>"><?php echo $catExt->getTitle();?></option>
                      <?php } ?>
            </select>
        </div>
        <div class="selectBox">
            <select  style="width:240px;"  id="color" class="comboBox1" name="color" onchange="xajax_updateProductsSelect(cat.options[cat.options.selectedIndex].value, color.options[color.options.selectedIndex].value);return false;">
                      <option selected="selected" value=""><?php echo $generalLang['color'];?></option>
                      <?php foreach ($colores as $color){ ?>
                      <option><?php echo $color;?></option>
                      <?php } ?>
            </select>
        </div>
        <div class="selectBox">
            <form name="busqueda" id="busqueda" method="get" action="productos-detalle.php">
                <span id="productosSelect">
                <select  style="width:240px;"   class="comboBox1" name="id" onchange="document.forms['busqueda'].submit()">
                  <option selected="selected" value="0"><?php echo $generalLang['variedad'];?></option>
                  <?php foreach ($productos as $producto){ ?>
                  <option value="<?php echo $producto->getId();?>"><?php echo $producto->getTitle();?></option>
                  <?php } ?>
                </select>
                </span>
            </form>
        </div>
    </div>
    </div>
</div>