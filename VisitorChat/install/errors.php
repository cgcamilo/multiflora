<?php

$_E[100] = "Could not connect to database. Please check your host and credentials.";
$_E[200] = "Could not create or use database.";
$_E[300] = "Could not create table 'messaging'. Try again or try to insert manual.";
$_E[400] = "Could not create table 'messaging_admin'. Try again or try to insert manual.";
$_E[500] = "Could not create table 'messaging_ban'. Try again or try to insert manual.";
$_E[600] = "Could not create table 'messaging_groups'. Try again or try to insert manual.";
$_E[700] = "Could not create table 'messaging_history'. Try again or try to insert manual.";
$_E[800] = "Could not create table 'messaging_users'. Try again or try to insert manual.";
$_E[900] = "Could not add table constraints. Try again or try to insert manual.";
$_E[1000] = "Cannot write to config files.";
$_E[1000] = "Could not create table 'messaging_smiley'. Try again or try to insert manual.";
?>