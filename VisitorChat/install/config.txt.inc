<?php

$_ = array();

$_['USER'] = '%user';
$_['PASS'] = '%pass';
$_['HOST'] = '%host';
$_['DB'] = '%db';

$_['MAIL_FROM'] = '%email';
$_['MAIN_FOLDER'] = 'VisitorChat';

$_['ALLOWED_FILE_EXT'] = array("psd", "doc", "docx", "xls", "zip", "7zip", "rar", "avi", "mp4", "jpeg", "jpg", "gif", "png", "bmp", "txt");

?>