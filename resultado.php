<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/productDAO.class.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/image.php';
include 'cms/modules/products/model/imageDAO.php';

$db = new Database();
$db->connect();

$s = $_GET['buscar'];

$productDAO = new ProductDAO($db);
$products = $productDAO->getsSearch($s, $lang);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />
<link href="css/multiflora.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script src="js/menu.js"></script>
<script src="js/slide.js"></script>
<script src="js/resultado.js"></script>
<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->

</head>

<body>
<?php include("header.php"); ?>
<div class="contendor-buscar">
	<div class="cont-buscar">
    	<div class="campo-buscar2">
             <input class="busqueda2" id="busqueda2" type="text" name="buscar" value="<?php echo $s;?>" />
             <input class="ini-buscar2" id="ini-buscar2" type="submit" value="" />
         </div>
    </div>
    <div class="flor"></div>
</div>


<div class="contendor-resultados">
	<div class="cont-resultados">
		<div class="resultados-tit"><?php echo count($products);?> <?php echo $generalLang['resultados'];?></div>
    </div>
    <div class="subastaMainWrapper" style="padding-bottom:60px; margin-bottom:0px;">
    	<div class="subFinContent">
        	<div class="subFinScroll">
            	<div class="subFinBox4">
                    <?php $page = false; $i = 1; foreach($products as $product){ if($i % 9 == 0) $page = true; ?>
                    <!--item -->
                    <div class="subItemFin">
                        <div class="subItemLeftFin">
                            <a href="productos-detalle.php?id=<?php echo $product->getId();?>">
                                <img src="cms/modules/products/files/<?php echo $product->getImg1();?>" width="300" height="185" alt="" />
                            </a>
                        </div>
                        <div class="subItemRight">
                            <span class="tituloFin"><a href="productos-detalle.php?id=<?php echo $product->getId();?>"><?php echo $product->getTitle();?></a></span><br />
                           
                        </div>
                    </div>
                    <?php $i++; if($page){$page = false; echo '</div><div class="subFinBox4">';} } ?>
            	</div>                 
            </div>        
        </div>
        <div class="navigationBox">
        	<div class="navArrowLeft"></div>
            <div class="navInfo"><span class="navFrom">1</span> de <span class="navTo">6</span></div>
        	<div class="navArrowRight"></div>
        </div>
    </div>
</div>



<?php include("footer.php"); ?>
</body>
</html>
