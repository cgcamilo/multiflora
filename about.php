<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/about/define.php';
include 'cms/modules/about/model/aspect.class.php';
include 'cms/modules/about/model/aspectDAO.class.php';

include 'cms/modules/certificaciones/define.php';
include 'cms/modules/certificaciones/model/link.class.php';
include 'cms/modules/certificaciones/model/linkCertificacionesDAO.php';

include 'cms/modules/como/define.php';
include 'cms/modules/como/model/linkComoDAO.php';

include 'cms/modules/ventajas/define.php';
include 'cms/modules/ventajas/model/aspectDAOVentajas.php';

$db = new Database();
$db->connect();

$DAO = new AspectDAO($db);
$intro      = $DAO->getByName('Intro ('.$lang.')');
$como       = $DAO->getByName('COMO LO HACEMOS ('.$lang.')');
$historia   = $DAO->getByName('Historia ('.$lang.')');
$responsabilidad   = $DAO->getByName('RESPONSABILIDAD SOCIAL ('.$lang.')');

$introIMG      = $DAO->getByName('Intro imagen '.$lang.'');
$respoIMG      = $DAO->getByName('Responsabilidad imagen '.$lang.'');
$comoTh        = $DAO->getByName('Como lo hacemos thumbs');
$historiaTh    = $DAO->getByName('Historia thumbs');
$ventajasTh    = $DAO->getByName('Ventajas thumbs');
$responsabilidadTh    = $DAO->getByName('RESPONSABILIDAD SOCIAL thumbs');

$imgHistoria1 = $DAO->getByName('Imagen Historia 1');
$imgHistoria2 = $DAO->getByName('Imagen Historia 2');
$imgHistoria3 = $DAO->getByName('Imagen Historia 3');

$certificacionesDAO = new LinkCertificacionesDAO($db, 2);
$certificaciones = $certificacionesDAO->gets("title", "asc");

$linkComoDAO = new LinkComoDAO($db, 2);
$comos = $linkComoDAO->gets("id", "asc");

$aspectDAOVentajas = new AspectDAOVentajas($db);
$imgVentajas1 = $aspectDAOVentajas->getByName("Imagen 1");
$imgVentajas2 = $aspectDAOVentajas->getByName("Imagen 2");

$vTexto1 =  $aspectDAOVentajas->getByName("Texto 1 (".$lang.")");
$vTexto2 =  $aspectDAOVentajas->getByName("Texto 2 (".$lang.")");
$vTexto3 =  $aspectDAOVentajas->getByName("Texto 3 (".$lang.")");
$vTexto4 =  $aspectDAOVentajas->getByName("Texto 4 (".$lang.")");
$vTexto5 =  $aspectDAOVentajas->getByName("Texto 5 (".$lang.")");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />
<link href="css/multiflora.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
     var ventaja_txt = new Array() ;
	ventaja_txt[1] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mattis erat sit amet risus condimentum tincidunt. Fusce ut magna erat, vitae sodales massa. Mauris mollis aliquam risus. Donec aliquet nunc et elit pharetra at suscipit felis posuere. Curabitur quis arcu sit amet nunc tempus aliquam. In et mauris neque. Sed lectus ligula, pellentesque nec pellentesque quis, pellentesque at tortor. Vivamus tortor sapien, laoreet non fringilla id, ornare sed nisi. Suspendisse pretium ultrices lacus vel imperdiet. Nam pharetra elit sit amet ante hendrerit tristique. ";
	ventaja_txt[2] = "Praesent eu rhoncus lectus. Ut lobortis odio eget nisl elementum in gravida nisl commodo. Sed elementum pulvinar leo vel posuere. Nulla volutpat massa vitae massa rhoncus ac tincidunt neque volutpat. Curabitur sollicitudin dictum nulla mattis bibendum. In diam arcu, mollis ut vehicula ut, suscipit in augue. Nullam fermentum lobortis massa, id dictum odio luctus eget. In hac habitasse platea dictumst. ";
	ventaja_txt[3] = "Aenean sed sem lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In hac habitasse platea dictumst. Proin tempor volutpat lectus dapibus fringilla. Integer mattis ultricies mauris, at iaculis elit gravida a. Curabitur ac urna sem, fringilla facilisis nulla. Aliquam erat volutpat. Etiam sed nunc at tortor facilisis faucibus. In imperdiet augue a leo porttitor sagittis condimentum metus vehicula. Maecenas sagittis elit a mi auctor vitae dapibus massa tempus. Sed aliquam, augue in lobortis pretium, magna metus tempus eros, sed varius purus lorem adipiscing nisl. Vivamus id quam enim. Quisque nec euismod sem. Sed commodo varius tellus, in porttitor magna rutrum at. ";
	ventaja_txt[4] = "Praesent non nisi neque, in posuere nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras vel vulputate lorem. Vivamus nisl metus, egestas ac rhoncus ut, tristique vitae arcu. Vivamus quis risus dui, a ullamcorper tortor. Cras ullamcorper lorem in mauris congue aliquam eleifend elit aliquam. Vivamus sit amet enim diam, at malesuada nisl. Curabitur gravida diam non mauris placerat auctor vitae id elit. ";
	
</script>
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/basic-jquery-slider.js"></script>
<script src="js/menu.js"></script>
<script src="js/slide.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/waypoints.js"></script>


<script type="text/javascript" src="js/ui.core-1.7.2.js"></script>
<script type="text/javascript" src="js/ui.draggable-1.7.2.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="js/plugin.scrollbar.js"></script>
	
        

<script type="text/javascript">
$(document).ready(function() {
	$('.top').addClass('hidden');
	$.waypoints.settings.scrollThrottle = 30;
	$('#wrapper').waypoint(function(event, direction) {
		$('.top').toggleClass('hidden', direction === "up");
	}, {
		offset: '-100%'
	}).find('#cont-about-nav').waypoint(function(event, direction) {
		$(this).parent().toggleClass('sticky', direction === "down");
		event.stopPropagation();
	});
	$('#banner').bjqs({
          'animation' : 'slide',
          'width' : 973,
          'height' : 240
        });
	$('#banner2').bjqs({
          'animation' : 'slide',
          'width' : 622,
          'height' : 240
        });
   $("#contenu").scrollbar(376);

});
</script>

<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->

</head>

<body>
<?php include("header.php"); ?>

<div id="wrapper">
   
<!--<div class="cont-about-nav" id="cont-about-nav">
	<div class="about-nav" id="about-nav">
    	<div class="about-menu">
        	<div class="clear"></div>
        	<ul>
            	<li class="ab1">Misión</li>
                <li class="ab2">Presencia Mundial</li>
                <li class="ab2">Presencia Mundial</li>
                <li class="ab3">Ventajas</li>
                <li class="ab4">Nuestro Proceso</li>
                
            </ul>
        </div>
        <div class="clear"></div>
        <div class="about-sub-menu">
        	
        	<ul>
            	<li class="sab1">Quienes Somos</li>
                <li class="sab2">Presencia Mundial</li>
                <li class="sab2">Presencia Mundial</li>
                <li class="sab3">Ventajas</li>
                <li class="sab4">Nuestro Proceso</li>
            </ul>
        </div>
    </div>
</div> -->
<div class="contenedor-about1">
  <div class="cont-about">

    <div id="container2">
  
      <!--  Outer wrapper for presentation only, this can be anything you like -->
      <div id="img-about">
        <!-- start Basic Jquery Slider -->
        <img src="cms/modules/about/files/<?php echo $introIMG->getValue();?>" title="Automatically generated caption" />
        <!-- end Basic jQuery Slider -->
      </div>
      <!-- End outer wrapper -->
      
    </div>
      <div class="somos-txt"><?php echo $intro->getValue();?></div>
    
    <div class="cont-about-nav2">
    	<div class="about-nav2">
        	<div class="about-menu2">
            	<ul>
                    <li class="about-bot-1" id="ab1" style="background-image: url('./cms/modules/about/files/<?php echo $comoTh->getValue();?>');">
                        <div class="img-bot"><img src="imagenes/sobre-about-bot.png" /></div>
                        <div class="tit-bot"><?php echo $generalLang['como'];?></div>
                    </li>
                    <li class="about-bot-1" id="ab2" style="background-image: url('./cms/modules/about/files/<?php echo $historiaTh->getValue();?>');">
                        <div class="img-bot"><img src="imagenes/sobre-about-bot.png" /></div>
                        <div class="tit-bot"><?php echo $generalLang['historia'];?></div>
                    </li>
                    <li class="about-bot-1" id="ab3" style="background-image: url('./cms/modules/about/files/<?php echo $ventajasTh->getValue();?>');">
                        <div class="img-bot"><img src="imagenes/sobre-about-bot.png" /></div>
                        <div class="tit-bot"><?php echo $generalLang['ventajas'];?></div>
                    </li>
                    <li class="about-bot-1" id="ab4" style="background-image: url('./cms/modules/about/files/<?php echo $responsabilidadTh->getValue();?>');">
                        <div class="img-bot"><img src="imagenes/sobre-about-bot.png" /></div>
                        <div class="tit-bot"><?php echo $generalLang['responsabilidad'];?></div>
                    </li>
                 
                </ul>
            </div>
        </div>
    </div>
    
    <div class="clear"></div>
  </div>
  <div class="flor"></div>
</div>
<div class="contenedor-about2">
  <div class="cont-about">
    <div class="about-tit"><?php echo $generalLang['como'];?></div>
    <div id="container">
  
      <!--  Outer wrapper for presentation only, this can be anything you like -->
      <div id="banner">
        <!-- start Basic Jquery Slider -->
        <ul class="bjqs">
          <?php foreach($comos as $comox){ if( $comox->getLang() != $_SESSION['lang'] ) continue;  ?>
            <li><img src="cms/modules/como/files/<?php echo $comox->getImage();?>" title="<?php echo $comox->getTitle();?>" /></li>
          <?php } ?>
        </ul>
        <!-- end Basic jQuery Slider -->
      </div>
      <!-- End outer wrapper -->
      
    </div>
     <div class="somos-txt"><?php echo $como->getValue();?></div>
    
    
    
    <div class="clear"></div>
  </div>
  <div class="flor"></div>
</div>

<div class="contenedor-about1">
  <div class="cont-about">
    <div class="about-tit"><?php echo $generalLang['historia'];?></div>
    <div class="about-cont1">
      <div class="about-img2">
      	<div class="historia-a">
            <div class="h-a1" style="background-image:url(./cms/modules/about/files/h1_<?php echo $imgHistoria1->getValue();?>);"></div>
            <div class="h-a2" style="background-image:url(./cms/modules/about/files/h2_<?php echo $imgHistoria1->getValue();?>);"></div>
            <div class="h-a3" style="background-image:url(./cms/modules/about/files/h3_<?php echo $imgHistoria1->getValue();?>);"></div>
        </div>
        <div class="historia-b">
            <div class="h-b1" style="background-image:url(./cms/modules/about/files/h1_<?php echo $imgHistoria2->getValue();?>);"></div>
            <div class="h-b2" style="background-image:url(./cms/modules/about/files/h2_<?php echo $imgHistoria2->getValue();?>);"></div>
            <div class="h-b3" style="background-image:url(./cms/modules/about/files/h3_<?php echo $imgHistoria2->getValue();?>);"></div>
        </div>
        <div class="historia-c">
            <div class="h-c1" style="background-image:url(./cms/modules/about/files/h1_<?php echo $imgHistoria3->getValue();?>);"></div>
            <div class="h-c2" style="background-image:url(./cms/modules/about/files/h2_<?php echo $imgHistoria3->getValue();?>);"></div>
            <div class="h-c3" style="background-image:url(./cms/modules/about/files/h3_<?php echo $imgHistoria3->getValue();?>);"></div>
        </div>
      </div>
      <div class="about-txt1">
       <div id="contenu">
			<?php echo $historia->getValue();?>
		</div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="flor"></div>
</div>
<div class="contenedor-about2">
  <div class="cont-about">
    <div class="about-tit"><?php echo $generalLang['ventajas'];?></div>
    <div class="about-cont1">
      <div class="about-area" style="background-image:url(cms/modules/ventajas/files/<?php echo $imgVentajas2->getValue();?>);">
      	<div class="area-a">
        	<ul class="area-list1">
            	<li>
                    <div class="area-num" id="area-num1">1</div>
                    <div class="area-fondo1" id="area-fondo1"></div>
                    <div class="area-img1" id="area1">
                        <img src="cms/modules/ventajas/files/v1_<?php echo $imgVentajas1->getValue();?>" />
                    </div>
                </li>
                <li>
                	<div class="area-num" id="area-num2">2</div>
                    <div class="area-fondo2" id="area-fondo2"></div>
                    <div class="area-img1" id="area2">
                        <img src="cms/modules/ventajas/files/v2_<?php echo $imgVentajas1->getValue();?>" />
                    </div>
                </li>
                <li>
                	<div class="area-num" id="area-num3">4</div>
                    <div class="area-fondo3" id="area-fondo3"></div>
                    <div class="area-img4" id="area3">
                        <img src="cms/modules/ventajas/files/v4_<?php echo $imgVentajas1->getValue();?>" />
                    </div>
                </li>
            </ul>
            
            <ul class="area-list2">
            	<li>
                	<div class="area-num" id="area-num4">3</div>
                    <div class="area-fondo4" id="area-fondo4"></div>
                    <div class="area-img2" id="area4">
                        <img src="cms/modules/ventajas/files/v3_<?php echo $imgVentajas1->getValue();?>" />
                    </div>
                </li>
                <li>
                	<div class="area-num" id="area-num5">5</div>
                    <div class="area-fondo5" id="area-fondo5"></div>
                    <div class="area-img3" id="area5">
                        <img src="cms/modules/ventajas/files/v5_<?php echo $imgVentajas1->getValue();?>" />
                    </div>
                </li>
     
            </ul>
        </div>
      </div>
      <div class="about-cont-din1" id="cont-din">
          <div class="tit-din">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
          <div class="txt-din">
            <p>
                <?php echo nl2br($vTexto1->getValue());?>
            </p>
          </div>
      </div>
      
      <div class="about-cont-din2" id="cont-din">
          <div class="tit-din">2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
          <div class="txt-din">
            <p>
                <?php echo nl2br($vTexto2->getValue());?>
            </p>
          </div>
      </div>
      
      <div class="about-cont-din3" id="cont-din">
          <div class="tit-din">3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
          <div class="txt-din">
            <p>
                <?php echo nl2br($vTexto3->getValue());?>
            </p>
          </div>
      </div>
      
      <div class="about-cont-din4" id="cont-din">
          <div class="tit-din">4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
          <div class="txt-din">
            <p>
                <?php echo nl2br($vTexto4->getValue());?>
            </p>
          </div>
      </div>
      <div class="about-cont-din5" id="cont-din">
          <div class="tit-din">5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
          <div class="txt-din">
            <p>
                <?php echo nl2br($vTexto5->getValue());?>
            </p>
          </div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="flor"></div>
</div>
<!--<div class="contenedor-about4">
  <div class="cont-about">
    <div class="about-tit">VENTAJAS</div>
    <div class="about-cont4">
    	
      	<div id="banner2">
        <!-- start Basic Jquery Slider -->
       <!-- <div class="ventajas-txt">
        	<div class="ventaja">
        		<p>Praesent eu rhoncus lectus. Ut lobortis odio eget nisl elementum in gravida nisl commodo. Sed elementum pulvinar leo vel posuere. Nulla volutpat massa vitae massa rhoncus ac tincidunt neque volutpat. Curabitur sollicitudin dictum nulla mattis bibendum. In diam arcu, mollis ut vehicula ut, suscipit in augue. Nullam fermentum lobortis massa, id dictum odio luctus eget. In hac habitasse platea dictumst. </p>
            </div>
            <div class="ventaja">
        		<p>Praesent eu rhoncus lectus. Ut lobortis odio eget nisl elementum in gravida nisl commodo. Sed elementum pulvinar leo vel posuere. Nulla volutpat massa vitae massa rhoncus ac tincidunt neque volutpat. Curabitur sollicitudin dictum nulla mattis bibendum. In diam arcu, mollis ut vehicula ut, suscipit in augue. Nullam fermentum lobortis massa, id dictum odio luctus eget. In hac habitasse platea dictumst. </p>
            </div>
        </div> -->
        <!--<ul class="bjqs">
          <li><img src="imagenes/about-slide1.jpg" title="Automatically generated caption"></li>
          <li><img src="imagenes/about-slide2.jpg" title="Automatically generated caption"></li>
          <li><img src="imagenes/about-slide1.jpg" title="Automatically generated caption"></li>
          <li><img src="imagenes/about-slide1.jpg" title="Automatically generated caption"></li>
       
        </ul>
        <!-- end Basic jQuery Slider -->
      <!--</div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="flor"></div>
 -->
<div class="contenedor-about1">
  <div class="cont-about">
    <div class="about-tit"><?php echo $generalLang['responsabilidad'];?></div>
    <div class="about-cont1">
  
      <!--  Outer wrapper for presentation only, this can be anything you like -->
      <div class="img-responsabilidad">
          <img src="cms/modules/about/files/<?php echo $respoIMG->getValue()?>" />
      </div>
      <!-- End outer wrapper -->
      
     <div class="somos-txt2"><?php echo $responsabilidad->getValue();?>
     	<div class="sellos">
            <?php foreach($certificaciones as $certificacion){ if( $certificacion->getLang() != $lang ) continue; ?>
            <div class="sello">
                <?php if($certificacion->getUrl() != ""){ ?>
                <a href="<?php echo $certificacion->getUrl();?>">
                <?php } ?>
                <img src="cms/modules/certificaciones/files/<?php echo $certificacion->getImage();?>" />
                <?php if($certificacion->getUrl() != ""){ ?>
                </a>
                <?php } ?>
            </div>
            <?php  } ?>
            <div class="clear"></div>
        </div>
     </div>
     
    </div>
    <div class="clear"></div>
  </div>
</div>

</div>
</div>



<?php include("footer.php"); ?>
</body>
</html>
