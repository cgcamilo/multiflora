<?php
$generalLang=array(
    "inicio" => "INICIO",
    "nosotros" => "NOSOTROS",
    "productos" => "PRODUCTOS",
    "noticias" => "NOTICIAS",
    "contactenos" => "CONTÁCTENOS",
    "lang" => "Español",
    "idioma" => "Idioma",
    "español" => "Español",
    "ingles" => "English",
    "ruso" => "Pусский",
    "destacados" => "Destacados",
    "redes" => "Redes sociales",
    "ayuda" => "Ayuda",
    "pedido" => "Pedido",
    "registrate" => "Regístrate",
    "buscar" => "Buscar flor",
    "variedades" => "variedades",
    "como" => "Como lo hacemos",
    "historia" => "Historia",
    "ventajas" => "Ventajas",
    "responsabilidad" => "Responsabilidad social",
    "ver" => "Ver más",
    "otrasnoticias" => "Otras noticias",
    "nombre" => "Nombre",
    "apellidos" => "Apellidos",
    "empresa" => "Empresa",
    "cargo" => "Cargo",
    "email" => "E-mail",
    "dir" => "Dirección",
    "ciudad" => "Ciudad",
    "tel" => "Teléfono",
    "pais" => "País",
    "zip" => "Código postal",
    "dir2" => "Dirección de contacto 2",
    "estado" => "Estado",
    "comentarios" => "Comentarios",
    "info1" => "Información de la empresa",
    "info2" => "Información de contacto",
    "enviar" => "ENVIAR",
    "registro" => "REGISTRO",
    "resultados" => "Resultados encontrados",
    "buscarproducto" => "Buscar producto",
    "tipo" => "Tipo flor",
    "color" => "Color",
    "variedad" => "Variedad",
    "variedadesde" => "Variedades de",
    "todas" => "Todas",
    "de" => "de",
    "solicitar" => "Solicitar producto",
    "especificaciones" => "Especificaciones",
    "registrate" => "Regístrate",
    "ingresar" => "Ingresar",
    "errordatos" => "Por favor ingresa todos los datos",
    "erroremail" => "Por favor ingresa un email válido",
    "mensajeenviado" => "Tu mensaje ha sido enviado con exito. Gracias!",
    "volver" => "Volver",
    "compartir" => "Compartir",
    "owner" => "Vendedor",
    "buyer" => "Comprador",
    "other" => "Otro"
);
?>