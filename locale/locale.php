<?php

/* idiomas que soportamos */
$sup_lang=array("es", "en");

/* recogemos el valor almacenado, de haberlo */
if($_SESSION["lang"]!="")
  $lang=$_SESSION["lang"];
else  /* sino, a ver que nos dice el navegador */
{
  $cli_lang=explode(",", $HTTP_ACCEPT_LANGUAGE);

  /* si el cliente indica 'es-es', seleccionaremos 'es' */
  /* en el momento que un idioma coincide, dejamos de buscar */
  for($i=0;$i<count($cli_lang) && !isset($lang); $i++)
    for($j=0;$j<count($sup_lang); $j++)
      if(!strncmp($cli_lang[$i],$sup_lang[$j],strlen($sup_lang[$j])))
      {
        $lang=$sup_lang[$j];
        break;
      }
}

/* podemos cambiar de lenguaje con un GET */
/* y esta decisi�n manda sobre lo que diga el navegador */
if($_GET["lang"]!="")
  $lang=$_GET["lang"];

switch($lang)
{
  /* por defecto hemos quedado que es ingles */
  default:

  case "en":
    include_once("./locale/en.inc.php");
    $_SESSION["lang"]="en";
  break;

  case "es":
    include_once("./locale/es.inc.php");
    $_SESSION["lang"]="es";
  break;

  case "ru":
    include_once("./locale/ru.inc.php");
    $_SESSION["lang"]="ru";
  break;
}

$lang = $_SESSION["lang"];
?>