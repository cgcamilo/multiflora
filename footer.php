<?php
if(!class_exists ('Aspect'))
    include 'cms/modules/footer/model/aspect.class.php';
if(!class_exists ('AspectFootDAO'))
    include 'cms/modules/footer/model/aspectFootDAO.php';
include 'cms/modules/footer/define.php';

$DAO = new AspectFootDAO($db);
$telUS = $DAO->getByName("Oficina Estados Unidos teléfono");
$dirUS = $DAO->getByName("Oficina Estados Unidos Dirección");
$emailUS = $DAO->getByName("Oficina Estados Unidos email");

$telES = $DAO->getByName("Oficina España teléfono");
$dirES = $DAO->getByName("Oficina España Dirección");
$emailES = $DAO->getByName("Oficina España email");

$telCO = $DAO->getByName("Oficina Colombia teléfono");
$dirCO = $DAO->getByName("Oficina Colombia Dirección");
$emailCO = $DAO->getByName("Oficina Colombia email");

$catalogo = $DAO->getByName("Catálogo ".$lang);
?>
<div class="footer-top"> </div>
</div>
<div class="footer">
  <div class="footer-cont">
    <div class="footer-cont-iz">
      <div class="links-content">
        <div class="links-tit"><b>MULTIFLORA</b> | Farm Direct Experts</div>
        <div class="links-lista">
          <ul>
            <a href="index.php" name="link"><li><?php echo $generalLang['inicio'];?></li></a>
            <a href="about.php" name="link"><li><?php echo $generalLang['nosotros'];?></li></a>
            <a href="productos.php" name="link"><li><?php echo $generalLang['productos'];?></li></a>
          </ul>
          <ul>
            <a href="subastas.php" name="link"><li>Subastas</li></a>
           <!-- <a href="#" name="link"><li>Envíos</li></a> -->
            <a href="noticias.php" name="link"><li><?php echo $generalLang['noticias'];?></li></a>
            <a href="contacto.php" name="link"><li><?php echo $generalLang['contactenos'];?></li></a>
          </ul>
            <a href="cms/modules/footer/files/<?php echo $catalogo->getValue();?>"><div class="catalogo">Descarge nuestro catálogo</div></a>
        </div>
      </div>
      <div class="links-content2">
        <div class="links-tit"><b><?php echo $generalLang['redes'];?></b></div>
        <div class="links-lista">
          <ul>
            <li> <a href="#"> <img src="imagenes/icono-face.png"/>
              <p>Facebook</p>
              </a> </li>
            <li> <a href="#"> <img src="imagenes/icono-twit.png"/>
              <p>Twitter</p>
              </a> </li>
            <!--<li> <a href="#"> <img src="imagenes/icono-flick.png"/>
              <p>Flickr</p>
              </a> </li>
             <li> <a href="#"> <img src="imagenes/icono-rss.png"/>
              <p>RSS</p>
              </a> </li> -->
              <!--<li class="intra-list"><div class="intra">Intranet</div></li> -->
          </ul>
        </div>
      </div>
      <div class="clear"></div>
      <div class="icono-hojas"></div>
    </div>
    <div class="footer-cont-de">
    	<div class="pais">ESTADOS UNIDOS</div>
        <div class="tlf"><?php echo $telUS->getValue();?></div>
        
        <div class="pais">ESPAÑA</div>
        <div class="tlf"><?php echo $telES->getValue();?></div>
        
        <div class="pais">COLOMBIA</div>
        <div class="tlf"><?php echo $telCO->getValue();?></div>
        
        <div class="pais"><a href="mailto:<?php echo $emailCO->getValue();?>"><?php echo $emailCO->getValue();?></a></div>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="footer-bottom">
	<div class="footer-bottom-cont">
    	<div class="right"><b>© 2012. MULTIFLORA</b> | Farm Direct Experts. Todos los derechos reservados</div>
        <div class="footer-autor"><span id="ahorranito2"></span><a href="http://www.imaginamos.com" target="_blank">Diseño Web: </a><a href="http://www.imaginamos.com">imagin<span>a</span>mos.com</a></div>
    </div>
</div>
</div>