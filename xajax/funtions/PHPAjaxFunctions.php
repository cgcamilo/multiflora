<?php

function updateProductsSelect($cat, $color){



    $response = new xajaxResponse();

    $function = "document.forms['busqueda'].submit()";
    $selectInit = '<select  style="width:240px;"   class="comboBox1" name="id" onchange="'.$function.'">';
    $selectEnd = '</select>';

    $db = new Database();
    $db->connect();
    $productDAO = new ProductDAO($db);
    $catDAO = new CategoryDAO($db);
    $catSearch = $catDAO->getById($cat);
    if($catSearch != null){
        $catName = $catSearch->getTitle();
    }else
        $catName = " ";


    $products = array();

    if( $cat != "0"  && $color == ""){
        $products = $productDAO->getByIdCat($cat);
    }

    if( $cat == "0"  && $color != ""){
        $products = $productDAO->getsByIdColor($color);
    }

    if( $cat != "0"  && $color != ""){
        $products = $productDAO->getsByCatAndColor($cat, $color);
    }

    $options = '';

    foreach($products as $product){
        if( $product->getLang() != $_SESSION['lang'] )
                continue;
        $options .= '<option value="'.$product->getId().'">'.$product->getTitle().'</option>';
    }

    ob_start();
    include 'productos-ajax.php';
    $answer = ob_get_clean();

    $response->assign("productosSelect","innerHTML",$selectInit.$options.$selectEnd);
    $response->assign("products-body","innerHTML",$answer);
    $response->assign("products-header","innerHTML","");
    $response->assign("products-other","innerHTML","");
    $response->script("msDropDown()");

   return $response;
}

?>