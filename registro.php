<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/users/model/user.class.php';
include 'cms/modules/users/model/userDAO.class.php';


$db = new Database();
$db->connect();

$db->doQuery("SELECT country_name FROM e_learning_countries",SELECT_QUERY);
$countries = $db->results;

$error = 0;
if( isset($_GET['name']) ){
    foreach( $_GET as $key => $value ){
        if( $value == "" && ($key != "zip" && $key != "corta" && $key != "dir2" && $key != "role2") )
            $error = 1;
        $$key = $value;
    }

    if( $error == 0 ){
        $db = new Database();
        $db->connect();

        $userDAO = new UserDAO($db);

        $user = new User();
        $user->setCity($city);
        $user->setCompany($company);
        $user->setDir($dir);
        $user->setEmail($email);
        $user->setIdNumber($idNumber);
        $user->setIdType($idType);
        $user->setLastName($lastName);
        $user->setName($name);
        $user->setRole($role.' '.$role2);
        $user->setTel($tel);
        $user->setLang($lang);
        $user->setCountry($country);
        $user->setCorta($corta);
        $user->setDir2($dir2);
        $user->setDir($dir);
        $user->setCity($city);
        $user->setZip($zip);
        $user->setState($state);

        $userDAO->save($user);

        unset ($_GET);
        $error = 2;
    }
    


}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />
<link href="css/multiflora.css" rel="stylesheet" type="text/css" />
<link href="css/dd.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script src="js/menu.js"></script>
<script src="js/slide.js"></script>
<script src="js/contacto.js"></script>
<script src="js/productos.js"></script> 
<script src="js/productos2.js"></script>
<script src="js/jquery.dd.js"></script>  

<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->
<script>
$(document).ready(function()
    {
			$("#msdrpdd20_msa_3 .ddTitleText").click( function(){
                $(".campo-cargo").css({
					'display': "block"
				});
            });
			$("#msdrpdd20_msa_0 .ddTitleText").click( function(){
                $(".campo-cargo").css({
					'display': "none"
				});
            });
			$("#msdrpdd20_msa_1 .ddTitleText").click( function(){
                $(".campo-cargo").css({
					'display': "none"
				});
            });
			$("#msdrpdd20_msa_2 .ddTitleText").click( function(){
                $(".campo-cargo").css({
					'display': "none"
				});
            });
	});
</script>
</head>

<body>
<?php include("header.php"); ?>
<div class="contendor-registro1" style="display:none;">
	<div class="cont-registro">
        <div class="registro-sub-tit">Información de ingreso</div>
        <div class="cont-registro-form">
        	<div class="registro-formulario-iz">
                <span>Nombre de usuario:<br /></span>
                <input class="nombre" id="nombre" type="text" name="nombre" value="<?php echo $_GET['nombre']?>" />
                
              
            </div>
            <div class="registro-formulario-de">
            	<span>Contraseña:<br /></span>
                <input class="psw" id="psw" type="password" name="psw" value="psw" />
            	
        	</div>
            <p class="explicación">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dignissim nisi ac erat tristique mollis. Cras sit amet orci tortor. Quisque euismod eros a nisi convallis vitae ullamcorper neque rhoncus. Etiam tincidunt odio vitae turpis facilisis eu dictum metus ultrices. Nulla facilisi. Nullam hendrerit convallis nibh non porta.</p>
                
            </div>
            	<input class="enviar2" id="ini-buscar" type="submit" value="" />
                
            <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="flor"></div>
</div>
    <form method="get" action="registro.php">
<div class="contendor-registro1">
	<div class="cont-registro">
            <div class="registro-tit"><?php echo $generalLang['registro'];?>
                <span style="color: red">
                    <?php if( $error == 1 ) echo 'Introduce los campos obligatorios'; ?>
                </span>
                <span style="color: green">
                    <?php if( $error == 2 ) echo 'Datos almacenados. Gracias.'; ?>
                </span>
        	<div class="reg-descarga" style="display:none;">
            	<div class="reg-txt">Descargue el formulario de registro para realizar pedidos en linea</div>
                <a href="#"><div class="reg-bot">Descargar</div></a>
            </div>
        
        </div>
        <div class="registro-sub-tit"><?php echo $generalLang['info1'];?></div>
        <div class="cont-registro-form">
		
        	<div class="registro-formulario-iz">
                <span><?php echo $generalLang['nombre'];?>:<br /></span>
                <input class="nombre" id="nombre" type="text" name="name" value="<?php echo $_GET['name']?>" />
                <span><br /><?php echo $generalLang['empresa'];?>:<br /></span>
                <input class="empresa" id="empresa" type="text" name="company" value="<?php echo $_GET['company']?>" />
                <span><br /><?php echo $generalLang['email'];?>:<br /></span>
                	<input class="nombre" id="nombre" type="text" name="email" value="<?php echo $_GET['email']?>" />
              
            </div>
            <div class="registro-formulario-de">
            	<span><?php echo $generalLang['apellidos'];?>:<br /></span>
                <input class="apellido" id="apellido" type="text" name="lastName" value="<?php echo $_GET['lastName']?>" />
                <span><br /><?php echo $generalLang['cargo'];?>:<br /></span>
                 <div class="selectBox">
                    <select  style="width:240px;"   class="comboBox1" name="role">
                              <option selected="selected" value="1">Select</option>
                              <option value="2"><?php echo $generalLang['owner'];?></option>
                              <option value="2"><?php echo $generalLang['buyer'];?></option>
                              <option value="2"><?php echo $generalLang['other'];?></option>
                    </select>
        		</div>
                <div class="clear"></div>
                <div class="campo-cargo">
                	<span>Especifique el cargo:<br /></span>
               	    <input class="apellido" id="apellido" type="text" name="role2" value="<?php echo $_GET['role2']?>" />
                
                
            </div>
           
    </div>
    <div class="clear"></div>
    </div>
    <div class="clear"></div>
    
    
</div>
<div class="flor"></div>
</div>

<div class="contendor-registro2">
	<div class="cont-registro">
    	<div class="registro-sub-tit"><?php echo $generalLang['info2'];?></div>
        <div class="cont-registro-form">
        	<div class="registro-formulario-iz">
                <span><?php echo $generalLang['dir'];?>:<br /></span>
                <input class="nombre" id="nombre" type="text" name="dir" value="<?php echo $_GET['dir']?>" />
                <span><br /><?php echo $generalLang['tel'];?>:<br /></span>
                <input class="empresa" id="empresa" type="text" name="tel" value="<?php echo $_GET['tel']?>" />
                <span><br /><?php echo $generalLang['zip'];?>:<br /></span>
                <input class="empresa" id="empresa" type="text" name="zip" value="<?php echo $_GET['zip']?>" />
                <span><br /><?php echo $generalLang['dir2'];?>:<br /></span>
                <input class="empresa" id="empresa" type="text" name="dir2" value="<?php echo $_GET['dir2']?>" />
              
            </div>
            <div class="registro-formulario-de">
            	<span><?php echo $generalLang['ciudad'];?>:<br /></span>
                <input class="apellido" id="apellido" type="text" name="city" value="<?php echo $_GET['city']?>" />
                <span><br /><?php echo $generalLang['pais'];?>:<br /></span>
                 <div class="selectBox" id="select88">
                    <select  style="width:240px;"   class="comboBox1" name="country">
                              <?php foreach($countries as $country){ ?>
                             <option>
                                 <?php echo $country['country_name'];?>
                             </option>
                              <?php } ?>
                    </select>
        		</div>
            	<div class="clear"></div>
            	<span><?php echo $generalLang['estado'];?>:<br /></span>
                 <div class="selectBox" id="select88">
                     <input type="text" name="state" value="<?php echo $_GET['state']?>" />
        		</div>
                
            </div>
            <div class="clear"></div>
            <div class="registro-formulario-bot">
                <span>
                <?php echo $generalLang['comentarios'];?>:
                <br />
                </span>
                <textarea class="comentarios" cols="40" rows="10" name="corta"><?php echo $_GET['corta']?></textarea>
            </div>
             
            <div class="clear"></div>
              
        
        <div class="carga-form">
        
        </div>
        
    </div>
    
              <input class="enviar2" id="ini-buscar" type="submit" value="1" />
    <div class="clear"></div>
    </div>
</div>
</form>

<?php include("footer.php"); ?>
</body>
</html>
