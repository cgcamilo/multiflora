<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/productDAO.class.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/image.php';
include 'cms/modules/products/model/imageDAO.php';

require ('xajax/xajax_core/xajax.inc.php');
$xajax = new xajax();

include 'xajax/funtions/PHPAjaxFunctions.php';
$xajax->registerFunction("updateProductsSelect");
$xajax->processRequest();


$db = new Database();
$db->connect();

$productDAO = new ProductDAO($db);
$id = $_GET['id'];
$product = $productDAO->getById($id);
if( $product == null ){
    $product = new Product();
    $product->setTitle('Not found');
    $product->setId(0);
}

$galleryDAO = new ImageDAO($db);
$pics = $galleryDAO->getsByIdProduct($product->getId(), "products_img_id", "asc");

if( $product->getImgId() != 0 ){
    $pics2 = $galleryDAO->getsByIdProduct($product->getImgId(), "products_img_id", "asc");
    foreach($pics2 as $pic2){
        $pics[] = $pic2;
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<!-- XAJAX -->
<?php
$xajax->printJavascript("xajax/");
?>
<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />

<link href="css/multiflora.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
	var altura = 200;

});
</script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b72a3d2a-b418-4b99-86af-4b07da083825"}); </script>


<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
<script src="js/menu.js"></script> 
<script src="js/slide.js"></script> 


<script src="js/productos-detalle.js"></script> 

<!-- custom Select -->
<link href="css/dd.css" rel="stylesheet" type="text/css" />

<script src="js/jquery.dd.js"></script> 
<!-- CUSTOM SCROLLBAR-->
<script language="javascript" type="text/javascript" src="js/jquery.mousewheel.js"> </script> 
<script language="javascript" type="text/javascript" src="js/jquery.jscrollpane.min.js"> </script>
<link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.css">



<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->
<script type="text/javascript" src="js/ui.core-1.7.2.js"></script>
<script type="text/javascript" src="js/ui.draggable-1.7.2.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="js/plugin.scrollbar.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $(".textBox").scrollbar(256);

});
function goback() {
    history.go(-1);
}
</script>
</head>
<body>
<?php include("header.php"); ?>

<?php include 'barraBuscar.php';?>

<div class="clear"></div>

<div class="prodContentWrapper2" style="background:#fff;" id="products-header">
	<div class="detalleBox">
    	<div class="detalleLeftBox">
        	<div class="nombre"><?php echo $product->getTitle();?></div>
        	<div class="especificaciones"><?php echo $generalLang['especificaciones'];?></div>
        	<div class="textoBoxScroll">
                    <div class="textBox"><?php echo nl2br($product->getDecription());?></div>
            </div>
            
            <div class="botonesBox">
            	<a  class='st_sharethis_large' displayText='Compartir' id="compartirBtn"><?php echo $generalLang['compartir'];?></a>
                
                
                <a  class="agregarBtn" href="http://multiflora.vida18.com/formslogin.asp?strurl=%2Fprovider%2FOrders%2Easp"><?php echo $generalLang['solicitar'];?> <?php echo $generalLang['producto'];?></a>
            </div>
            <div class="clear"></div>
            <div class="galeriaThumbsBox">
                <?php if( count($pics) > 4 ){ ?>
            	<div class="arrowLeft"></div>
                <?php } ?>
                <div class="thumbsContainer">
                    <div class="thumbsBox">
                        <!--<img src="cms/modules/products/files/<?php echo $product->getImg1();?>" width="80"  alt="" />-->
                        <?php foreach($pics as $pic){ ?>
                        <img src="cms/modules/products/files/<?php echo $pic->getPath();?>" width="80" alt="" />
                        <?php } ?>
                    </div>
                </div>
                <?php if( count($pics) > 4 ){ ?>
            	<div class="arrowRight"></div>
                <?php } ?>
            </div>
            
            
            
        </div>
    	<div class="detalleRightBox">
        	<div class="galeriaBigImages">
                <!--<img src="cms/modules/products/files/<?php echo $product->getImg1();?>" width="460" alt="" />-->
                <?php foreach($pics as $pic){ ?>
                <img src="cms/modules/products/files/<?php echo $pic->getPath();?>" width="460" alt="" />
                <?php } ?>
           	</div>
        </div>  
    <div class="clear"></div>
    </div>
</div>

<div id="products-body" class="contendor-otras-categorias"></div>

<?php include("footer.php"); ?>

</body>
</html>
