<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/productDAO.class.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/image.php';
include 'cms/modules/products/model/imageDAO.php';

require ('xajax/xajax_core/xajax.inc.php');
$xajax = new xajax();

include 'xajax/funtions/PHPAjaxFunctions.php';
$xajax->registerFunction("updateProductsSelect");
$xajax->processRequest();

$db = new Database();
$db->connect();

$catDAO = new CategoryDAO($db);
$idCat = $_GET['idCat'];
$cat = $catDAO->getById($idCat);

if( $cat == null ){
    $cat = new Category();
    $cat->setTitle('Not found');
    $cat->setId(0);
}

$subCatDAO = new SubCategoryDAO($db);
$subs = $subCatDAO->getsByCat($idCat, "products_subcat_order", "asc");

$productDAO = new ProductDAO($db);

$products = $productDAO->getByIdCat($idCat);
$subCatName = $generalLang['todas'];

if( isset($_GET['idSubCat']) ){
    $products = $productDAO->getsByIdSubCat($_GET['idSubCat']);
    $subCat = $subCatDAO->getById($_GET['idSubCat']);
    if( $subCat != null )
        $subCatName = $subCat->getTitle ();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<!-- XAJAX -->
<?php
$xajax->printJavascript("xajax/");
?>
<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />

<link href="css/multiflora.css" rel="stylesheet" type="text/css" />




<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
<script src="js/menu.js"></script> 
<script src="js/slide.js"></script> 


<script src="js/productos.js"></script> 

<!-- custom Select -->
<link href="css/dd.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.dd.js"></script> 



<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->


</head>
<body>
<?php include("header.php"); ?>


<!--<div class="prodContentWrapper">
	<div class="prodBox">    
    	<a href="productos-detalle.php"><div class="prodItem">
   	    	<img src="imagenes/productos1.jpg" width="280" height="280" alt="producto1" />
            <div class="prodName">Chrysanthemums</div>
            <div class="prodNum">15 species</div>
            <div class="line"></div>
      	</div></a>
        
        <a href="productos-detalle.php"><div class="prodItem">
   	    	<img src="imagenes/productos2.jpg" width="280" height="280" alt="producto1" />
            <div class="prodName">Carnation</div>
            <div class="prodNum">15 species</div>
            <div class="line"></div>
      	</div></a>
        
        <a href="productos-detalle.php"><div class="prodItem" style="border-right:none">
   	    	<img src="imagenes/productos3.jpg" width="280" height="280" alt="producto1" />
            <div class="prodName">Mini</div>
            <div class="prodNum">15 species</div>
            <div class="line"></div>
      	</div></a>
        
        <a href="productos-detalle.php"><div class="prodItem">
   	    	<img src="imagenes/productos4.jpg" width="280" height="280" alt="producto1" />
            <div class="prodName">Roses</div>
            <div class="prodNum">15 species</div>
            <div class="line"></div>
      	</div></a>
        
        <a href="productos-detalle.php"><div class="prodItem">
   	    	<img src="imagenes/productos5.jpg" width="280" height="280" alt="producto1" />
            <div class="prodName">Novelties</div>
            <div class="prodNum">15 species</div>
            <div class="line"></div>
      	</div></a>
        
        <a href="productos-detalle.php"><div class="prodItem" style="border-right:none">
   	    	<img src="imagenes/productos6.jpg" width="280" height="280" alt="producto1" />
            <div class="prodName">Chrysanthemums</div>
            <div class="prodNum">15 species</div>
            <div class="line"></div>
      	</div></a>
        
        <div class="clear"></div>
  </div>
  <div class="flor"></div>
</div> -->

<?php include 'barraBuscar.php';?>

<div class="titulo-tipo" id="products-header">
	<div class="tit-tipo">
            <div class="img-tipo"><img src="cms/modules/products/files/<?php echo $cat->getImage();?>" width="70" /></div>
        <div class="rot-tit-tipo"><?php echo $cat->getTitle();?></div>
        <div class="rot-tipo"><?php echo $cat->getDescription();?></div>
    </div>
</div>

<?php if( count($subs) != 0 ){ ?>
<div class="contendor-otras-categorias" id="products-other">
	<div class="cont-otras-categorias">
		
        <div class="bot-prev"></div>
        <div class="slide-cont">
            <div class="categorias-slide">
                
                <ul>
                    <?php foreach($subs as $sub){ ?>
                    <li class="categorias-mini">
                    	<div class="cat-roll">
                        <div class="categorias-mini-tit">
                            <a href="productos.php?idCat=<?php echo $idCat;?>&idSubCat=<?php echo $sub->getId();?>">
                                <?php echo $sub->getTitle();?>
                            </a>
                        </div>
                        <div class="categorias-m-img">
                            <a href="productos.php?idCat=<?php echo $idCat;?>&idSubCat=<?php echo $sub->getId();?>">
                            <img src="cms/modules/products/files/<?php echo $sub->getImage();?>" alt="" />
                            </a>
                        </div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <?php } ?>
                </ul>
                
            </div>
        </div>
        <div class="bot-next"></div>
        <div class="clear"></div>
    </div>
</div>
<?php } ?>
        
<div class="prodContentWrapper" style="background:#fff;" id="products-body">
    <div class="variedad-tit">
        <?php echo $generalLang['variedades'];?> <?php echo $generalLang['de'];?> <?php echo strtoupper($cat->getTitle()); ?>  >  <?php echo $subCatName;?>
    </div>
	<div class="navigationBox2">
        	<div class="navArrowLeft" id="nav-iz2"></div>
            <div class="navInfo"><span class="navFrom">1</span> de <span class="navTo">6</span></div>
        	<div class="navArrowRight" id="nav2-de2"></div>
        </div>
    <div class="subastaMainWrapper" style="padding-bottom:60px; margin-bottom:0px;">
    	<div class="subFinContent">
        	<div class="subFinScroll">
            	<div class="subFinBox4">
                    <!--item -->
                    <?php $i = 0; foreach( $products as $product ){ ?>
                    <?php if( $i%9 == 0 && $i != 0 ) echo '</div><div class="subFinBox4">'; ?>
                    <div class="subItemFin">
                        <div class="subItemLeftFin">
                            <a href="productos-detalle.php?id=<?php echo $product->getId();?>">
                                <img src="cms/modules/products/files/<?php echo $product->getImg1();?>" width="300" height="185" alt="" />
                            </a>
                        </div>
                        <div class="subItemRight">
                            <span class="tituloFin">
                                <a href="productos-detalle.php?id=<?php echo $product->getId();?>">
                                    <?php echo $product->getTitle();?>
                                </a>
                            </span><br />
                        </div>
                    </div>
                    <?php $i++; } ?>
            	</div> 
            </div>        
        </div>
        <div class="navigationBox">
        	<div class="navArrowLeft"></div>
            <div class="navInfo"><span class="navFrom">1</span> de <span class="navTo">6</span></div>
        	<div class="navArrowRight"></div>
        </div>
    </div>
</div>



<?php include("footer.php"); ?>

</body>
</html>
