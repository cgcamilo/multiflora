<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/productDAO.class.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/image.php';
include 'cms/modules/products/model/imageDAO.php';

include 'cms/modules/home/define.php';
include 'cms/modules/home/model/aspectHomeDAO.php';
include 'cms/modules/home/model/aspect.class.php';

include 'cms/modules/links/define.php';
include 'cms/modules/links/model/link.class.php';
include 'cms/modules/links/model/linkDestacadosDAO.php';


$db = new Database();
$db->connect();

$catDAO = new CategoryDAO($db);
$cats = $catDAO->getsByLang($lang, "order", "asc");

$aspectHomeDAO = new AspectHomeDAO($db);
$banner = $aspectHomeDAO->getByName('Banner ('.$lang.')');

$LinkDestacadosDAO = new LinkDestacadosDAO($db, 2);
$destacados = $LinkDestacadosDAO->gets("id", "asc");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />

<style type="text/css">
    #slide0{
        background-image:url(./cms/modules/home/files/<?php echo $banner->getValue(); ?>);
        background-position: center center;
        background-repeat: no-repeat;
        height: 500px;
        left: 0;
        position: absolute;
        width: 100%;
        z-index: -1;
    }
    <?php $i=1; foreach($cats as $cat){ ?>
    #slide<?php echo $i;?>{
        background-image:url(./cms/modules/products/files/<?php echo $cat->getImage_big();?>);
        background-position: center center;
        background-repeat: no-repeat;
        height: 500px;
        left: 0;
        position: absolute;
        width: 100%;
        z-index: -1;
            filter:alpha(opacity=0);
            -moz-opacity:0;
            -khtml-opacity: 0;
            opacity: 0;
    }
    <?php $i++; } ?>
</style>
<link href="css/multiflora.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script src="js/menu.js"></script> 
<script src="js/slide.js"></script> 




<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->


</head>

<body>
<?php include("header.php"); ?>
<div class="contenedor">

  <div class="slide">
    <div class="slide-img" id="slide0"></div>
    <div class="slide-img" id="slide1" name="imagen"></div>
    <div class="slide-img" id="slide2" name="imagen"></div>
    <div class="slide-img" id="slide3" name="imagen"></div>
    <div class="slide-img" id="slide4" name="imagen"></div>
    <div class="slide-img" id="slide5" name="imagen"></div>
    <div class="slide-img" id="slide6" name="imagen"></div>
    <div class="rotulo-cont">
    	<!--[if IE]>
            <?php foreach($cats as $cat){ ?>
            <div class="rotulo2" name="rotulo">
              <div class="rot-nom"><?php echo $cat->getTitle();?></div>
              <div class="rot-tit"><?php echo $cat->getSubTitle();?></div>
            </div>
            <?php } ?>
        <![endif]-->
        
        <!--[if !IE]><![IGNORE[--><![IGNORE[]]> 

            <?php foreach($cats as $cat){ ?>
            <div class="rotulo" name="rotulo">
              <div class="rot-nom"><?php echo $cat->getTitle();?></div>
              <div class="rot-tit"><?php echo $cat->getSubTitle();?></div>
            </div>
            <?php } ?>
        <!--<![endif]-->
        
        <?php foreach($cats as $cat){ ?>
        <a href="productos.php?idCat=<?php echo $cat->getId();?>">
        <div class="bot-amarillo" name="bot"></div>
        </a>
        <?php } ?>
         
  	</div>
  </div>
    
  <div class="slide-selector">
    <ul>
      <?php foreach($cats as $cat){ ?>
      <li>
          <a href="productos.php?idCat=<?php echo $cat->getId();?>">
              <div class="mini-img" name="mini" id="img-1">
                  <img src="cms/modules/products/files/<?php echo $cat->getImage();?>" alt="" />
              </div>
          </a>
          <div class="mini-txt" name="nombre"><?php echo $cat->getTitle();?></div>
          <div class="mini-txt2" name="especies">
              <?php
                $productDAO = new ProductDAO($db);
                $ps = $productDAO->getByIdCat($cat->getId());
              ?>
              <?php echo count($ps);?> variedades
          </div>
      </li>
      <?php } ?>
    </ul>
  </div>
  
  <div class="base-slide"></div>
  
  <div class="franja-blanca">
    <div class="franja-content">
      <div class="info-tit">Destacados</div>
      <div class="info-content">
        <?php $i=1; foreach($destacados as $destacado){ if($destacado->getLang() != $lang) continue; ?>
        <div class="info-caja<?php echo $i;?>">
            <div class="info-caja-img"><img src="cms/modules/links/files/<?php echo $destacado->getImage();?>" /></div>
            <a href="<?php echo $destacado->getUrl(); ?>" target="<?php if($destacado->getBlank()) echo'_blank'; else echo '_self'; ?>">
                <div class="img-capa" name="capa"></div>
            </a>
          <div class="info-caja-tit" name="capa_txt"><?php echo $destacado->getTitle();?></div>
          <div class="info-caja-txt"><?php echo $destacado->getDescription();?></div>
        </div>
        <?php $i++; } ?>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>

<?php include("footer.php"); ?>

</body>
</html>
