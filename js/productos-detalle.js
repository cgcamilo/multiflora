// JavaScript Document
$(document).ready(function(){
	
	//Inicializando custom Combobox
	$(".comboBox1").msDropDown().data("dd");
	//Inicializa Scroll

	
	
	if ($(".thumbsBox").length > 0){
	var numThumbs = $(".thumbsBox img").size();
	var thumbsWidth = $(".thumbsBox img").width();
	var thumbsMargin = parseInt($(".thumbsBox img").css('margin-right').replace('px',''));
	var totalWidthImg = thumbsWidth + thumbsMargin;
	var widthBox = totalWidthImg * numThumbs;
	$(".thumbsBox").width(widthBox);
	}
	
	
	var currentPos = 0;
	var numOnDisplay = 4;
	$(".arrowRight").click(function(){
		if (currentPos<numThumbs-numOnDisplay){
			$(".thumbsBox").animate({'left': '-='+totalWidthImg+'px'}, 300)
			currentPos++
		}
	});
	$(".arrowLeft").click(function(){
		if(currentPos>0){
			$(".thumbsBox").animate({'left': '+='+totalWidthImg+'px'}, 300)
			currentPos--
		}
	});
	
	$('.galeriaBigImages img').css('opacity', '0');
	$('.galeriaBigImages img:eq(0)').css('opacity', '1');
	$(".thumbsBox img").click(function () {
		var index = $(".thumbsBox img").index(this);
		$('.galeriaBigImages img').animate({'opacity': '0'}, 300)
		$('.galeriaBigImages img:eq('+index+')').animate({'opacity': '1'}, 300)
	});
	
			
});