// JavaScript Document
$(document).ready(function(){
	
	//Inicializando custom Combobox
	$(".comboBox1").msDropDown().data("dd");
	//Inicializa Scroll

	
	
	var numThumbs = $(".subFinScroll div.subFinBox4").size();
	
	var thumbsWidth = $(".subFinScroll div.subFinBox4").width();
	var widthBox = thumbsWidth * numThumbs;
	$(".subFinScroll").width(widthBox);
	
	$(".navTo").html(numThumbs);
	$(".navFrom").html(1);
	var currentPos = 0;
	var numOnDisplay = 1;
	$(".navArrowRight").click(function(){
		if (currentPos<numThumbs-numOnDisplay){
			$(".subFinScroll").animate({'left': '-='+thumbsWidth+'px'}, 300)
			currentPos++
			$(".navFrom").html(currentPos+1);
		}
	});
	$(".navArrowLeft").click(function(){
		if(currentPos>0){
			$(".subFinScroll").animate({'left': '+='+thumbsWidth+'px'}, 300)
			currentPos--
			$(".navFrom").html(currentPos+1);
		}
	});
	
	$("#nav-de2").click(function(){
		if (currentPos<numThumbs-numOnDisplay){
			$(".subFinScroll").animate({'left': '-='+thumbsWidth+'px'}, 300)
			currentPos++
			$(".navFrom").html(currentPos+1);
		}
	});
	$("#nav-iz2").click(function(){
		if(currentPos>0){
			$(".subFinScroll").animate({'left': '+='+thumbsWidth+'px'}, 300)
			currentPos--
			$(".navFrom").html(currentPos+1);
		}
	});
	
	
	
	var numThumbs2 = $(".categorias-slide ul li").size();
	var thumbsWidth2 = $(".categorias-slide ul li").width();
	var widthBox2 = thumbsWidth2 * numThumbs2;
	$(".categorias-slide").width(widthBox2);
	
	$(".navTo2").html(numThumbs2);
	$(".navFrom2").html(1);
	
	var currentPos2 = 0;
	var numOnDisplay2 = 4;
	if(numThumbs2 <=3){
	
			$(".bot-next").css({
				'opacity': "0"
			});
			$(".bot-prev").css({
				'opacity': "0"
			});
		
	}
	var t = setInterval(function(){if (currentPos2<numThumbs2-numOnDisplay2){
			$(".categorias-slide").animate({'left': '-='+thumbsWidth2+'px'}, 300)
			currentPos2++
			$(".navFrom2").html(currentPos2+1);
		}},3000);
	
	
	$(".bot-next").click(function(){
		if (currentPos2<numThumbs2-numOnDisplay2){
			$(".categorias-slide").animate({'left': '-='+thumbsWidth2+'px'}, 300)
			currentPos2++
			$(".navFrom2").html(currentPos2+1);
		}
		
	});
	$(".bot-prev").click(function(){
		if(currentPos2>0){
			$(".categorias-slide").animate({'left': '+='+thumbsWidth2+'px'}, 300)
			currentPos2--
			$(".navFrom2").html(currentPos2+1);
		}
	});
	
			
});