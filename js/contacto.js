
// JavaScript Document
$(document).ready(function(){
	
	//GALERIA del SUBASTAS FINALIZADAS
	var numThumbs = $(".equipo-slide ul li").size();
	var thumbsWidth = $(".equipo-slide ul li").width();
	var widthBox = thumbsWidth * numThumbs;
	$(".equipo-slide").width(widthBox);
	
	$(".navTo").html(numThumbs);
	$(".navFrom").html(1);
	
	var currentPos = 0;
	var numOnDisplay = 5;
	$(".bot-next").click(function(){
		if (currentPos<numThumbs-numOnDisplay){
			$(".equipo-slide").animate({'left': '-='+thumbsWidth+'px'}, 300)
			currentPos++
			$(".navFrom").html(currentPos+1);
		}
	});
	$(".bot-prev").click(function(){
		if(currentPos>0){
			$(".equipo-slide").animate({'left': '+='+thumbsWidth+'px'}, 300)
			currentPos--
			$(".navFrom").html(currentPos+1);
		}
	});
	
			
});