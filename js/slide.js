
if(typeof(document.getElementsByClassName) != 'function') {
  	document.getElementsByClassName = function (cn) {
		var rx = new RegExp("\\b" + cn + "\\b"), allT = document.getElementsByTagName("*"), allCN = [], i = 0, a;
			while (a = allT[i++]) {
			  if (a.className && a.className.indexOf(cn) + 1) {
				if(a.className===cn){ allCN[allCN.length] = a; continue;   }
				rx.test(a.className) ? (allCN[allCN.length] = a) : 0;
			  }
			}
		return allCN;
	}
}
onload=function(){
	var c=document.getElementsByClassName('pp'),i=-1;
	while(c[++i])
		alert(c[i].innerHTML);
	
}


$(document).ready(function () {
		
	var valor_act = null;
    var estado_act = null;
    var array = document.getElementsByClassName("mini-img");
	
    var array_nombres = document.getElementsByClassName("mini-txt");
    var array_especies = document.getElementsByClassName("mini-txt2");
    var array_img = document.getElementsByClassName("slide-img");
    var array_txt = document.getElementsByClassName("rotulo");
	var array_txt2 = document.getElementsByClassName("rotulo2");
    var array_bot = document.getElementsByClassName("bot-amarillo");
    var img_act = document.getElementById("slide0");
    var img_sig = null;
    var mar_l = 250;
	var mar_2 = 450;
    var mar_t = 220;
	var array_info = document.getElementsByClassName("img-capa");
	var array_tit = document.getElementsByClassName("info-caja-tit");
	
	
	var array_sub = document.getElementsByClassName("otra");
	
    var array_nombres_sub = document.getElementsByClassName("mini-txt-sub");
    var array_especies_sub = document.getElementsByClassName("mini-txt2-sub");
	
	$(array).hover(

    function () {
		
		valor_act = this;
			 $(array_img[0]).animate({
					'opacity': "0"
				}, 250);
		 for (i = 0; i < array.length; i++) {
            if (array[i] != valor_act) {
				$(array[i]).stop(true);
                $(array_txt[i]).stop(true);
				 $(array_txt2[i+1]).stop(true);
                $(array_bot[i]).stop(true);
                $(array[i]).animate({
                    'opacity': "0.1"
                }, 250);
				$(array_txt[i]).css({
                    'opacity': "0",
                    'margin-left': mar_l
                });
				$(array_txt2[i+1]).css({
                    'opacity': "0",
                    'margin-left': mar_l
                });
                $(array_bot[i]).css({
                    'opacity': "0",
                    'margin-top': mar_t
                });
               
				
					
			
				
            } else {
               img_sig = array_img[i+1]
   			
				$(img_act).animate({
					'opacity': "0"
				}, 250, function () {
					$(img_sig).animate({
						'opacity': "1"
					}, 250);
				});
				$(array_txt[i]).animate({
                    'opacity': "1",
                    'margin-left': "50"
                }, 500);
				$(array_txt2[i+1]).animate({
						'opacity': "1",
						'margin-left': "50"
					}, 500);
                $(array_bot[i]).animate({
                    'opacity': "1",
                    'margin-top': "140",
					'margin-left': "290"
                }, 500);
				img_act = array_img[i+1];
				$(array_nombres[i]).animate({
                    'opacity': "0"
                }, 250);
				$(array_especies[i]).animate({
                    'margin-top': "-18",
                    'opacity': "1"
                }, 250);
				
            }
			
			$(this).animate({
				'opacity': "1"
			}, 250);	
        }
		
    }, function () {
        for (i = 0; i < array.length; i++) {
            if (array[i] != valor_act) {
                array_nombres[i];
                $(array[i]).animate({
                    'opacity': "1"
                }, 250);
            } else {
                $(array_nombres[i]).animate({
                    'opacity': "1"
                }, 250);
                $(array_especies[i]).animate({
                    'margin-top': "0",
                    'opacity': "0"
                }, 250);
            }
        }
        $(this).animate({
            'opacity': "1"
        }, 250);
		
    });
	
	
	$(".slide-selector ul").hover(

    function () {
     
    }, function () {
        
				
		for (i = 0; i < array.length; i++) {
			    $(array[i]).stop(true);
				$(array_img[i]).stop(true);
				$(array_img[i+1]).stop(true);
				$(array_img[i+2]).stop(true);
                $(array_txt[i]).stop(true);
				 $(array_txt2[i+1]).stop(true);
                $(array_bot[i]).stop(true);
			$(array_txt[i]).css({
                    'opacity': "0",
                    'margin-left': mar_l
                });
				$(array_txt2[i+1]).css({
                    'opacity': "0",
                    'margin-left': mar_l
                });
                $(array_bot[i]).css({
                    'opacity': "0",
                    'margin-top': mar_t
                });
				$(array[i]).css({
                    'opacity': "1"
                });
				$(array_img[i]).css({
                    'opacity': "0"
                });
				$(array_img[i+1]).css({
                    'opacity': "0"
                });
				$(array_img[i+2]).css({
                    'opacity': "0"
                });
		}
		$(array_img[0]).css({
						'opacity': "1"
					});
    });
	
	
	$(array_info).hover(

    function () {
        valor_act = this;
        for (i = 0; i < array_info.length; i++) {
            if (array_info[i] != valor_act) {
                $(array_info[i]).stop(true);
				
                $(array_tit[i]).css({
                    
					'color' : "#E3D604"
                });
				$(array_info[i]).animate({
                    'opacity': "0.5"
                }, 250);
            } else {
               
            }
        }
        $(this).animate({
            'opacity': "0"
        }, 250);
    }, function () {
         for (i = 0; i < array_info.length; i++) {
            if (array_info[i] != valor_act) {
                $(array_info[i]).stop(true);
				
				$(array_tit[i]).css({
                    
					'color' : "#87B425"
                });
				
                $(array_info[i]).animate({
                    'opacity': "0"
                }, 250);
            } else {
               
            }
        }
        $(this).animate({
            'opacity': "0"
        }, 250);
    });
	
	
	$(array_sub).hover(

    function () {
		
		valor_act = this;
		
		 for (i = 0; i < array_sub.length; i++) {
            if (array_sub[i] != valor_act) {
				$(array_sub[i]).stop(true);
                $(array_sub[i]).animate({
                    'opacity': "0.1"
                }, 250);
               
				
					
			
				
            } else {

				$(array_nombres_sub[i]).animate({
                    'opacity': "0"
                }, 250);
				$(array_especies_sub[i]).animate({
                    'margin-top': "-18",
                    'opacity': "1"
                }, 250);
				
            }
			
			$(this).animate({
				'opacity': "1"
			}, 250);	
        }
		
    }, function () {
		
        for (i = 0; i < array_sub.length; i++) {
            if (array_sub[i] != valor_act) {
                array_nombres_sub[i];
                $(array_sub[i]).animate({
                    'opacity': "0.3"
                }, 250);
            } else {
                $(array_nombres_sub[i]).animate({
                    'opacity': "1"
                }, 250);
                $(array_especies_sub[i]).animate({
                    'margin-top': "0",
                    'opacity': "0"
                }, 250);
            }
        }
        $(this).animate({
            'opacity': "1"
        }, 250);
    });
	
	
	
	$("#area1").hover(
	
	function () {
		$(this).animate({
            'opacity': "0"
        }, 250);
		$("#area-fondo1").animate({
            'opacity': "0.5",
			'height' : "116"
        }, 250);
		$("#area-num1").css({
            'color': "#fff"
        });
		$(".about-cont-din2,.about-cont-din3,.about-cont-din4,.about-cont-din5").css({
            'opacity': "0",
			'display': "none"
        });
		$(".about-cont-din1").css({
			'display': "block"
        });
		$(".about-cont-din1").animate({
			'opacity': "1"
        }, 250);
    }, 
	
	function () {
        $(this).animate({
            'opacity': "1"
        }, 250);
		$("#area-fondo1").animate({
            'opacity': "0",
			'height' : ""
        }, 250);
		$("#area-num1").css({
            'color': "#000"
        });
    });
	
	$("#area2").hover(
	
	function () {
		$(this).animate({
            'opacity': "0"
        }, 250);
		$("#area-fondo2").animate({
            'opacity': "0.5",
			'height' : "120"
        }, 250);
		$("#area-num2").css({
            'color': "#fff"
        });
		$(".about-cont-din1,.about-cont-din3,.about-cont-din4,.about-cont-din5").css({
            'opacity': "0",
			'display': "none"
        });
		$(".about-cont-din2").css({
			'display': "block"
        });
		$(".about-cont-din2").animate({
			'opacity': "1"
        }, 250);
    }, 
	
	function () {
        $(this).animate({
            'opacity': "1"
        }, 250);
		$("#area-fondo2").animate({
            'opacity': "0",
			'height' : "0"
        }, 250);
		$("#area-num2").css({
            'color': "#000"
        });
    });
	
	$("#area3").hover(
	
	function () {
		$(this).animate({
            'opacity': "0"
        }, 250);
		$("#area-fondo3").animate({
            'opacity': "0.5",
			'height' : "123"
        }, 250);
		$("#area-num3").css({
            'color': "#fff"
        });
		$(".about-cont-din1,.about-cont-din2,.about-cont-din3,.about-cont-din5").css({
            'opacity': "0",
			'display': "none"
        });
		$(".about-cont-din4").css({
			'display': "block"
        });
		$(".about-cont-din4").animate({
			'opacity': "1"
        }, 250);
    }, 
	
	function () {
        $(this).animate({
            'opacity': "1"
        }, 250);
		$("#area-fondo3").animate({
            'opacity': "0",
			'height' : "0"
        }, 250);
		$("#area-num3").css({
            'color': "#000"
        });
    });
	
	$("#area4").hover(
	
	function () {
		$(this).animate({
            'opacity': "0"
        }, 250);
		$("#area-fondo4").animate({
            'opacity': "0.5",
			'height' : "237"
        }, 250);
		$("#area-num4").css({
            'color': "#fff"
        });
		$(".about-cont-din1,.about-cont-din2,.about-cont-din4,.about-cont-din5").css({
            'opacity': "0",
			'display': "none"
        });
		$(".about-cont-din3").css({
			'display': "block"
        });
		$(".about-cont-din3").animate({
			'opacity': "1"
        }, 250);
    }, 
	
	function () {
        $(this).animate({
            'opacity': "1"
        }, 250);
		$("#area-fondo4").animate({
            'opacity': "0",
			'height' : "0"
        }, 250);
		$("#area-num4").css({
            'color': "#000"
        });
    });
	
	
	$("#area5").hover(
	
	function () {
		$(this).animate({
            'opacity': "0"
        }, 250);
		$("#area-fondo5").animate({
            'opacity': "0.5",
			'height' : "123"
        }, 250);
		$("#area-num5").css({
            'color': "#fff"
        });
		$(".about-cont-din1,.about-cont-din2,.about-cont-din3,.about-cont-din4").css({
            'opacity': "0",
			'display': "none"
        });
		$(".about-cont-din5").css({
			'display': "block"
        });
		$(".about-cont-din5").animate({
			'opacity': "1"
        }, 250);
    }, 
	
	function () {
        $(this).animate({
            'opacity': "1"
        }, 250);
		$("#area-fondo5").animate({
            'opacity': "0",
			'height' : "0"
        }, 250);
		$("#area-num5").css({
            'color': "#000"
        });
    });
	
	$("#desp1").hover(
	
	function () {
		$("#desp2").stop(true);
		$("#desp3").stop(true);
		$("#desp-cont1").animate({
            'opacity': "1",
			'height': "200",
			'margin-top': "0"
        }, 250);
		$("#desp-bot1").css({
            'background-position': "bottom"

        });
		$("#desp-txt1").css({
            'display': "block"

        });
		
    }, 
	
	function () {
        $("#desp-cont1").animate({
            'opacity': "0",
			'height': "0",
			'margin-top': "0"
        }, 250);
		$("#desp-bot1").css({
            'background-position': "top"

        });
		$("#desp-txt1").css({
            'display': "none"

        });
    });
	
	$("#desp2").hover(
	
	function () {
		$("#desp1").stop(true);
		$("#desp2").stop(true);
		$("#desp-cont2").animate({
            'opacity': "1",
			'height': "200",
			'margin-top': "0"
        }, 250);
		$("#desp-bot2").css({
            'background-position': "bottom"

        });
		$("#desp-txt2").css({
            'display': "block"

        });
		
    }, 
	
	function () {
        $("#desp-cont2").animate({
            'opacity': "0",
			'height': "0",
			'margin-top': "0"
        }, 250);
		$("#desp-bot2").css({
            'background-position': "top"

        });
		$("#desp-txt2").css({
            'display': "none"

        });
    });
	
	$("#desp3").hover(
	
	function () {
		$("#desp1").stop(true);
		$("#desp2").stop(true);
		$("#desp-cont3").animate({
            'opacity': "1",
			'height': "200",
			'margin-top': "0"
        }, 250);
		$("#desp-bot3").css({
            'background-position': "bottom"

        });
		$("#desp-txt3").css({
            'display': "block"

        });
		
    }, 
	
	function () {
        $("#desp-cont3").animate({
            'opacity': "0",
			'height': "0",
			'margin-top': "0"
        }, 250);
		$("#desp-bot3").css({
            'background-position': "top"

        });
		$("#desp-txt3").css({
            'display': "none"

        });
    });
	
	
	var estado_ha=0;
	var estado_hb=0;
	var estado_hc=1;
	$(".h-a1").hover(
	
	function () {
		if(estado_ha==0){
		$(this).stop(true);
		$(this).animate({
            'opacity': "1"

        },300);
		}
    }, 
	
	function () {
		if(estado_ha==0){
        $(this).stop(true);
		$(this).animate({
            'opacity': "0"

        },300);
		}
    });
	
	$(".h-b2").hover(
	
	function () {
		if(estado_hb==0){
		$(this).stop(true);
		$(this).animate({
            'opacity': "1"

        },300);
		}
		
    }, 
	
	function () {
		if(estado_hb==0){
        $(this).stop(true);
		$(this).animate({
            'opacity': "0"

        },300);
		}
    });
	
	$(".h-c3").hover(
	
	function () {
		if(estado_hc==0){
		$(this).stop(true);
		$(this).animate({
            'opacity': "1"

        },300);
		}
		
    }, 
	
	function () {
		if(estado_hc==0){
        $(this).stop(true);
		$(this).animate({
            'opacity': "0"

        },300);
		}
    });
	
	
	$(".h-a1").click( function(){
		if(estado_ha==0){
                $(".h-a1").animate({
					'opacity': "1"
				},300);
				$(".h-a2").animate({
					'opacity': "1"
				},300);
				$(".h-a3").animate({
					'opacity': "1"
				},300);
				
				$(".h-b1").animate({
					'opacity': "0"
				},300);
				$(".h-b2").animate({
					'opacity': "0"
				},300);
				$(".h-b3").animate({
					'opacity': "0"
				},300);
				
				
				$(".h-c1").animate({
					'opacity': "0"
				},300);
				$(".h-c2").animate({
					'opacity': "0"
				},300);
				$(".h-c3").animate({
					'opacity': "0"
				},300);
				
				
				
				
				
				estado_ha=1;
				estado_hb=0;
				estado_hc=0;
		}
	});
	
	
	
	$(".h-b2").click( function(){
		if(estado_hb==0){
                $(".h-b1").animate({
					'opacity': "1"
				},300);
				$(".h-b2").animate({
					'opacity': "1"
				},300);
				$(".h-b3").animate({
					'opacity': "1"
				},300);
				
				$(".h-a1").animate({
					'opacity': "0"
				},300);
				$(".h-a2").animate({
					'opacity': "0"
				},300);
				$(".h-a3").animate({
					'opacity': "0"
				},300);
				
				
				$(".h-c1").animate({
					'opacity': "0"
				},300);
				$(".h-c2").animate({
					'opacity': "0"
				},300);
				$(".h-c3").animate({
					'opacity': "0"
				},300);
				
				
				
				
				
				estado_ha=0;
				estado_hb=1;
				estado_hc=0;
		}
	});
	
	$(".h-c3").click( function(){
		if(estado_hc==0){
                $(".h-c1").animate({
					'opacity': "1"
				},300);
				$(".h-c2").animate({
					'opacity': "1"
				},300);
				$(".h-c3").animate({
					'opacity': "1"
				},300);
				
				$(".h-a1").animate({
					'opacity': "0"
				},300);
				$(".h-a2").animate({
					'opacity': "0"
				},300);
				$(".h-a3").animate({
					'opacity': "0"
				},300);
				
				
				$(".h-b1").animate({
					'opacity': "0"
				},300);
				$(".h-b2").animate({
					'opacity': "0"
				},300);
				$(".h-b3").animate({
					'opacity': "0"
				},300);
				
				
				
				
				
				estado_ha=0;
				estado_hb=0;
				estado_hc=1;
		}
	});
	
});
