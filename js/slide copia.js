// JavaScript Document
if(typeof(document.getElementsByClassName) != 'function') {
  	document.getElementsByClassName = function (cn) {
		var rx = new RegExp("\\b" + cn + "\\b"), allT = document.getElementsByTagName("*"), allCN = [], i = 0, a;
			while (a = allT[i++]) {
			  if (a.className && a.className.indexOf(cn) + 1) {
				if(a.className===cn){ allCN[allCN.length] = a; continue;   }
				rx.test(a.className) ? (allCN[allCN.length] = a) : 0;
			  }
			}
		return allCN;
	}
}
onload=function(){
	var c=document.getElementsByClassName('pp'),i=-1;
	while(c[++i])
		alert(c[i].innerHTML);
	
}

$(document).ready(function () {
		
	
    var valor_act = null;
    var estado_act = null;
    var array = document.getElementsByClassName("mini-img");
    var array_nombres = document.getElementsByClassName("mini-txt");
    var array_especies = document.getElementsByClassName("mini-txt2");
    var array_img = document.getElementsByClassName("slide-img");
    var array_txt = document.getElementsByClassName("rotulo");
    var array_bot = document.getElementsByClassName("bot-amarillo");
    var img_act = document.getElementById("slide0");
    var img_sig = null;
    var mar_l = -330;
    var mar_t = 170;
	
	var array_info = document.getElementsByClassName("img-capa");
	var array_tit = document.getElementsByClassName("info-caja-tit");
	
	
    $(array).hover(

    function () {
		
        valor_act = this;
		
        for (i = 0; i < array.length; i++) {
            if (array[i] != valor_act) {
                $(array[i]).stop(true);
                $(array_txt[i]).stop(true);
                $(array_bot[i]).stop(true);
                $(array[i]).animate({
                    'opacity': "0.1",
                }, 250);
                $(array_txt[i]).css({
                    'opacity': "0",
                    'margin-left': mar_l,
                });
                $(array_bot[i]).css({
                    'opacity': "0",
                    'margin-top': mar_t,
                });
				
					
					
				
            } else {
                img_sig = array_img[i]
                $(img_act).animate({
                    'opacity': "0",
                }, 250, function () {
                    $(img_sig).animate({
                        'opacity': "1",
                    }, 250);
                });
                img_act = array_img[i];
                $(array_nombres[i]).animate({
                    'opacity': "0",
                }, 250);
                $(array_especies[i]).animate({
                    'margin-top': "-18",
                    'opacity': "1"
                }, 250);
                $(array_txt[i]).animate({
                    'opacity': "1",
                    'margin-left': "-430",
                }, 500);
                $(array_bot[i]).animate({
                    'opacity': "1",
                    'margin-top': "140",
                }, 500);
            }
        }
        $(this).animate({
            'opacity': "1",
        }, 250);
    }, function () {
        for (i = 0; i < array.length; i++) {
            if (array[i] != valor_act) {
                array_nombres[i];
                $(array[i]).animate({
                    'opacity': "0.3",
                }, 250);
            } else {
                $(array_nombres[i]).animate({
                    'opacity': "1",
                }, 250);
                $(array_especies[i]).animate({
                    'margin-top': "0",
                    'opacity': "0"
                }, 250);
            }
        }
        $(this).animate({
            'opacity': "1",
        }, 250);
    });
	
	
	$(array_info).hover(

    function () {
        valor_act = this;
        for (i = 0; i < array_info.length; i++) {
            if (array_info[i] != valor_act) {
                $(array_info[i]).stop(true);
				
                $(array_tit[i]).css({
                    
					'color' : "#E3D604"
                });
				$(array_info[i]).animate({
                    'opacity': "0.5",
                }, 250);
            } else {
               
            }
        }
        $(this).animate({
            'opacity': "0",
        }, 250);
    }, function () {
         for (i = 0; i < array_info.length; i++) {
            if (array_info[i] != valor_act) {
                $(array_info[i]).stop(true);
				
				$(array_tit[i]).css({
                    
					'color' : "#87B425"
                });
				
                $(array_info[i]).animate({
                    'opacity': "0",
                }, 250);
            } else {
               
            }
        }
        $(this).animate({
            'opacity': "0",
        }, 250);
    });
	
	
});