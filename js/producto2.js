// JavaScript Document
$(document).ready(function(){
	
	//GALERIA del SUBASTAS FINALIZADAS
	
	var numThumbs = $(".subFinScroll div.subFinBox4").size();
	var thumbsWidth = $(".subFinScroll div.subFinBox4").width();
	var widthBox = thumbsWidth * numThumbs;
	$(".subFinScroll").width(widthBox);
	
	$(".navTo").html(numThumbs);
	$(".navFrom").html(1);
	
	var currentPos = 0;
	var numOnDisplay = 1;
	$(".navArrowRight").click(function(){
		if (currentPos<numThumbs-numOnDisplay){
			$(".subFinScroll").animate({'left': '-='+thumbsWidth+'px'}, 300)
			currentPos++
			$(".navFrom").html(currentPos+1);
		}
	});
	$(".navArrowLeft").click(function(){
		if(currentPos>0){
			$(".subFinScroll").animate({'left': '+='+thumbsWidth+'px'}, 300)
			currentPos--
			$(".navFrom").html(currentPos+1);
		}
	});
	
			
});