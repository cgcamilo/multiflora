
// JavaScript Document
$(document).ready(function(){
	
	//GALERIA del SUBASTAS FINALIZADAS
	var numThumbs = $(".noticias-slide ul li").size();
	var thumbsWidth = $(".noticias-slide ul li").width();
	var widthBox = thumbsWidth * numThumbs;
	$(".noticias-slide").width(widthBox);
	
	$(".navTo").html(numThumbs);
	$(".navFrom").html(1);
	
	var currentPos = 0;
	var numOnDisplay = 3;
	$(".bot-next").click(function(){
		if (currentPos<numThumbs-numOnDisplay){
			$(".noticias-slide").animate({'left': '-='+thumbsWidth+'px'}, 300)
			currentPos++
			$(".navFrom").html(currentPos+1);
		}
	});
	$(".bot-prev").click(function(){
		if(currentPos>0){
			$(".noticias-slide").animate({'left': '+='+thumbsWidth+'px'}, 300)
			currentPos--
			$(".navFrom").html(currentPos+1);
		}
	});
	
			
});