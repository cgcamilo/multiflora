<?php
session_start();
include 'locale/locale.php';
include 'cms/core/class/db.class.php';

include 'cms/modules/footer/define.php';
include 'cms/modules/footer/model/aspect.class.php';
include 'cms/modules/footer/model/aspectFootDAO.php';

$db = new Database();
$db->connect();

$DAO = new AspectFootDAO($db);

$imgIntro = $DAO->getByName("Imagen Contactenos ".$lang);

$telUS = $DAO->getByName("Oficina Estados Unidos teléfono");
$dirUS = $DAO->getByName("Oficina Estados Unidos Dirección");
$emailUS = $DAO->getByName("Oficina Estados Unidos email");

$telES = $DAO->getByName("Oficina España teléfono");
$dirES = $DAO->getByName("Oficina España Dirección");
$emailES = $DAO->getByName("Oficina España email");

$telCO = $DAO->getByName("Oficina Colombia teléfono");
$dirCO = $DAO->getByName("Oficina Colombia Dirección");
$emailCO = $DAO->getByName("Oficina Colombia email");

$db = new Database();
$db->connect();

include 'mail.php';
$error = 0;

if( isset($_GET['nombre']) ){
    $nombre = $_GET['nombre'];
    $email = $_GET['email'];
    $corta = $_GET['corta'];
    $ciudad = $_GET['ciudad'];
    $pais = $_GET['pais'];

    if($email == "" || $nombre == "" || $corta == "")
        $error = 1;

    if($error == 0 && !ValidaMail($email)){
        $error = 2;
    }

    if($error == 0){
        //todo bien
        $error = 3;
        //envio mail de notificacion
        $body = "<h3>Notificaci&oacute;n de contacto Multiflora</h3>\n\n";
        $body .= "Un usuario envia este mensaje desde la zona de contacto.<br />\n";
        $body .= "<b>Nombre:</b> ".accents2HTML($nombre)."<br />\n";
        $body .= "<b>Ciudad:</b> ".accents2HTML($ciudad)."<br />\n";
        $body .= "<b>Pais:</b> ".accents2HTML($pais)."<br />\n";
        $body .= "<b>Correo Electr&oacute;nico:</b> ".accents2HTML($email)."<br />\n";
        $body .= "<b>Mensaje:</b> <br />".nl2br($corta)."<br />\n";

        $mail2 = new sendCMail("dmcallister@multiflora.com", "system@imaginamos.com", "zona de contacto Multiflora", $body, "text/html");
        $mail2->sendMail();
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Multiflora</title>

<link href="VisitorChat/views/stylesheet/stylesheet.css" type="text/css" media="screen" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="VisitorChat/views/javascript/visitorchat.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="imagenes/icono.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />
<link href="css/multiflora.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script src="js/menu.js"></script>
<script src="js/slide.js"></script>
<script src="js/contacto.js"></script>

<!--[if IE 8]>
<link href="css/multiflorae8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if IE 7]>
<link href="css/multiflorae7.css" rel="stylesheet" type="text/css">
<![endif]-->

</head>

<body>
<?php include("header.php"); ?>
<div class="contendor-contacto">
	<div class="cont-contacto">
    	<div class="contacto-tit"><?php echo $generalLang['contactenos'];?></div>
        <div class="contacto-left">
            <div class="contacto-img">
<!--            <iframe width="977" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=colombia&amp;aq=&amp;sll=40.396764,-3.713379&amp;sspn=8.213335,19.753418&amp;ie=UTF8&amp;hq=&amp;hnear=Colombia&amp;ll=4.570868,-74.297333&amp;spn=42.16193,79.013672&amp;t=m&amp;z=4&amp;output=embed"></iframe>

                --> 			<img src="cms/modules/footer/files/<?php echo $imgIntro->getValue();?>" alt="" />
 			</div>          <!-- <div class="contacto-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus bibendum, nisi elit mattis leo, sit amet dictum tortor augue eu ligula. Nam nec nulla tortor, eget mattis ante. Vestibulum vel odio ut tortor blandit hendrerit. Sed suscipit, mi eget scelerisque sollicitudin, nulla dolor elementum urna, placerat ornare dolor sapien egestas magna. </div> -->
        </div>
        <div class="cont-contacto-form">
            <?php if( $error == 1 ){ ?>
                        <div class="wrapper">
                            <label style="color: red"><?php echo $generalLang['errordatos'];?></label>
                        </div>
                        <?php } ?>
                        <?php if( $error == 2 ){ ?>
                        <div class="wrapper">
                            <label style="color: red"><?php echo $generalLang['erroremail'];?></label>
                        </div>
                        <?php } ?>
                        <?php if( $error == 3 ){ ?>
                        <div class="wrapper">
                            <label style="color: green"><?php echo $generalLang['mensajeenviado'];?></label>
                        </div>
                        <?php } ?>
            <form method="get" action="contacto.php">
        	<div class="formulario-iz">
                <span><?php echo $generalLang['nombre'];?>:<br /></span>
                <input class="nombre" id="nombre" type="text" name="nombre" value="<?php echo $_GET['nombre'];?>" />
                <span><?php echo $generalLang['pais'];?>:<br /></span>
                <input class="pais" id="pais" type="text" name="pais" value="<?php echo $_GET['pais'];?>" />
                
                
            </div>
            <div class="formulario-de">
            	<span><?php echo $generalLang['email'];?>:<br /></span>
                <input class="email" id="email" type="text" name="email" value="<?php echo $_GET['email'];?>" />
                <span><?php echo $generalLang['ciudad'];?>:<br /></span>
                <input class="ciudad" id="ciudad" type="text" name="ciudad" value="<?php echo $_GET['ciudad'];?>" />
            </div>
            <div class="formulario-bot">
            	<span><?php echo $generalLang['comentarios'];?>:<br /></span>
            	<textarea class="comentarios" name="corta" rows="10" cols="40"><?php echo $_GET['corta'];?></textarea> <br />
                <input class="enviar2" id="ini-buscar" type="submit" value="" />
            </div>
            <div class="clear"></div>
        </form>
    </div>
    <div class="clear"></div>
    </div>
    
    
</div>
<div class="datos-contacto">
	<div class="cont-datos-contacto">
    	<div class="dato-contacto">
        	<div class="contacto-pais">Estados Unidos</div>
            <div class="dato-contacto-txt"><?php echo $telUS->getValue();?><br /><?php echo $generalLang['dir'];?>: <?php echo $dirUS->getValue();?><br /><a href="#"><?php echo $emailUS->getValue();?></a></div>
        </div>
        <div class="dato-contacto">
        	<div class="contacto-pais">España</div>
            <div class="dato-contacto-txt"><?php echo $telES->getValue();?><br /><?php echo $generalLang['dir'];?>: <?php echo $dirES->getValue();?><br /><a href="#"><?php echo $emailES->getValue();?></a></div>
        </div>
        <div class="dato-contacto">
        	<div class="contacto-pais">Colombia</div>
            <div class="dato-contacto-txt"><?php echo $telCO->getValue();?><br /><?php echo $generalLang['dir'];?>: <?php echo $dirCO->getValue();?><br /><a href="#"><?php echo $emailCO->getValue();?></a></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="contendor-equipo" style="display:none;">
	<div class="cont-equipo">
		<div class="equipo-tit">NUESTRO EQUIPO</div>
        <div class="bot-prev"></div>
        <div class="slide-cont">
            <div class="equipo-slide">
                
                <ul>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="equipo-mini">
                    	
                        <div class="equipo-m-img"><img src="imagenes/equipo-img1.jpg" /></div>
                        
                        <div class="equipo-mini-info">
                        	<div class="equipo-mini-tit">Nombre Apellido</div>
                            <div class="equipo-mini-fecha">Cargo</div>
                            <div class="equipo-mini-txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum, sapien at cursus.</div>
                        </div>
                        <div class="clear"></div>
                    </li>
                  
                  
                </ul>
                
            </div>
        </div>
        <div class="bot-next"></div>
        <div class="clear"></div>
    </div>
</div>

<?php include("footer.php"); ?>
</body>
</html>
